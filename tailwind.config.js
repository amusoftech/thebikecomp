module.exports = {
  purge: [],
  theme: {
    extend: {
      colors: {
        bg: {
          default: '#3f3f3f',
          filter: '#F1F1F1',
        },
        fg: {
          blue1: '#7EC1F9',
          blue2: '#58B0FC'
        }
      },
      fontSize: {
        '10px': '10px',
        '20px': '20px',
      }

    },
  },
  variants: {},
  plugins: [],
}
