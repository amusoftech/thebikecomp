import React from 'react';
import { MainLayout } from '../containers';
import styles from '../components/Privacy/style.module.scss';
const CompitionType = () => {
    return (
        <MainLayout>
            <h3 className="text-center font-bold mt-6">THE BIKE COMP QUESTION
            <p className="text-sm text-center bold">Use your knowledge to answer the question below</p></h3>
            <h4 className="text-center font-bold mt-6">Which of the following car tyre manufacturers does<br />not make bicycle tyres?</h4>
            <p className="text-sm text-center">Answer *</p>


            <div className="flex flex-wrap ">
                <div className="w-full sm:w-6/12 p-5">
                    <p className="text-sm "  style={{ color: 'red',marginLeft:"54%" }}>Check box. Answer and Name of<br />
                    account and tickets most populate a <br />exportable databse upon payment<br /> Confirmation </p>
                </div>
                <div className={`w-full sm:w-6/12 p-5 ${styles.contactInfo}`}>

                    <div className="info-item items-center">

                        <div>
                            <div className="checkbox">
                                <label>
                                    <input type="checkbox" value="option1" />
                                Goodyear
                            </label>
                            </div>
                            <div className="checkbox">
                                <label>
                                    <input type="checkbox" value="option1" />
                                Continental
                            </label>
                            </div>
                            <div className="checkbox">
                                <label>
                                    <input type="checkbox" value="option1" />
                                Pirelli
                            </label>
                            </div>
                            <div className="checkbox">
                                <label>
                                    <input type="checkbox" value="option2" />
                                Bridgestone
                            </label>
                            </div>
                            <div className="checkbox">
                                <label>
                                    <input type="checkbox" value="option3" />
                                Michelin
                            </label>
                            </div>
                        </div>
                    </div>
                   
                </div>
            </div>
            <div className="flex justify-center py-8">
                <div style={{marginLeft:'50%'}}>
                    <button style={{backgroundColor: '#000',color: '#fff',padding:' 6px 27px',borderRadius: '20px'}} type="submit" >Checkout</button>
                    
                </div>
            </div>


           

        </MainLayout>
    );


}

export default CompitionType;