import React from 'react';
import Password from '../components/Auth/Password';
import { MainLayout } from '../containers';
import { connect} from 'react-redux';



const ResetPassword = ({props,auth}) => {
    const userData = auth &&  auth.user
  
    return (
        <MainLayout>
            <h3 className="text-center font-bold mt-12">Password Reset</h3>
    <p className="text-center mt-6">Enter a new Password for <br />{userData && userData.email}</p>
            <div className="max-w-screen-sm mx-auto">
                <Password userData={userData} /> 
            </div>
        </MainLayout>
    );
} 
const mapStateToProps = state => ({
    ...state
});

 

export default connect(  mapStateToProps, null)(ResetPassword);
