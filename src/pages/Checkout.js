import React, { useEffect } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux'; 
import { loadStripe } from '@stripe/stripe-js';
import {
    CardElement, 
    useStripe,
    useElements,
    Elements
  } from '@stripe/react-stripe-js';
import { BodyContainer } from '../containers';
import { CheckoutList, Footer, Header } from '../components';
 
import { setMenuLayer,getSiteCMS } from '../redux/actions';
import { STRIPE_CLIENT_KEY_DEV } from '../utils/common.utils';
import { paypalCheckoutRequest } from '../api/checkout.api';
import { useHistory } from 'react-router';




const stripePromise = loadStripe(STRIPE_CLIENT_KEY_DEV);

// let stripePromise;

const CheckoutPage = ({items,selectedQuestion, appLoaded, setMenuLayer$ ,props}) => {

    useEffect(() => {
        setMenuLayer$(-1);
        // stripePromise = loadStripe('pk_test_5vZPRXlthCAArZOOovZnK7JT00txj5drRX');
        return () => {
            setMenuLayer$(1);
        }
    }, [appLoaded])


const onPaymentDone = (details, data)=>{
    // TICKETS TO CHECKOUT  ====> items
    // SELECTED QUESTION AND ANSWER ====>  selectedQuestion
    
    const checkoutPayPal = async () => {        
        const res = await paypalCheckoutRequest({items,selectedQuestion});
           if (!!res.status && !!res.link) {
          window.location.href = res.link;
        }
      };

}
const userData = props && props.auth && props.auth.user;
  const redirect = useHistory();
  const site_details = useSelector(state => state.preOrderReducer.sitecms);
  const dispatchCMS = useDispatch()
  useEffect(() => {
      dispatchCMS(getSiteCMS())
  }, []);
  const siteData = site_details[0];
 
  /* if ( typeof  userData == undefined){
    console.log("userData :",userData);
    redirect.push("/login");

  } */

    return (
        <>
            <Header />
            <BodyContainer>
                <h3 className="text-center font-bold mt-6 mb-4 text-bg">CHECKOUT</h3>
                <section className="mx-auto max-w-screen-md px-2 pb-12">
                <Elements stripe={stripePromise}>
                {/* <Elements stripe={siteData && siteData.stripe_public_key}> */}
                    <CheckoutList items={items} question={selectedQuestion} onPaymentDone={onPaymentDone} />   
                </Elements>
                </section>
            </BodyContainer> 
            <Footer />
        </>
    );


   



}


const CheckoutForm = () => {
    const stripe = useStripe();
    const elements = useElements();
  
    const handleSubmit = async (event) => {
      event.preventDefault();
      const {error, paymentMethod} = await stripe.createPaymentMethod({
        type: 'card',
        card: elements.getElement(CardElement),
      });
    };
  
    return (
      <form onSubmit={handleSubmit}>
        <CardElement />
        <button type="submit" disabled={!stripe}>
          Pay
        </button>
      </form>
    );
  };



const mapStateToProps = state => ({
    appLoaded: state.setting.appLoaded,
    selectedQuestion:state.cart.selectedQuestion ,
    items: state.cart.items,
});

const mapDispatchToProps = {
    setMenuLayer$: setMenuLayer,
}

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutPage);