import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";
import { BikeBrief, BannerFilter } from "../components";

export default class BikesTab extends Component {
  constructor(props) {
    super(props);

    this.state = {
      noEBike: false,
      noBike: false,
    };
  }

  render() {
    const {
      bikes,
      showDetails,
      sortByBrand,
      sortByType,
      sortByPriceAtZ,
      getBikeBrands,
      getBikeTypes
    } = this.props;
    const { noEBike, noBike } = this.state;
    return (
      <div>
        <Tabs selectedTabClassName={"bwstyle"}>
          <TabList style={{ fontSize: 25, fontWeight: "bold" }}>
            <Tab
              onClick={() => {
                getBikeBrands()
                getBikeTypes()
              }}

              className={"myTab react-tabs__tab"}>Bikes</Tab>
            <Tab onClick={() => {
              getBikeBrands({ isEbike: true })
              getBikeTypes({ isEbike: true })

            }}
              className={"myTab react-tabs__tab"}>E-Bikes</Tab>
          </TabList>

          <TabPanel>
            <div className="flex flex-wrap mob_bikes_tab">
              <BannerFilter
                sortByPriceAtZ={sortByPriceAtZ}
                sortByBrand={sortByBrand}
                sortByType={sortByType}
                hideBanner
              />
              {bikes &&
                bikes.map((bike, i) => {
                  return (
                    bike.bike_type != 5 && (
                      <BikeBrief
                        onClick={() => showDetails(bike)}
                        bike={bike}
                        key={i}
                      />
                    )
                  );
                })}
            </div>
          </TabPanel>
          <TabPanel>
            <div className="flex flex-wrap mob_bikes_tab">
              <BannerFilter
                sortByPriceAtZ={sortByPriceAtZ}
                sortByBrand={sortByBrand}
                sortByType={sortByType}

                hideBanner />
              {bikes &&
                bikes.map((bike, i) => {
                  return (
                    bike.bike_type == 5 && (
                      <BikeBrief
                        onClick={() => showDetails(bike)}
                        bike={bike}
                        key={i}
                      />
                    )
                  );
                })}
            </div>
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}
