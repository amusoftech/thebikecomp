import React from 'react';
import { MainLayout } from '../containers';
import { SectionContainer, SectionTitle } from '../components/Privacy/Widgets';

import styles from '../components/Privacy/style.module.scss';

const CookiePrivacyPage = () => {
    return (
        <MainLayout>
            <div className="text-center">
                <h2 className="font-bold">The Bike Comp</h2>
                <h3 className="font-bold">Cookie Policy</h3>
            </div>
            <SectionContainer>
                <p>
                    The thebikecomp.com Website (operated by the The Bike Comp at 85-90 Paul Street, Level 3, The Hoxton Mix, London, EC2A 4NE United Kingdom) website uses cookies. We use cookies to personalise content and ads, to provide social media features and to analyse our traffic. We also share information about your use of our site with our social media, advertising and analytics partners who may combine it with other information that you’ve provided to them or that they’ve collected from your use of their services.
                </p><br />
                <p>Cookies are small text files that can be used by websites to make a user's experience more efficient.</p><br />
                <p>The law states that we can store cookies on your device if they are strictly necessary for the operation of this site. For all other types of cookies we need your permission.</p><br />
                <p>This site uses different types of cookies. Some cookies are placed by third party services that appear on our pages.</p><br />
                <p>You can at any time change or withdraw your consent from the Cookie Declaration on our website.</p><br />
                <p>Learn more about who we are, how you can contact us and how we process personal data in our Privacy Policy.</p><br />

                <p>Please state your consent ID and date when you contact us regarding your consent.</p><br />

                <p>Your consent applies to the following domains: dev.thebikecomp.com</p><br />

                <p>Your current state: Allow all cookies. <br /><br />
Your consent ID: lDDBfnNtKPuugT5PX+BmYxL2deqtQGKrvhJX6lV1hf3TNqjqWkVHCw==<br /><br />
Consent date: Monday, October 12, 2020, 11:08:17 PM GMT+5:30</p>

                <p>Cookie declaration last updated on 30/10/2020 by Cookiebot:</p>
            </SectionContainer>


            <SectionContainer>              
                <p><b>Necessary (6)</b><br/>Necessary cookies help make a website usable by enabling basic functions like page navigation and access to secure areas of the website. The website cannot function properly without these cookies.</p>
                <table className={`${styles.table} mt-5`}>
                    <thead>
                        <tr className="text-center font-semibold">
                            <td>Name</td>
                            <td>Provider</td>
                            <td>Purpose</td>
                            <td>Expiry</td>
                            <td>Type</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>__stripe_midme</td>
                            <td>dev.thebikecomp.com</td>
                            <td>This cookie is necessary for making credit card transactions on the website. The service is provided by Stripe.com which allows online transactions without storing any credit card information.</td>
                            <td>1 year</td>
                            <td>HTTP Cookie</td>
                        </tr>

                        <tr>
                            <td>__stripe_sid</td>
                            <td>dev.thebikecomp.com</td>
                            <td>This cookie is necessary for making credit card transactions on the website. The service is provided by Stripe.com which allows online transactions without storing any credit card information.</td>
                            <td>1 day</td>
                            <td>HTTP Cookie</td>
                        </tr>

                        <tr>
                            <td>CookieConsent</td>
                            <td></td>
                            <td>Stores the user's cookie consent state for the current domain</td>
                            <td>1 year </td>
                            <td>HTTP Cookie</td>
                        </tr>

                        <tr>
                            <td>G_ENABLED_IDPS</td>
                            <td>dev.thebikecomp.com</td>
                            <td>Used for secure login to the website with a Google account.</td>
                            <td>7979 years</td>
                            <td>HTTP Cookie</td>
                        </tr>

                        <tr>
                            <td>m</td>
                            <td></td>
                            <td>Determines the device used to access the website. This allows the website to be formatted accordingly.</td>
                            <td>2 years</td>
                            <td>HTTP Cookie</td>
                        </tr>

                        <tr>
                            <td>PHPSESSID</td>
                            <td>dev.thebikecomp.com</td>
                            <td>Preserves user session state across page requests.</td>
                            <td>1 day</td>
                            <td>HTTP Cookie</td>
                        </tr>

                    </tbody>
                </table>
                <br /><br />
                <p><b>Marketing (2)</b><br/>Marketing cookies are used to track visitors across websites. The intention is to display ads that are relevant and engaging for the individual user and thereby more valuable for publishers and third party advertisers.</p>
                <table className={`${styles.table} mt-5`}>
                    <thead>
                        <tr className="text-center font-semibold">
                            <td>Name</td>
                            <td>Provider</td>
                            <td>Purpose</td>
                            <td>Expiry</td>
                            <td>Type</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>NID</td>
                            <td></td>
                            <td>Registers a unique ID that identifies a returning user's device. The ID is used for targeted ads.</td>
                            <td>6 months</td>
                            <td>HTTP Cookie</td>
                        </tr>
                      
                        <tr>
                            <td>oauth2_cs::https://dev.thebikecomp.com::298852658480-af14ifi32ufio5h99i8msfr1pet7uhc5.apps.googleusercontent.com</td>
                            <td></td>
                            <td>Pending</td>
                            <td>Session </td>
                            <td>HTML Local Storage</td>
                        </tr>

                       

                    </tbody>
                </table>
                <br /><br />
            </SectionContainer>


            <script id="CookieDeclaration" src="https://consent.cookiebot.com/708daa26-ca2a-4fc5-91b7-8b4af55dfca7/cd.js" type="text/javascript" async></script>
        </MainLayout>
    );
}

export default CookiePrivacyPage;