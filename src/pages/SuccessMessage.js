import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { MainLayout } from '../containers';


export default class SuccessMessage extends Component {
  render() {
    return (
      <div>
        <MainLayout>         
          
          <div className="max-w-screen-sm mx-auto">
       
          <div className="mt-6 ">
          <img  style={{margin: '0 auto'}} alt="svgImg" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHg9IjBweCIgeT0iMHB4Igp3aWR0aD0iNjQiIGhlaWdodD0iNjQiCnZpZXdCb3g9IjAgMCAxNzIgMTcyIgpzdHlsZT0iIGZpbGw6IzAwMDAwMDsiPjxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0ibm9uemVybyIgc3Ryb2tlPSJub25lIiBzdHJva2Utd2lkdGg9IjEiIHN0cm9rZS1saW5lY2FwPSJidXR0IiBzdHJva2UtbGluZWpvaW49Im1pdGVyIiBzdHJva2UtbWl0ZXJsaW1pdD0iMTAiIHN0cm9rZS1kYXNoYXJyYXk9IiIgc3Ryb2tlLWRhc2hvZmZzZXQ9IjAiIGZvbnQtZmFtaWx5PSJub25lIiBmb250LXdlaWdodD0ibm9uZSIgZm9udC1zaXplPSJub25lIiB0ZXh0LWFuY2hvcj0ibm9uZSIgc3R5bGU9Im1peC1ibGVuZC1tb2RlOiBub3JtYWwiPjxwYXRoIGQ9Ik0wLDE3MnYtMTcyaDE3MnYxNzJ6IiBmaWxsPSJub25lIj48L3BhdGg+PGcgZmlsbD0iIzM4ZTc2ZSI+PHBhdGggZD0iTTE0OC41MDgyNCwzNy41ODJsLTY1LjY1NTg0LDc3LjQ3MjI0bC0zMy4yNTQ0OCwtMzEuMDExNmw0LjY4ODcyLC01LjAzMjcybDI3Ljk4MDk2LDI2LjA5MjRsNjEuNzM0MjQsLTcyLjg0MmMtMTQuNDYxNzYsLTE1LjU5Njk2IC0zNS4xMDUyLC0yNS4zODAzMiAtNTguMDAxODQsLTI1LjM4MDMyYy00My42Mjk1MiwwIC03OS4xMiwzNS40OTA0OCAtNzkuMTIsNzkuMTJjMCw0My42Mjk1MiAzNS40OTA0OCw3OS4xMiA3OS4xMiw3OS4xMmM0My42Mjk1MiwwIDc5LjEyLC0zNS40OTA0OCA3OS4xMiwtNzkuMTJjMCwtMTguMjI4NTYgLTYuMjEyNjQsLTM1LjAyNjA4IC0xNi42MTE3NiwtNDguNDE4eiI+PC9wYXRoPjwvZz48L2c+PC9zdmc+"/>  
          </div>
          <p className="text-center mt-6">Your Password has been reset successfully</p>
            <div className="text-center paddlr10 log_btn">
              <Link to="/login" type="submit" style={{ width: '50%', backgroundColor: '#fbb03b', padding: 12, fontWeight: 'bold', borderRadius: 20, marginTop: 20,marginBottom:50 }} >Log in</Link>
            </div>

          </div>

        </MainLayout>
      </div>
    )
  }
}
