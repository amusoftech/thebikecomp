import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
// import { Link } from 'react-router-dom';

import { BodyContainer } from '../containers';
import { SuccessList, Footer, Header } from '../components';
import { confirmCheckoutRequest } from '../api/checkout.api';
import { emptyCart, setMenuLayer } from '../redux/actions';

const SuccessPage = ({ appLoaded, emptyCart$, setMenuLayer$ }) => {

    const [items, setItems] = useState([]);

  

    useEffect(() => {
        setMenuLayer$(-1);

        // check session id
        const search = window.location.search;
        const params = new URLSearchParams(search);
        let token = '';
        if (!!params.get('session_id')) {
            token = params.get('session_id');
        } else if (params.get('token')) {
            token = params.get('token');
        }
        
        async function confirmPayment(param) {
            const res = await confirmCheckoutRequest(param);
           
            if (!!res && !!res.data) {
                setItems(res.data);
                emptyCart$();
            }
        }
        confirmPayment(token);

        
        return () => {
            setMenuLayer$(1);
        }
    }, [appLoaded])

    return (
        <>
            <Header />
           

            <BodyContainer>
                <h3 className="text-center font-bold mt-6 mb-4 text-bg">Your payment has been successful<br /></h3>
                <h4 className="text-center font-bold">An invoice has been sent to your email</h4>
              
                <section className="mx-auto max-w-screen-md px-2 pb-12">
                    <SuccessList items={items} />
                </section>
            </BodyContainer>
            <Footer />
        </>
    );
}

const mapStateToProps = state => ({
    appLoaded: state.setting.appLoaded,
});

const mapDispatchToProps = {
    emptyCart$: emptyCart,
    setMenuLayer$: setMenuLayer,
}

export default connect(mapStateToProps, mapDispatchToProps)(SuccessPage);