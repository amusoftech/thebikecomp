import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { MainLayout } from "../containers";
import styles from "../components/Privacy/style.module.scss";
import responsive from "../assets/css/responsive.css";
import { Link, withRouter,useHistory } from "react-router-dom";
import { connect, useDispatch, useSelector } from "react-redux";
import { getAllQuestion, setSelectedQuestion } from "../redux/actions";

const Question = ({ history,setSelectedQuestion$,props }) => {
  const [checkBoxChecked, setCheckBoxChecked] = useState("");
  const { handleSubmit, errors, register, trigger } = useForm();

  const [selectedQuestion, setSelectedQuestion] = useState();
  const [selectedAnswer, setSelectedAnswer] = useState();
  const handleOnChange = (e) => {
    trigger(e.target.name);
  };

  const onSubmit = async (data) => {
    // console.log(data);
  };

  
  const dispatch = useDispatch();
  const questions =
    useSelector((state) => state.preOrderReducer.questions) || [];

  useEffect(() => {
    /*  if(questions.length < 1 ){ */
    dispatch(getAllQuestion());
    /*   } */
  }, []);
  
  const userData = props && props.auth && props.auth.user;
  const redirect = useHistory();
/*   if(userData ==null){
    console.log("userData :",userData);
    //redirect.push("/login");

  } */
 

  return (
    <MainLayout>
      <h3 className="text-center font-bold mt-6">
        THE BIKE COMP QUESTION
        <p className="text-sm text-center bold">
          Use your knowledge to answer the question below
        </p>
      </h3>
      <form className="w-full px-6 py-10" onSubmit={handleSubmit(onSubmit)}>
        {questions ? (
          questions.map((question, i) => {
           // console.log("Orders", question);
            return (
              <div>
                <h4 className="text-center font-bold mt-6">
                  {question.question}
                </h4>
                <p className="text-sm text-center">
                  Please select one of the answers below
                </p>
                <div className="flex flex-wrap ">
                  <div className="w-full sm:w-6/12 p-5  mt-6 sm:mt-0"></div>
                  <div
                    className={`w-full sm:w-6/12 p-5   ${styles.contactInfo} for_mobile`}
                  >
                    <div className="info-item items-center">
                      <div>
                        {question.answer1 && (
                          <div className="checkbox">
                            <label>
                              <input
                                value={question.answer1}
                                checked={checkBoxChecked === question.answer1}
                                type="checkbox"
                                onChange={() => {
                                  setCheckBoxChecked(question.answer1);
                                  setSelectedQuestion$(question.id, question.answer1)
                                }}
                                value={question.answer1}
                                name="answer"
                              />
                              {question.answer1}
                            </label>
                          </div>
                        )}
                        {question.answer2 && (
                          <div className="checkbox">
                            <label>
                              <input
                                value={question.answer2}
                                checked={checkBoxChecked === question.answer2}
                                type="checkbox"
                                onChange={() => {
                                  setCheckBoxChecked(question.answer2);
                                   setSelectedQuestion$(question.id, question.answer2)
                                }}
                                value={question.answer2}
                                name="answer"
                              />
                              {question.answer2}
                            </label>
                          </div>
                        )}
                        {question.answer3 && (
                          <div className="checkbox">
                            <label>
                              <input
                                value={question.answer3}
                                checked={checkBoxChecked === question.answer3}
                                type="checkbox"
                                onChange={() => {
                                  setCheckBoxChecked(question.answer3);
                                   setSelectedQuestion$(question.id, question.answer3)
                                }}
                                value={question.answer3}
                                name="answer"
                              />
                              {question.answer3}
                            </label>
                          </div>
                        )}
                        {question.answer4 && (
                          <div className="checkbox">
                            <label>
                              <input
                                value={question.answer4}
                                checked={checkBoxChecked === question.answer4}
                                type="checkbox"
                                onChange={() => {
                                  setCheckBoxChecked(question.answer4);
                                   setSelectedQuestion$(question.id, question.answer4)
                                }}
                                value={question.answer4}
                                name="answer"
                              />
                              {question.answer4}
                            </label>
                          </div>
                        )}
                        {question.answer5 && (
                          <div className="checkbox">
                            <label>
                              <input
                                value={question.answer5}
                                checked={checkBoxChecked === question.answer5}
                                type="checkbox"
                                onChange={() => {
                                  setCheckBoxChecked(question.answer5);
                                  setSelectedQuestion$(question.id, question.answer5)
                                }}
                                value={question.answer5}
                                name="answer"
                              />
                              {question.answer5}
                            </label>
                          </div>
                        )}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            );
          })
        ) : (
          <p>No data here</p>
        )}

        <div className="flex justify-center py-8">
          <div style={{ marginLeft: "50%" }}>
            <button
              onClick={() => {
                checkBoxChecked.length > 0 && history.push("/checkout");
              }}
              style={{
                backgroundColor: "#000",
                color: "#fff",
                padding: " 6px 27px",
                borderRadius: "20px",
              }}
              type="submit"
            >
              Checkout
            </button>
          </div>
        </div>
      </form>
    </MainLayout>
  );
};
const mapStateToProps = (state) => ({
  ...state,
});

const mapDispatchToProps = {
  setSelectedQuestion$: setSelectedQuestion,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Question));
