import React, { useEffect } from 'react';

const ComingSoon = () => {

    useEffect(() => {

        //const element = document.getElementById('coming-soon');
        // element.addEventListener('mousemove', (e) => {
        //     const width = document.body.clientWidth;
        //     const height = document.body.clientHeight;
        //     console.log('[move]', e.offsetX, e.offsetY);
        //     element.style.backgroundPositionX = -e.offsetX;
        //     element.style.backgroundPositionY = -e.offsetY;
        // })
        // const movementStrength = 25;
        // let height = movementStrength / document.body.clientHeight;
        // let width = movementStrength / document.body.clientWidth;
        // element.addEventListener('mousemove', (e) => {
        //     console.log(element.style.backgroundPositionX)
        //     let pageX = e.pageX - document.body.clientWidth / 2;
        //     let pageY = e.pageY - document.body.clientHeight / 2;
        //     let newValueX = width * pageX * -1 - 25;
        //     let newValueY = height * pageY * -1 - 50;
        //     element.style.backgroundPositionX = newValueX;
        // });
        return () => {}
    })

    return (
        <div className="" id="coming-soon">
            <div className="pt-5 px-3">
                <img className="logo" src="/images/logo-white-mix.png" alt="Logo of The Bike Comp"/>
            </div>
            <p className="text-center text-white text-5xl font-bold">We're coming soon!</p>
        </div>
    );
}

export default ComingSoon;