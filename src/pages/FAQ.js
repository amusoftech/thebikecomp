import React, { useState, useEffect } from 'react';
// import { Link } from 'react-router-dom';
import { MainLayout } from '../containers';
import { FAQItem, HowItWorks } from '../components';
import { FAQs } from '../constants/data';
import { connect, useDispatch, useSelector } from 'react-redux';
import { getAllFaq, getSiteCMS } from '../redux/actions';
import { Link, withRouter } from 'react-router-dom';


const FAQPage = () => {
    const [selected, setSelected] = useState(-1);
    const onFAQClick = i => {
        console.log(i, selected);
        setSelected(i === selected ? -1 : i);
    }
    const dispatch = useDispatch()
    const FAQs = useSelector(state => state.preOrderReducer.faqs) || []

    useEffect(() => {
        /*  if(questions.length < 1 ){ */
        dispatch(getAllFaq())
        /*   } */
    }, [])

    const dispatchCMS = useDispatch()
    const site_details = useSelector(state => state.preOrderReducer.sitecms);
    //const site_details = useSelector(state => state.preOrderReducer.sitecms);
    useEffect(() => {
        dispatchCMS(getSiteCMS())
    }, []);
    const siteData = site_details[0];
    console.log('siteData', siteData);

    return (
        <MainLayout>
            <HowItWorks />
            <h3 className="font-bold text-center text-3xl">FAQs</h3>
            <p className="text-center max-w-4xl mx-auto text-gray-600 text-sm mt-4 px-3">You'll find the answers to some of our most Frequently Asked Questions below. If you can’t find the answer you're looking for, please e-mail us on <a className="common-link" href="mailto:info@thebikecomp.com" rel="noopener noreferrer" target="_blank">{siteData && siteData.site_email}</a> and we'll be delighted to help you.</p>
            <section className="mt-10 mx-auto max-w-4xl px-3">
                {
                    FAQs.map((faq, i) => (
                        <FAQItem title={faq.question} description={faq.answer} isOpen={selected === i} onClick={() => onFAQClick(i)} key={i} />
                    ))
                }
            </section>

        </MainLayout>
    )
};

//export default FAQPage;
const mapStateToProps = state => ({
    ...state
});



export default connect(mapStateToProps, null)(withRouter(FAQPage));