import React,{useEffect,useState} from 'react';
import { MainLayout } from '../containers';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { fetchUserByToken } from '../redux/actions';
import PasswordNew from '../components/Auth/PasswordNew';

const NewPassword = ({ userByToken,loadingByToken, props, auth,fetchUserByToken$,match }) => { 

    const token = match.params && match.params.token
    console.log("loadingByToken",loadingByToken, " userByToken", userByToken); 
    const [userData, setuserData] = useState(auth && auth.user)
     
    useEffect(() => {
        //IF TOKE EXIST, PASSWORD IS BEING RECOVRED BY ACCESS TOKEN FROM EMAIL
        //find_user 
         token && fetchUserByToken$({token})
        //  console.log("token",token);
    }, [])

    console.log("userByToken",userByToken);
    return (
        <MainLayout>
            <h3 className="text-center font-bold mt-12">Password Reset</h3>
            <p className="text-center mt-6">Enter a new Password for <br />{userData && userData.email}</p>
            <div className="max-w-screen-sm mx-auto">
             {token && loadingByToken  ? 
             
             <h3>Loading your datail...</h3> 
             : <PasswordNew token={token}   userData={userData ? userData : userByToken} />}
             
            </div>
        </MainLayout>
    );
}
const mapStateToProps = state => ({ 
    loadingByToken: state.auth.loadingByToken,
    userByToken:state.auth.userByToken  
});

const mapStateToDispatch = {
    fetchUserByToken$: fetchUserByToken
};




export default connect(mapStateToProps, mapStateToDispatch)( withRouter( NewPassword));
