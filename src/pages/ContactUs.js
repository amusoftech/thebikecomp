import React, { useState, useEffect } from 'react';
// import GoogleMapReact from 'google-map-react';
import { GoogleMap, Marker, withGoogleMap, withScriptjs } from "react-google-maps";
import { compose, withProps } from "recompose";
import { MainLayout } from '../containers';
import { ContactUsForm } from '../components';
import styles from '../components/Privacy/style.module.scss';
import { connect, useDispatch, useSelector } from 'react-redux';
import { getSiteCMS } from '../redux/actions';



// const AnyReactComponent = ({ text }) => <div><img src="/images/marker.png" alt="Map Marker - TheBikeComp" style={{ width: 30 }} /></div>;

const ContactUsPage = ({ props, contact$ }) => {
    // eslint-disable-next-line
    const [center, setCenter] = useState({ lat: 59.95, lng: 30.33 });
    // eslint-disable-next-line
    const [zoom, setZoom] = useState(11);

    const [selected, setSelected] = useState(-1);
    const onFAQClick = i => {
        console.log(i, selected);
        setSelected(i === selected ? -1 : i);
    }
    const dispatch = useDispatch()
    const site_details = useSelector(state => state.preOrderReducer.sitecms);
    //const site_details = useSelector(state => state.preOrderReducer.sitecms);
    useEffect(() => {
        dispatch(getSiteCMS())
    }, []);
    const siteData = site_details[0];
    const onSubmit = (e) => {
        e.preventDefault()
        const phone = e.target.phone.value
        const first_name = e.target.first_name.value
        const last_name = e.target.last_name.value
        const email = e.target.email.value
        const message = e.target.message.value

    }
    return (
        <MainLayout>
            <div className="mx-auto max-w-3xl px-3 mt-4">
                <h3 className="text-center font-bold">CONTACT US</h3>
                <p className="text-sm text-center">We welcome your feedback, so if you have any suggenstions or comments please send them to us using the comments box below.</p>
                <p className="text-sm text-center">Alternatively, pelase don't hesitate to call our head office in {siteData && siteData.site_address}</p>
                {/* <div className={styles.mapContainer}>
                    <div className={styles.ratioContainer}> */}
                       {/*  <div className={styles.mapWrapper}> */}
                            {/*  <ContactUsMap isMarkerShown /> */}
                            <div className="google-map-code mt-4">
                           
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.339742306546!2d-0.08610778427180499!3d51.52532817963803!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761caf8b8c994f%3A0xa30a72e89d0e7982!2s85%20Paul%20St%2C%20Shoreditch%2C%20London%2C%20UK!5e0!3m2!1sen!2sin!4v1606995326204!5m2!1sen!2sin" width="100%" height="350" frameborder="0" style={{ border: 0 }} allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </div>

{/*                         </div> */}
                   {/*  </div>
                </div> */}
                {/* <Map1 /> */}
                <ContactUsForm onSubmit={onSubmit} siteData={siteData} />
            </div>
        </MainLayout>
    );
}

const ContactUsMap = compose(
    withProps({
        googleMapURL:
            "https://maps.googleapis.com/maps/api/js?key=AIzaSyCjv1kSNXZo5hP94w7na4KAprDc5X3yd6w&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `100%` }} />,
        mapElement: <div style={{ height: `100%` }} />
    }),
    withScriptjs,
    withGoogleMap
)(props => {
    return (
        <GoogleMap defaultZoom={8} defaultCenter={{ lat: 51.490096, lng: -0.127011 }}>
            {props.isMarkerShown && (
                <Marker position={{ lat: 51.490096, lng: -0.127011 }} />
            )}
        </GoogleMap>
    );
})

const mapStateToProps = state => ({
    ...state
});

export default connect(mapStateToProps, null)(ContactUsPage);
