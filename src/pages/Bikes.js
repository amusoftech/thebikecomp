import React, { useState, useEffect } from "react";
import Modal from "react-modal";
import { connect, useDispatch } from "react-redux";

import { MainLayout } from "../containers";
import { BannerFilter, BikeBrief, BikeDetails } from "../components";
import BikesTab from "./BikesTab";
import { getBikeBrands, getBikesInfo, getBikeTypes } from "../redux/actions";
import TermsModal from "../components/Modal/TermsModal";
import { getHasToShowModal } from "../components/Modal/ModalHelp";

const customStyles = {
  overlay: {
    backgroundColor: "rgba(0, 0, 0, 0.65)",
    zIndex: 10,
  },
  content: {
    top: "50%",
    left: "50%",
    width: "100%",
    maxWidth: "1346px",
    maxHeight: "90vh",
    overflowY: "atuo",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    backgroundColor: "#26262E",
    border: "none",
    borderRadius: "25px",
    padding: 0,
  },
};

const BikesPage = ({ bikes, bikeBrands,getBikeBrands$,getBikeTypes$ }) => {
  const dispatch = useDispatch();
 
  const [showModal, setShowModal] = useState(false);
  const [selBike, selectBike] = useState(bikes && bikes[0]) || [];
   /* const [queryFilter, setqueryFilter] = useState('') */
  const onClose = () => setShowModal(false);
  const showDetails = (bike) => {
    selectBike(bike);
    setShowModal(true);
  }; //BIKE_GET_BRANDS
 
 
  return (
    <MainLayout>
      <BannerFilter hideFilter /> 
      <BikesTab
        getBikeBrands={getBikeBrands$}
        getBikeTypes={getBikeTypes$}
        sortByPriceAtZ={(options) => {
          console.log("options", options);
          dispatch(
            getBikesInfo(
              options ? `?byPrice=1&priceOrder=${options.value}` : null
            )
          );
        }}
        sortByType={(type) => {
          dispatch(getBikesInfo(type ? `?byTypeId=${type.value}` : null));
        }}
        sortByBrand={(brand) => {
          dispatch(getBikesInfo(brand ? `?brandId=${brand.value}` : null));
        }}
        bikes={bikes}
        showDetails={showDetails}
      />

      <Modal
        isOpen={showModal}
        onRequestClose={onClose}
        style={customStyles}
        contentLabel="Example Modal"
        ariaHideApp={false}
      >
        <BikeDetails    onClose={onClose} bike={selBike} />
      </Modal>
      <TermsModal /> 
    </MainLayout>
  );
};

const mapStateToProps = (state) => ({
  appLoaded: state.setting.appLoaded,
  bikes: state.bike.bikes,
  bikeBrands: state.bike.brands,
});

const mapDispatchToProps = {
  getBikeBrands$:getBikeBrands,
  getBikeTypes$: getBikeTypes
};

export default connect(mapStateToProps, mapDispatchToProps)(BikesPage);
