import React from 'react';
import { Link } from 'react-router-dom';
import { MainLayout } from '../containers';

import { Summary, WhatCollects, WhatTellsYou, WhoShareWith } from '../components/Privacy';
import { SectionContainer, SectionTitle } from '../components/Privacy/Widgets';

const PrivacyPage = () => {
    return (
        <MainLayout>
            <div className="text-center mt-4">
                <h2 className="font-bold">The Bike Comp</h2>
                <h3 className="font-bold">Privacy Policy</h3>
            </div>
            <Summary />
            <WhatTellsYou />
            <WhatCollects />
            <WhoShareWith />
            <SectionContainer>
                <SectionTitle text="DO WE SEND ANY OF YOUR DATA OUTSIDE OF THE EEA?" />
                <p className="mt-6">The European Economic Area or “EEA” is deemed to have good standards when it comes to data privacy. As such, we consciously limit the occasions when we may need to transfer or handle your data outside of the EEA. Where we do, for example where our service providers are based outside of the EEA, we make sure that your data is still treated fairly and lawfully in all respects (including making sure we have a legal ground for sending your data outside the EEA and putting in place all necessary safeguards for such arrangement).</p>
                <p className="mt-8">Where relevant, you will have the right to see a copy of any safeguards we put in place for international transfers of your data. Just get in touch with us if you would like to find out more.</p>
               
            </SectionContainer>

            <SectionContainer>
                <SectionTitle text="HOW WE KEEP YOUR DATA SECURE?" />
                <p className="mt-6">We adopt industry standard security processes to ensure your data is kept safe and secure and to prevent unauthorised access or use or loss of your data. By way of example, we use secure server software (EV SSL) which encrypts all information you input before it is sent to us. We also make sure that third parties who need to handle your data when helping us to deliver our services are subject to suitable confidentiality and security standards.</p>
                <p className="mt-8">Despite the security measures we implement, please be aware that the transmission of data via the internet is not completely secure. As such, we cannot guarantee that information transmitted to us via the internet will be completely secure and any transmission is at your own risk.</p>
            </SectionContainer>

            <SectionContainer>
                <SectionTitle text="HOW LONG DO WE KEEP YOUR DATA FOR?" />
                <p className="mt-6">We will keep your data for as long as you hold a TheBikeComp account and/or where you are still happy to hear from us about our latest news, products and services. Once you no longer wish to be engaged with TheBikeComp we may still need to keep hold of your data if there is a legal reason for doing so (such as for tax purposes where you have made purchases to play in our competitions or where we need to resolve any disputes with you).</p>
            </SectionContainer>



            <SectionContainer>
                <SectionTitle text="LINKS TO THIRD PARTY WEBSITES" />
                <p className="mt-6">Our website may contain links to enable you to visit other websites easily. However, once you have used these links to leave our website we do not have any control over these third party websites and are not responsible for the protection and privacy of any information which you provide whilst visiting such sites. Your use of these third party sites are not governed by this Privacy Policy. You should exercise caution and examine the privacy policies and terms of use applicable to the web sites in question.</p>
            </SectionContainer>
            <SectionContainer>
                <SectionTitle text="CHANGES TO OUR PRIVACY POLICY" />
                <p className="mt-6">If we amend our Privacy Policy, it will be published on the TheBikeComp.com website so please check back regularly to see if there have been any updates. If we make any substantial changes, we may also email you if it’s appropriate.</p>
            </SectionContainer>
            <SectionContainer>
                <SectionTitle text="YOUR RIGHTS" />
                <p className="mt-6">In certain situations, you are entitled to:</p>
                <ul className="pl-12 list-disc">
                    {
                        [
                            <span><b>access</b> a copy of your personal data;</span>,
                            <span><b>correct or update</b> your personal data, which you can do yourself by logging into your account or if you would prefer, please contact us and we can help you out;</span>,
                            <span><b>erase</b> your personal data;</span>,
                            <span><b>object</b> to the processing of your personal data where we are relying on a legitimate interest (as set out in the above table);</span>,
                            <span><b>restrict</b> the processing of your personal data;</span>,
                            <span>request the <b>transfer</b> of your personal data to a third party; or</span>,
                            <span>where you have provided your consent to certain of our processing activities, in certain circumstances, you may <b>withdraw your consent</b> at any time (but please note that we may continue to process such personal data if we have legitimate legal grounds for doing so).</span>

                        ].map((item, i) => (
                            <li className="leading-8" key={i}>{item}</li>
                        ))
                    }
                </ul>
                <div className="mt-12">If you want to exercise any of these rights, please <Link to="/contact-us"><span className="common-link">Contact Us</span></Link>. You don’t have to pay a fee to exercise your rights, unless your request is clearly unfounded, repetitive or excessive (in which case we can charge a reasonable fee). Alternatively, we may refuse to comply with your request in these circumstances. Where your request is legitimate, we will always respond within one month (unless there is a legal reason to take longer, such as where your request is particularly complex). We may also need you to confirm your identity before we proceed with your request if it is not clear to us who is making the request.</div>
                <div className="mt-12">In addition to the above, you may get in touch with the ICO (Information Commissioner’s Office) if you are concerned about the way in which we are handling your personal data. However, where possible, we would really appreciate you speaking with us first if you have any concerns.</div>
            </SectionContainer>
            <SectionContainer >
                <SectionTitle text="HOW TO OPT-OUT OF MARKETING" />
                <p className="">You can opt-out of any TheBikeComp marketing at any time either by <Link to="/contact-us"><span className="common-link">Contacting Us</span></Link> or by using the opt-out function detailed in the relevant marketing email. Please note that we may still need to send you service notifications by email, such as advising you of updated privacy terms or terms of play.</p>
                <p className="mt-12">We do not pass your data onto any third parties for their marketing purposes. However, if we ever wish to do so in the future we will always get your consent. You can then opt-out from any third party marketing at anytime following the same process as above.</p>
            </SectionContainer>
            <SectionContainer>
                <SectionTitle text="contact us" />
                <p className="mt-6">If you would like to discuss anything in this policy or if you want to exercise your rights, please get in touch:</p>
                <p className="mt-8">Email: <a className="common-link" href="mailto: info@thebikecomp.com" rel="noopener noreferrer" target="_blank">info@thebikecomp.com</a></p>
                <p className="mt-12">Please write to us at:</p>
                <p className="mt-6">Customer Service</p>
                <p>TM Competitions LTD</p>
                <p>3rd Floor</p>
                <p>86-90 Paul Street</p>
                <p>London.</p>
                <p>EC24 4NE</p>
                <p>United Kingdom</p>
                <p className="mt-12">Telephone: +447773645816</p>
                <p className="mt-16">LAST UPDATED: JUNE 2020</p>
                




            </SectionContainer>
            
        </MainLayout>
    );
}

export default PrivacyPage;