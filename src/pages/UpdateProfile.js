import React from 'react';
import { connect } from 'react-redux';
import UpdateUser from '../components/Auth/UpdateUser';
import { MainLayout } from '../containers';





const UpdateProfile =(  props ) => {
    const userData = props && props.auth && props.auth.user
  //  console.log('My Details:',userData);
    return (
        <MainLayout>
            <h3 className="text-center font-bold mt-12">Edit Account Details</h3>

            <div className="max-w-screen-sm mx-auto formWidth">
                <UpdateUser details={userData}/>
            </div>
          
        </MainLayout>
    );
}

const mapStateToProps = state => ({
    ...state
});

 

export default connect(  mapStateToProps, null)(UpdateProfile);
