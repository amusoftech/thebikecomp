export { default as AboutUsPage } from './AboutUs';
export { default as BikesPage } from './Bikes';
export { default as CheckoutPage } from './Checkout';
export { default as CookiePrivacyPage } from './Cookie';
export { default as ComingSoonPage } from './ComingSoon';
export { default as ContactUsPage } from './ContactUs';
export { default as FAQPage } from './FAQ';
export { default as GamePage } from './Game';
export { default as HomePage } from './Home';
export { default as LoginPage } from './Login';
export { default as PrivacyPage } from './Privacy';
export { default as SignUpPage } from './SignUp';
export { default as SuccessPage } from './Success';
export { default as TOCPage } from './TOC';
export { default as MyProfile } from './MyProfile';
export { default as ResetPassword } from './ResetPassword';
export { default as NewPassword } from './NewPassword';
export { default as UpdateProfile } from './UpdateProfile';
export { default as ForgotPassword } from './ForgotPassword';
export { default as Message } from './Message';
export { default as SuccessMessage } from './SuccessMessage';
export{ default as ProfileUpdate} from './ProfileUpdate';
export{ default as Welcome} from './Welcome';


export {default as MyCompition} from './../components/User/MyCompition';
