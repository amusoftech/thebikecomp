import React from 'react';

import { AboutLayout } from '../containers';
import { AboutUs } from '../components'
const AboutUsPage = () => (
    <AboutLayout>
        <AboutUs/>
    </AboutLayout>
);

export default AboutUsPage;