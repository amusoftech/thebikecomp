import React, { useState } from "react";
import { Link, Route, useHistory } from "react-router-dom";
import { useForm } from "react-hook-form";
import { subscribe } from "./../redux/actions";
import { connect } from "react-redux";
import { toastr } from 'react-redux-toastr';
import './welcomeStyle.css';


import { Images } from '../assets/images/ElephantBike.png'

const socialIcons = [
    {
        image: "/images/social/facebook.png",
        link: "https://facebook.com/thebikecomp",
    },
    {
        image: "/images/social/instagram.png",
        link: "https://instagram.com/thebikecomp",
    },
];
const Welcome = ({ subscribe$ }) => {
    const { handleSubmit, errors, register, trigger } = useForm();
    const [userEmail, setUserEmail] = useState()
    const handleOnChange = (e) => {
        console.log("name", e.target.value);
        setUserEmail(e.target.value)

    };
    const onSubmit = async (data) => {
        console.log("==>>", userEmail);
        if (userEmail) {
            subscribe$(userEmail);
            toastr.success("Done", "You have successfully Subscribed to us.")
            setUserEmail('')
        } else {
            toastr.error("Error", "Invalid email.")
        }

    };
    const triggerAllFields = () => {
        const names = ["email"];
        for (let name of names) {
            trigger(name);
        }
    };

    const cookies = document.cookie;
    const history = useHistory();
    const search = window.location.search;
    const params = new URLSearchParams(search);
    const redirect = params.get('redirect');

    /*   if (cookies) {
          history.push("/cookie-policy");
      } else {
          history.push("/");
      }
     */

    return (
        <div className="flex flex-wrap body welcome">
            <section className="welcomeSec">
                <div className={`px-8 mx-auto max-w-6xl`} style={{ fontSize: 14 }}>
                    <h2 className="py-8 text-center font-bold"  style={{ fontFamily: 'Arial',textShadow: '1px 2px #d8cdcdc2' }}>The Bike Comp</h2>
                    <p className="mb-6 headText">
                        An exciting new monthly competition to win Bikes & E-Bikes.<br />
                 We're launching 1st December 2020, <br />
                 but while you wait we're giving away a bike for FREE!<br />
                  Competition closes 29th November 2020 <br />
                        <span className="belowDetails">Details and Entry Below </span>
                    </p>
                    <div className="w-full flex flex-wrap">
                        <div className="w-full sm:w-1/3"></div>
                        <div className="w-full sm:w-1/3">

                            <img src="/images/ElephantBike.png" alt="Home Banner" />

                        </div>
                        <div className="w-full sm:w-1/3"></div>
                    </div>


                    <h3> <b>WIN A BIKE, GIVE A BIKE</b></h3>
                    <p className="secondText"> You win and our chosen charity donates a bike in Malawi.<br />
 The Elephant Bike is a LIMITED EDITION: MADE IN BRITAIN by top quality <br />
  bike experts, fully and professionally refurbished former British Postal Bike. With a robust <br />
   steel step through frame in a choice of two sizes, with easy to use 3 speed hub gears and <br />great load carrying capacity. this is the bike you need for town shopping and commuting.<br />
                    </p>
                    <h3> <b> TO ENTER</b></h3>
                    <h5><b> Subscribe for updates & 1 FREE ENTRY</b></h5>
                    <form
                        className="w-full mt-6"
                        onSubmit={handleSubmit(onSubmit)}
                    >
                        <input value={userEmail} type="text" onChange={handleOnChange}
                            className="text-gray-100 placeholder-gray-500 py-2 px-4 rounded-md bg-transparent
                                focus:outline-none border border-fg-blue1 mr-2 mb-2"
                            style={{ fontSize: 14,color:'black' }} placeholder="Email Address" name="email"
                        />
                        <button type="submit" className="subscribeBtn">SUBSCRIBE</button>
                    </form>
                    <p className="mt-3 lastContent">By Subscribing you agree to:<br />The Bike Comp - Elephant Bike Competition <Link  target="_blank" to="/The%20Bike%20Comp%20-%20Elephant%20Bike%20Competition%20Terms%20&%20Conditions.pdf" style={{color:'blue'}}>Terms & Conditions</Link> <br />and The Bike Comp <Link to="/The%20Bike%20Comp%20-%20Terms%20and%20Conditions.pdf"  style={{color:'blue'}}  target="_blank">Terms & Conditions</Link></p>
                    <p className="lastContent">For more information on the prize and amazing charity that we will be contributing to <br /> Visit</p>
                    <div className="w-full flex flex-wrap">
                        <div className="w-full sm:w-1/3"></div>
                        <div className="w-full sm:w-1/3 centerText">
                            <a href="https://www.cycleofgood.com/products/elephant-bikes/elephant-bike/" target="_blank">
                            <img src="/images/Group_300.png"   alt="Home Banner" />
                            </a>
                            
                        </div>
                        <div className="w-full sm:w-1/3"></div>
                    </div>
              
                </div>

            </section>

        </div >


    );
};
const mapStateToProps = (state) => ({
    ...state,
});

const mapDispatchToProps = {
    subscribe$: subscribe,
};

export default connect(mapStateToProps, mapDispatchToProps)(Welcome);
//export default Footer;
