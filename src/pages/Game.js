import React, { Component } from 'react';

import { BodyContainer } from '../containers';
import { Footer, GamePanel, Header, SubmitTicket } from '../components';

class GamePage extends Component {



    constructor() {
        super();
        this.state = {
            hScroll: '0.00',
            vScroll: '0.00',
        };        
    }

    componentDidMount() {

    }

    onSetHScroll = (val) => {
        this.setState({ ...this.state, hScroll: Number(val).toFixed(2) })
    }

    onSetVScroll = (val) => {
        this.setState({ ...this.state, vScroll: Number(val).toFixed(2) })
    }

    render() {
        return (
            <>
                <Header />
                <BodyContainer>
                    <h3 className="text-center font-bold mt-6 mb-4">The Bike Comp Game</h3>
                    <section className="mx-auto max-w-screen-lg px-2">
                        <GamePanel 
                            hScroll={this.state.hScroll}
                            vScroll={this.state.vScroll}
                            onSetHScroll={this.onSetHScroll.bind(this)}
                            onSetVScroll={this.onSetVScroll.bind(this)}
                            />
                        <SubmitTicket 
                            range={{x: this.state.hScroll, y: this.state.vScroll}}
                            onSetHScroll={this.onSetHScroll.bind(this)}
                            onSetVScroll={this.onSetVScroll.bind(this)}
                        />
                    </section>
                </BodyContainer>
                <Footer />
            </>
        );
    }
}

export default GamePage;