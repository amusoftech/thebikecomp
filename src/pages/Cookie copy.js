import React from 'react';
import { MainLayout } from '../containers';
import { SectionContainer, SectionTitle } from '../components/Privacy/Widgets';

import styles from '../components/Privacy/style.module.scss';

const CookiePrivacyPage = () => {
    return (
        <MainLayout>
            <div className="text-center">
                <h2 className="font-bold">The Bike Comp</h2>
                <h3 className="font-bold">Cookie Policy</h3>
            </div>
            <SectionContainer>
                <p>
                    The thebikecomp.com Website (operated by the The Bike Comp at 85-90 Paul Street, Level 3, The Hoxton Mix, London, EC2A 4NE United Kingdom) website uses cookies. We use cookies to personalise content and ads, to provide social media features and to analyse our traffic. We also share information about your use of our site with our social media, advertising and analytics partners who may combine it with other information that you’ve provided to them or that they’ve collected from your use of their services.
                </p><br />
                <p>Cookies are small text files that can be used by websites to make a user's experience more efficient.</p><br />
                <p>The law states that we can store cookies on your device if they are strictly necessary for the operation of this site. For all other types of cookies we need your permission.</p><br />
                <p>This site uses different types of cookies. Some cookies are placed by third party services that appear on our pages.</p><br />
                <p>You can at any time change or withdraw your consent from the Cookie Declaration on our website.</p><br />
                <p>Learn more about who we are, how you can contact us and how we process personal data in our Privacy Policy.</p><br />

                <p>Please state your consent ID and date when you contact us regarding your consent.</p><br />

                <p>Your consent applies to the following domains: dev.thebikecomp.com</p><br />

                <p>Your current state: Allow all cookies. <br /><br />
Your consent ID: lDDBfnNtKPuugT5PX+BmYxL2deqtQGKrvhJX6lV1hf3TNqjqWkVHCw==<br /><br />
Consent date: Monday, October 12, 2020, 11:08:17 PM GMT+5:30</p>

<p>Cookie declaration last updated on 30/10/2020 by Cookiebot:</p>




                <p>The thebikecomp.com website (operated by XXX business, registered number XXXXXX, of address – “<b>XXX</b>”, “<b>we</b>”, “<b>us</b>” or “<b>our</b>”) uses cookies and other tracking technologies. In summary, these distinguish you from other users of our website and may:</p>
                <ul className="list-disc pl-12 mt-6">
                    {
                        [
                            <span>be essential to the running of the website;</span>,
                            <span>help the website to perform more effectively;</span>,
                            <span>allow us to deliver a more personalised browsing experience;</span>,
                            <span>keep you up to date with The Bike Comp, even after you have left the website;</span>,
                            <span>enable us to understand our customers’ browsing behaviour; and/or</span>,
                            <span>allow us to improve the website.</span>
                        ].map((item, i) => (
                            <li className="leading-10">{item}</li>
                        ))
                    }
                </ul>
            </SectionContainer>
            <SectionContainer>
                <SectionTitle text="WHAT ARE COOKIES AND TRACKING TECHNOLOGIES?" />
                <p className="mt-6">These essentially enable us (or our third party partners, as detailed below) to store information on your browser or device to enable us or them to identify you and monitor certain activities, for the purposes set out in this policy.</p>
                <p className="mt-4">They may:</p>
                <p className="mt-4">(i) expire at the end of your browser session (allowing us to link your activities during that session – these are often called “session cookies”);</p>
                <p className="mt-4">(ii) be stored on your browser/device in between sessions, enabling us to remember your preferences and actions for future visits. They need to be manually deleted or will expire after the particular period set by that cookie – these are often referred to as “persistent cookies”;</p>
                <p className="mt-4">(iii) be set by TheBikeComp, which are often referred to as “first-party cookies”; or</p>
                <p className="mt-4">(iv) be set by third parties – often referred to as “third party cookies”.</p>

                <p className="mt-10 font-bold">Importantly, we will not use any cookies/tracking technologies (or permit any third parties to place these on our website) which are not strictly necessary for the operation of the website, without first getting your consent.</p>
                <p className="mt-10">You can always change your mind about all types of cookies/tracking technologies (see “How to block these cookies/tracking technologies” section below).</p>
                <p className="mt-10">Please note that third parties (including, for example, advertising networks and providers of external services like web traffic analysis services) may also use cookies/similar technologies, over which we have no control. These are subject to the terms of the relevant third parties’ policies so please read these carefully.</p>
            </SectionContainer>

            <SectionContainer>
                <SectionTitle text="WHAT TYPES OF COOKIES/TRACKING TECHNOLOGIES DO WE USE?" />
                <p className="mt-4">The following table sets out the different types of cookies  uses, with the second table detailing our third party cookies individually:</p>
                <table className={`${styles.table} mt-10`}>
                    <thead>
                        <tr className="text-center font-semibold">
                            <td>Type</td>
                            <td>Purpose</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td className="font-semibold">Strictly necessary</td>
                            <td>These are required for the operation of our website. They include, for example, cookies that enable you to log into your account and use our shopping cart when you wish to play in a competition.</td>
                        </tr>
                        <tr>
                            <td className="font-semibold">Analytical/performance</td>
                            <td>These allow us to recognise and count the number of website visitors, to see how visitors move around and use our website (including time spent on pages, download errors, click throughs etc). This helps us to ensure that our website is operating effectively and improve our services.</td>
                        </tr>
                        <tr>
                            <td className="font-semibold">Functionality</td>
                            <td>These are used to recognise you when you return to our website. This allows us to tailor your browsing experience by personalising the content you see, greet you personally without you needing to input your details each time and to remember your preferences.</td>
                        </tr>
                        <tr>
                            <td className="font-semibold">Targeting</td>
                            <td>These record your visit to our website, the pages you have visited and the links you have followed. We will use this information to make our website and the advertising displayed on it more relevant to your interests. We may also share this information with third parties for this purpose (if you have consented).</td>
                        </tr>
                        <tr>
                            <td className="font-semibold">Re-targeting</td>
                            <td>	These record the same types of information as above but display TheBikeComp (or third party advertising, to the extent you have consented) once you have left the TheBikeComp website.</td>
                        </tr>
                    </tbody>
                </table>

                <table className={`${styles.table} mt-16`}>
                    <thead>
                        <tr className="text-center font-semibold">
                            <td>Name</td>
                            <td>Type</td>
                            <td>Purpose</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Google Analytics/Signals</td>
                            <td>Analytical/performance</td>
                            <td>Google Analytics software helps us to recognise and count the number of website visitors, to see how visitors move around and use our website (including time spent on pages, download errors, click throughs etc). This helps us to ensure that our website is operating effectively and improve our services.</td>
                        </tr>
                        <tr>
                            <td>Awin</td>
                            <td>Analytical/performance</td>
                            <td>This cookie is used to track whether we owe any commission to a site (affiliate) which has referred business to us.</td>
                        </tr>
                        <tr>
                            <td>Google AdWords</td>
                            <td>Analytical/performance</td>
                            <td>This cookie is only active when you complete an order to track how our campaigns are performing and if we then owe any money as a commission.</td>
                        </tr>
                        <tr>
                            <td>Bronto</td>
                            <td>Targeting</td>
                            <td>Bronto cookie helps us track the performance of our e-mail marketing campaigns to make them more relevant to you.</td>
                        </tr>
                        <tr>
                            <td>ARM</td>
                            <td>Targeting</td>
                            <td>This cookie tracks the performance of our TV marketing campaign to allow us to measure it's effectiveness and make it more targeted.</td>
                        </tr>
                        <tr>
                            <td>Facebook</td>
                            <td>Targeting</td>
                            <td>This cookie tracks the performance of our Facebook marketing campaigns allowing us to analyse them and make them more personalised to you.</td>
                        </tr>
                        <tr>
                            <td>Qubit</td>
                            <td>Targeting</td>
                            <td>Qubit helps us record your visit to our website, the pages you have visited and the links you have followed to make our website more relevant to your interests.</td>
                        </tr>
                        <tr>
                            <td>Conversant</td>
                            <td>Re-targeting</td>
                            <td>Conversant records your visit to our website and the pages you have visited so once you have left the TheBikeComp  website, we can make our advertising more specific to you.</td>
                        </tr>
                        <tr>
                            <td>Hotjar</td>
                            <td>Analytical/performance</td>
                            <td>Hotjar’s cookie helps us to track user movement around our website in order to allow us to optimise its performance and enhance your user experience.</td>
                        </tr>
                    </tbody>
                </table>
            </SectionContainer>

            <SectionContainer>
                <SectionTitle text="HOW TO BLOCK THESE COOKIES/TRACKING TECHNOLOGIES" />
                <p className="mt-4">You can manage your settings and block cookies/tracking technologies by activating the setting on your browser that allows you to refuse the setting of all or some cookies. However, if you use your browser settings to block all cookies (including strictly necessary cookies) you may not be able to access all or parts of our website.</p>
                <p className="mt-16">LAST UPDATED: JUNE 2020</p>
            </SectionContainer>

            <script id="CookieDeclaration" src="https://consent.cookiebot.com/708daa26-ca2a-4fc5-91b7-8b4af55dfca7/cd.js" type="text/javascript" async></script>
        </MainLayout>
    );
}

export default CookiePrivacyPage;