import React from 'react';

import { BodyContainer } from '../containers';
import { Footer, Header, HomeBanner, HowItWorks } from '../components';

const HomePage = () => {
    return (
        <>
            <Header />
            <HomeBanner />
            <BodyContainer>
                <HowItWorks />
            </BodyContainer>
            <Footer />
        </>
    );
}

export default HomePage;