import React, { useEffect, useState } from 'react';
import { MainLayout } from '../containers';
import styles from './../components/Auth/style.module.scss';
import { connect } from "react-redux";
import { useForm } from "react-hook-form";
import { forgot } from '../redux/actions';
import { useHistory } from 'react-router';

const validations = {
  email: {
    required: "Email is required!",
    minLength: {
      value: 5,
      message: "Email must be at least 5 characters long!",
    },
    pattern: {
      // eslint-disable-next-line
      value: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
      message: "Please input valid email address!",
    },
  },
};
const ForgotPassword = ({ forgot$ }) => {
  const { handleSubmit, errors, register, trigger } = useForm();
  const [serverError, setserverError] = useState();
  const handleOnChange = (e) => {
    setserverError(undefined)
    trigger(e.target.name);
  };
  const history = useHistory();
  const onSubmit = async (data) => {
    forgot$(data, async  (result) => {
      if (result) {
        history.push('/message/' + data.email); 
      }
      else {      
        result && setserverError(result.message); 
      }
    });
  };

  const triggerAllFields = () => {
    setserverError(undefined);
    const names = ["email"];
    for (let name of names) {
      trigger(name);
    }
  };
  return (
    <MainLayout>
      <h3 className="text-center font-bold mt-12">Forget your Password?</h3>
      <p className="text-center mt-6">Enter your email <br />to recover your account</p>
      <div className="max-w-screen-sm mx-auto">
        <form className="w-full px-6 py-10" onSubmit={handleSubmit(onSubmit)}>
          <div className="flex flex-wrap">
            <div className={`${styles.formGroup} ${styles.wFull}`}>
              <input type="text" name="email" placeholder="email"
                style={{ borderRadius: 20, paddingLeft: 25 }}
                onChange={handleOnChange}
                ref={register(validations.email)}
              />
              {errors.email && (
                <p style={{ position: 'absolute' }} className={`text-sm ${styles.error}`}>{errors.email.message}</p>
              )}
              {
                serverError &&
                <p
                  style={{ position: "absolute" }}
                  className={`text-sm ${styles.error}`}
                >
                  {serverError}
                </p>

              }
            </div>
          </div>

          <div className="text-center paddlr10 log_btn">
            <button onClick={triggerAllFields} type="submit" style={{ width: '100%', backgroundColor: '#fbb03b', padding: 12, fontWeight: 'bold', borderRadius: 20, marginTop: 20 }} >Reset Password</button>

          </div>
        </form>
      </div>

    </MainLayout>
  );
}
const mapStateToProps = (state) => ({

});

const mapDispatchToProps = {
  forgot$: forgot,
};
export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword)