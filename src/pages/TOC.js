import React from 'react';
import { AboutLayout } from '../containers';
// import { TOC } from '../components'

const TOCPage = () => (
    <AboutLayout>
        <div className="text-center mt-4">
            <h2 className="font-bold">The Bike Comp</h2>
            <h3 className="font-bold">Terms and Conditions</h3>
            <p className="text-center mt-3">To view THE BIKE COMP competition Terms and Conditions <a className="common-link" href="http://thebikecomp.com/The%20Bike%20Comp%20-%20Terms%20and%20Conditions.pdf" target="_blank" rel="noopener noreferrer">click here</a></p>
        </div>
        {/* <TOC /> */}
    </AboutLayout>
)

export default TOCPage;