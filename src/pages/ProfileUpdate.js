import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { MainLayout } from '../containers';


export default class ProfileUpdate extends Component {
  render() {
    return (
      <div>
        <MainLayout>         
          
          <div className="max-w-screen-sm mx-auto">
       
          
          <p className="text-center mt-6">Your Account Details have been successfully updated</p>
            <div className="text-center paddlr10 log_btn">
              <Link to="/my-profile" type="submit" style={{ width: '50%', backgroundColor: '#fbb03b', padding: 12, fontWeight: 'bold', borderRadius: 20, marginTop: 20,marginBottom:50 }} >My Profile</Link>
            </div>

          </div>

        </MainLayout>
      </div>
    )
  }
}
