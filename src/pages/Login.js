import React from 'react';

import { MainLayout } from '../containers';
import { LoginForm } from '../components';


const LoginPage = () => {
    return (
        <MainLayout>
            <h3 className="text-center font-bold mt-12">LOG IN</h3>
            <div className="max-w-screen-sm mx-auto">
                <LoginForm />
            </div>
        </MainLayout>
    );
}

export default LoginPage;