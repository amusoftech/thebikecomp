import React, { useState } from 'react';
import { MainLayout } from '../containers';
import ProfileTab from './../components/User/ProfileTabs'
import ProfileView from '../components/User/ProfileView';

import { connect } from 'react-redux';
import { withRouter } from 'react-router';
const MyProfile = (props) => {
    const userData = props && props.auth && props.auth.user

    const dispatch = props.dispatch

    const [email_pref_1, setEmail_pref_1] = useState(userData ? (userData && userData.email_preferences_1 != 0) : false)
    const [email_pref_2, setEmail_pref_2] = useState(userData ? (userData && userData.email_preferences_2 != 0) : false)
    const activeTab = props.match && props.match.params && props.match.params.at


    // useEffect(() => {
    //     console.log(userData)
    //     userData && console.log(userData.email_preferences_1);
    //     //const {email_preferences_1} =userData
    //     userData && setEmail_pref_1( userData.email_preferences_1 != "0" )
    //     userData && setEmail_pref_2( userData.email_preferences_2 != "0" )

    // }, [userData])
    return (
        <MainLayout>
            <div className="max-w-screen-sm mx-auto desktop-view">
                <ProfileTab activeTab={activeTab} dispatch={dispatch} email_pref_1={email_pref_1} email_pref_2={email_pref_2} userDetails={userData} />
            </div>

            <div className="max-w-screen-sm mx-auto mobile-view">
                <ProfileView />
            </div>

        </MainLayout>
    );
}

/* export default MyProfile;    */


const mapStateToProps = state => ({
    ...state
});



export default connect(mapStateToProps, null)(withRouter(MyProfile));


