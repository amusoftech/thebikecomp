import React from 'react';
import { MainLayout } from '../containers';
import { RegisterForm } from '../components';
import { Link, useLocation, useHistory } from "react-router-dom";

const ColoredLine = ({ color }) => (
    <hr
        style={{
            color: color,
            backgroundColor: color,
            height: 1,
            marginBottom:120
        }}
    />
);


const SignUpPage = () => {
    return (
        <MainLayout>
            <h3 className="text-center font-bold mt-12">Sign up with email</h3>

            <div className="max-w-screen-sm mx-auto">
                <RegisterForm />
            </div>
            <ColoredLine color="#aeaeae" />
        </MainLayout>
    );
}

export default SignUpPage;