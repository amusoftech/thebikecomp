import React  from 'react'
import { Button } from 'react-bootstrap'
import Modal from 'react-modal';
import Compition from './Compition';
import Moment from 'moment';

const customStyles = {
    overlay: {
        backgroundColor: 'rgb(0 0 0 / 35%)',

        zIndex: 10,
    },
    content: {
        top: '50%',
        left: '50%',
        width: '50%',
        height: 320,
        overflowY: 'atuo',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: '#fff',
        border: '1px solid #000',
        padding: 0,
    }
}


export const ModalExample = ({ bikeDetails, showModal, onClose }) => {



    const date = Moment(bikeDetails.created_at).format('MMMM  YYYY');

    return (
        <>
            <Modal
                isOpen={showModal}
                onRequestClose={onClose}
                style={customStyles}
                contentLabel='Example Modal'
                ariaHideApp={false}
            >  <Button onClick={onClose} className="closeIcon" >
                    <img src="https://img.icons8.com/metro/100/000000/cancel.png" />
                </Button>
                <div className="head">

                    <span className="uppercase text-center" style={{ marginLeft: '28%' }}>The Bike Competition :{date}
                    </span>

                </div>

                < Compition bikeDetails={bikeDetails} />

            </Modal>
        </>

    );

}

export default ModalExample;