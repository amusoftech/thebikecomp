import React, {  useEffect, useState } from 'react';
import { MainLayout } from '../../containers';
import { ModalExample } from './ModalExample';
import { ViewResult } from './ViewResult';
import { connect, useDispatch, useSelector } from 'react-redux';
import Moment from 'moment';
import { Button } from 'react-bootstrap'
import { Link } from "react-router-dom";

import { getPreOrdersByUser, getUserFromAsync } from '../../redux/actions';
import { BASE_URL } from '../../utils/common.utils';


const customStyles = {
    overlay: {
        backgroundColor: 'rgb(0 0 0 / 35%)',

        zIndex: 10,
    },
    content: {
        top: '50%',
        left: '50%',
        width: '50%',
        height: 320,
        overflowY: 'atuo',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: '#fff',
        border: '1px solid #000',
        padding: 0,
    }
}

//export default class MyCompition extends Component {
const MyCompition = (props) => {


    const [isOpen, setIsOpen] = useState(false)
    const [selecteditem, setSelecteditem] = useState({})
    const toggleModal = () => {
        setIsOpen(!isOpen)
    }

    const closeModal = () => {
        this.setState({ isOpen: false });
    }


    const userData = props && props.auth;
    const userDetails = userData && userData.user;
    const dispatch = useDispatch()
    const preOrders = useSelector(state => state.preOrderReducer.preOrders) || []
    useEffect(() => {
        userDetails && userDetails.id && dispatch(getPreOrdersByUser(`?user_id=${userDetails && userDetails.id}`))
        dispatch(getUserFromAsync())
    }, [userDetails && userDetails.id]);


    /*     const dispatchWinnder = useDispatch();
        const previousOrder =
        useSelector((state) => state.preOrderReducer.previousOrders) || [];
    
      useEffect(() => {     
          dispatchWinnder(getPreviousWinnder());
    
      }, []);
    
       */
     

    const [showModal, setShowModal] = useState(false);
    const onClose = () => setShowModal(false);

    return (
        <div>
            <div className="desktop-view">
                <div className="flex flex-wrap  mt-6">
                    {preOrders.length != 0 ?
                        preOrders.map((data, i) => {
                            const payload = JSON.parse(data.payload);
                            const bikeDetails = payload && payload['bike'];
                           const singleBike = payload && payload[0]['bike'];
                            const date = Moment(data.created_at).format('MMMM  YYYY');
                            const imgg = singleBike && singleBike['main_image'];
                            return <div key={i}>
                                <div className="order-box sm:w-6/12 p-5 mt-6 sm:mt-0" style={{ marginTop: 20 }}>
                                    <div className="w-full flex flex-wrap ">
                                        <div className="sm:w-6/12 ">
                                            <p className="text-sm"><b>The Bike Comp</b></p>
                                            <p className="text-sm">{date}</p>
                                        </div>
                                        <div className="sm:w-6/12 ">
                                            <img style={{ width: '50%', margin: 'auto' }} src={`${BASE_URL}${imgg}`} />
                                            {/* <img src="https://img.icons8.com/ios/64/000000/bicycle.png" /> */}
                                        </div>
                                    </div>
                                    <div className="w-full flex flex-wrap ">
                                        <div className="sm:w-6/12">
                                            <Button className="order-btn " onClick={() => {
                                                setSelecteditem(data)
                                                toggleModal()
                                            }}>View My Order </Button>

                                        </div>
                                        <div className="sm:w-6/12 ">
                                            {/*   < ViewResult  winner={previousOrder}/> */}
                                            < ViewResult />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        })
                        :
                        <p className="text-center marginLeft">You have not entered any competitions yet, <br />please click <Link to="/bikes" style={{ color: 'blue' }}>here</Link> to go to   <span className="uppercase text-center">The Bike Competitions</span></p>
                    }
                </div>

            </div>
            <div className="mobile-view">
                <MainLayout>
                    <div className="body" style={{ borderRadius: 20, marginTop: 50, paddingBottom: 73, marginBottom: 50 }}>

                        {preOrders.length != 0 ?
                            preOrders.map((data, i) => {
                                const payload = JSON.parse(data.payload);
                                const bikeDetails = payload && payload['bike'];
                                const date = Moment(data.created_at).format('MMMM  YYYY');
                                const imgg = bikeDetails && bikeDetails['main_image'];
                                return <div key={i}>
                                    <div className={i == 0 ? "order-box-first" : "order-box"} >
                                        <div className="w-full flex flex-wrap ">
                                            <div className="sm:w-6/12 mt-6">
                                                <p className="text-sm"><b>The Bike Comp</b></p>
                                                <p className="text-sm">{date}</p>
                                            </div>
                                            <div className="sm:w-6/12 mt-6 ">
                                                <img style={{ width: '30%', margin: '-35px 0 16px 153px' }} src={`${BASE_URL}${imgg}`} />
                                            </div>
                                        </div>
                                        <div className="w-full flex flex-wrap ">
                                            <div className="sm:w-6/12">
                                                <Button className="order-btn " onClick={() => {
                                                    setSelecteditem(data)
                                                    toggleModal()
                                                }} style={{ marginTop: 2 }}>View My Order </Button>

                                            </div>
                                            <div className="sm:w-6/12 ">
                                                < ViewResult />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            })
                            :
                            <p className="text-center marginLeft">You have not entered any competitions yet, <br />please click <Link to="/bikes" style={{ color: 'blue' }}>here</Link> to go to   <span className="uppercase text-center">The Bike Competitions</span></p>
                        }

                    </div>
                </MainLayout>
            </div>
            {preOrders && <ModalExample showModal={isOpen} onClose={toggleModal} bikeDetails={selecteditem} />}
        </div>
    )
}

//export default connect(mapStateToProps, mapDispatchToProps)(MyCompition);
/* export default MyProfile;    */


const mapStateToProps = state => ({
    ...state
});


export default connect(mapStateToProps, null)(MyCompition);

