import React, { Component } from 'react'
import MyDetails from './MyDetails';
import { Link } from "react-router-dom";


export default class ProfileView extends Component {
   
    render() {
        return (
            <div>
                <div className="body" style={{ borderRadius: 20 }}>
                    <button className="uppercase text-center" style={{ borderTopLeftRadius: 20, borderTopRightRadius: 20, backgroundColor: '#000', color: '#fff', width: '100%', padding: '7px 0 7px 0', marginTop: '-1px' }}>My Acount</button>
                    <Link to="/my-details" className="text-color">
                        <button  className="uppercase mobile-tab" >
                            <span className="iconSpan">
                            <img className="icon" src="https://img.icons8.com/ios-filled/64/000000/dog-house.png" />My Details
                        </span>
                        </button>
                    </Link>
                   
                   {/*  <button  className="uppercase mobile-tab" >
                        <span className="iconSpan">
                            <img className="icon" src="https://img.icons8.com/ios-filled/64/000000/dog-house.png" />Saved Addresses
                        </span>
                    </button> */}
                    <Link to="/email-preferance" className="text-color">
                    <button  tabFor="vertical-tab-three" className="uppercase mobile-tab">
                        <span className="iconSpan">
                            <img className="icon" src="https://img.icons8.com/ios-filled/50/000000/paper-plane.png" />
                            Email Preferance
                        </span>
                    </button>
                    </Link>
                    <Link to="/my-compition" className="text-color">
                    <button  tabFor="vertical-tab-four" className="uppercase mobile-tab">
                        <span className="iconSpan">
                            <img className="icon" src="https://img.icons8.com/fluent-systems-filled/64/000000/trophy.png" /> Competition
                        </span>
                    </button>
                    </Link>
                   
                  {/*   <button  tabFor="vertical-tab-four" className="uppercase mobile-tab">
                        <span className="iconSpan">
                            <img className="icon" src="https://img.icons8.com/ios/48/000000/bank-cards--v1.png" />Saved payment details
                        </span>
                    </button> */}
                    <Link to="/help" className="text-color">
                    <button  tabFor="vertical-tab-five" className="uppercase mobile-tab" >
                        <span className="iconSpan">
                            <img className="icon" src="https://img.icons8.com/ios/64/000000/help.png" />Help
                        </span>
                    </button>
                    </Link>


                </div>
            </div>
        )
    }
}
