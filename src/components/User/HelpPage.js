import React, { Component } from 'react'
import { MainLayout } from '../../containers'
import { Link } from "react-router-dom";

export default class HelpPage extends Component {
    render() {
        return (
            <div>

                <div className="desktop-view w-100">

                    <div className="checkbox flex justify-center" >
                        <Link to="/about-us" className="linkStyle"> About</Link>
                    </div>
                    <div className="checkbox mt-5 flex justify-center ">
                        <Link to="/faq" className="linkStyle"> How it Works</Link>
                    </div>
                    <div className="checkbox mt-5 flex justify-center ">
                        <Link to="/faq" className="linkStyle">  FAQs</Link>
                    </div>
                    <div className="flex justify-center py-8">
                        <div>
                            <Link to="/contact-us">
                                <button className="uppercase custom-top" style={{ backgroundColor: '#000', color: '#fff', padding: ' 6px 27px', borderRadius: '20px' }} type="submit" >Send us a message</button>
                            </Link>
                        </div>
                    </div>

                </div>
                <div className="mobile-view w-100 ">
                    <MainLayout>
                        <div className="checkbox flex justify-center mt-6" >
                            <Link to="/about-us" className="linkStyle"> About</Link>
                        </div>
                        <div className="checkbox mt-5 flex justify-center ">
                            <Link to="/faq" className="linkStyle"> How it Works</Link>
                        </div>
                        <div className="checkbox mt-5 flex justify-center ">
                            <Link to="/faq" className="linkStyle">  FAQs</Link>
                        </div>
                        <div className="flex justify-center py-8">
                            <div>
                                <Link to="/contact-us">
                                    <button className="uppercase custom-top" style={{ backgroundColor: '#000', color: '#fff', padding: ' 6px 27px', borderRadius: '20px' }} type="submit" >Send us a message</button>
                                </Link>
                            </div>
                        </div>
                    </MainLayout>
                </div>


            </div>
        )
    }
}
