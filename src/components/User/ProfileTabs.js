import React from 'react';
import { Tab, TabPanel, Tabs, TabList } from "react-web-tabs";
import ReactDOM from "react-dom";
import "react-web-tabs/dist/react-web-tabs.css";
import './tabStyle.css';
import Compition from './Compition';
import EmailPreferance from './EmailPreferance';
import HelpPage from './HelpPage';
import MyDetails from './MyDetails';
import MyCompition from './MyCompition';


const ProfileTabs = ({ activeTab, userDetails, email_pref_2, email_pref_1, dispatch }) => {
 

  return (
    <Tabs
      defaultTab={!activeTab ? "vertical-tab-one" : "vertical-tab-three"}
      vertical className="vertical-tabs">
      <TabList>
        <Tab tabFor="vertical-tab-one" className="uppercase pull-left" style={{ borderTopLeftRadius: 13, fontSize: 32 }} >My Account</Tab>
        <Tab tabFor="vertical-tab-two-tab" className={"uppercase pull-left"}><span className="iconSpan"><img className="icon" src="https://img.icons8.com/ios-filled/64/000000/dog-house.png" />My Details</span></Tab>
        <Tab tabFor="vertical-tab-three" className="uppercase pull-left"><span className="iconSpan"><img className="icon" src="https://img.icons8.com/ios-filled/50/000000/paper-plane.png" />Email Preferences</span></Tab>
        <Tab tabFor="vertical-tab-four" className="uppercase pull-left"> <span className="iconSpan"><img className="icon" src="https://img.icons8.com/fluent-systems-filled/64/000000/trophy.png" />Competitions</span></Tab>
        <Tab tabFor="vertical-tab-five" className="uppercase pull-left" ><span className="iconSpan"><img className="icon" src="https://img.icons8.com/ios/64/000000/help.png" />Help</span></Tab>
      </TabList>

      <TabPanel tabId="vertical-tab-one" >
        <div className="head" style={{ borderTopRightRadius: 13 }}>
          <span className="iconSpan uppercase details"><img className="icon" src="https://img.icons8.com/ios-filled/64/000000/dog-house.png" /><label className="labelStyle">My Details</label></span>
        </div>

        <MyDetails userDetails={userDetails} />


      </TabPanel>

      <TabPanel tabId="vertical-tab-two-tab">
        <div className="head" style={{ borderTopRightRadius: 13 }}>
          <span className="iconSpan uppercase details" ><img className="icon" src="https://img.icons8.com/ios-filled/64/000000/dog-house.png" /><label className="labelStyle">My Details</label></span>
        </div>
        <div className="" style={{ border: 'none' }}>
          <MyDetails userDetails={userDetails} />
        </div>

      </TabPanel>

      <TabPanel tabId="vertical-tab-three">
        <div className="head" style={{ borderTopRightRadius: 13 }}>
          <span className="iconSpan uppercase email"><img className="icon" src="https://img.icons8.com/ios-filled/50/000000/paper-plane.png" /><label className="labelStyle">Email Preferences</label></span>
        </div>
        <div className="body" style={{ border: 'none' }}>
          <EmailPreferance dispatch={dispatch} email_pref_1={email_pref_1} email_pref_2={email_pref_2} />
        </div>

      </TabPanel>
      <TabPanel tabId="vertical-tab-four">
        <div className="head" style={{ borderTopRightRadius: 13 }}>
          <span className="iconSpan uppercase competition"><img className="icon" src="https://img.icons8.com/fluent-systems-filled/64/000000/trophy.png" /><label className="labelStyle">My Competitions</label></span>
        </div>


        <MyCompition />


      </TabPanel>
      <TabPanel tabId="vertical-tab-five">
        <div className="head" style={{ borderTopRightRadius: 13 }}>
          <span className="iconSpan iconSpanhelp uppercase"><img className="icon" src="https://img.icons8.com/ios-filled/64/000000/help.png" /><label className="labelStyle">Help</label></span>
        </div>
        <div className="body" style={{ border: 'none' }}>
          <HelpPage />
        </div>

      </TabPanel>
    </Tabs>
  );
}

export default ProfileTabs;