import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { MainLayout } from "../../containers";
import { updateEmailPref } from "../../redux/actions";

class EmailPreferance extends Component {
  constructor(props) {
    super(props);
    const {email_pref_2,email_pref_1} = props;
    this.state = {
      email_preferance_1: email_pref_1,
      email_preferance_2: email_pref_2,
    }; //updateEmailPref
  }
  setPref = (details) =>{
  const { updateEmailPref$,auth:{user},dispatch } = this.props; 
  const { email_preferance_1, email_preferance_2} = this.state;

  const userData =  user;
  let tmpData = userData;
  //tmpData.email_preferences_1 = email_preferance_1
  //tmpData.email_preferences_2 = email_preferance_2 
  dispatch({ type: "AUTH_LOGIN", payload:{...tmpData,...details} });  

  console.log('user.id',details);
  updateEmailPref$({ ...details,email:userData.email },
    ()=>{ 
       // console.log("My Call back");
      //  window && window.location.reload() 
     //return <Redirect to={'/my-profile/2'}/>
     window.location.replace('/my-profile/2')
    })
}
  render() {
      const {auth:{user}} = this.props
     return (
      <div>
        <div className="desktop-view">
          <div className="checkbox marginLeft">
            <label className="checkboxMargin">
              <input
                type="checkbox"
                checked={user && user.email_preferences_1  && user.email_preferences_1 === "1"}
                onChange={() => {
                  this.setState({ email_preferance_1: !(user && user.email_preferences_1  && user.email_preferences_1 === "1") },
                    ()=>this.setPref(
                      {email_preferance_1:!(user && user.email_preferences_1  && user.email_preferences_1 === "1"),
                      email_preferance_2:(user && user.email_preferences_2  && user.email_preferences_2 === "1")
                    })
                    );
                  //DISPTACH YOUR ACTION TO REDUX.
                 
                }}
              />
              I would like to receive email updates from The Bike Comp
            </label>
          </div>
          <div className="checkbox mt-5 marginLeft">
            <label className="checkboxMargin">
              <input
                type="checkbox"
                checked={user && user.email_preferences_2  && user.email_preferences_2 === "1"}
                name="email_preferance_2"
                onChange={() => {
                  this.setState({ email_preferance_2: !( user && user.email_preferences_2  && user.email_preferences_2 === "1")},
                    ()=>this.setPref(
                      {email_preferance_1:(user && user.email_preferences_1  && user.email_preferences_1 === "1"),
                      email_preferance_2:!(user && user.email_preferences_2  && user.email_preferences_2 === "1")
                    }
                    )  
                    );
                  
                }}
              />
              I would like to subscribe to The Bike Comp for notifications about
              competition updates, new bikes and accessories added.
            </label>
          </div>
          <div className="checkbox mt-5 marginLeft">
            <small className="mt-5">
              <i>
                Please tick/untick according to your personal preferences. We do
                not share your personal data with any third parties
              </i>
            </small>
          </div>
        </div>
        <div className="mobile-view w-100">
          <MainLayout>
            <div
              style={{
                marginLeft: "24%",
                textAlign: "justify",
                marginRight: "5%",
              }}
              className="mt-4"
            >
              <div className="checkbox">
                <label>
                  <input type="checkbox" value="option1" />I would like to
                  receive the email updates from The Bike Comp
                </label>
              </div>
              <div className="checkbox mt-5">
                <label>
                  <input type="checkbox" value="option1" />I would like to
                  subscribe The Bike Comp for notification
                </label>
              </div>
              <p className="mt-5">
                Please tick/untick according to your personal Preferance. We do
                not share your personal data with any third party
              </p>
            </div>
          </MainLayout>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  ...state,
});

const MapDispatchToProps = { 
  updateEmailPref$: updateEmailPref,
};
export default connect(mapStateToProps, MapDispatchToProps)(EmailPreferance);
