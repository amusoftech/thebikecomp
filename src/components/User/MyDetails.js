import React  from 'react';
import { MainLayout } from '../../containers';
import { connect, useDispatch  } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';


const MyDetails = (props) => {

    const userData = props && props.auth && props.auth.user
    const history= props && props.history 
    const dispatch = useDispatch()


    const data = userData//userDetails.userDetails.userDetails;

    return (
        <div>
            <div className="flex flex-wrap desktop-view">
                <div className="flex flex-wrap  mt-6">
                    <div className="sm:w-6/12 p-5  mt-6 sm:mt-0" >

                        <p className="text-sm marginLeft"><b>Name:</b></p>
                        <p className="text-sm marginLeft">{data && data.name}</p>
                        <p className="text-sm marginLeft mt-4"><b>Surname:</b></p>
                        <p className="text-sm marginLeft">{data && data.surname}</p>
                        <p className="text-sm marginLeft mt-4"><b>Email:</b></p>
                        <p className="text-sm marginLeft">{data && data.email}</p>
                        <p className="text-sm marginLeft mt-4"><b>Phone Number :</b></p>
                        <p className="text-sm marginLeft">{data && data.phone}</p>
                        <p className="text-sm marginLeft mt-4"><b>Address :</b></p>
                        <p className="text-sm marginLeft">{data && data.address}</p>
                       
                    </div>
                    <div className="sm:w-6/12 p-5  mt-6 sm:mt-0">
                        <Link to="/update-profile" style={{ color: '#9f9eef' }}>Edit account details</Link><br /><br />
                        <a onClick={()=>{
                            history.push('/reset-password')
                        }}  className=" mt-8" style={{ color: '#9f9eef' }}>Change my password</a>

                    </div>
                </div>
            </div>
            <div className="mobile-view w-100">
                <MainLayout>
                    <div style={{ marginLeft: '24%', textAlign: 'justify', marginRight: '5%' }} className="mt-4">
                        <p className="text-sm mt-6"><b>Name:</b></p>
                        <p className="text-sm mt-6">{data && data.name}</p>
                        <p className="text-sm mt-6"><b>Surname:</b></p>
                        <p className="text-sm mt-6">{data && data.surname}</p>
                        <p className="text-sm mt-6"><b>Email:</b></p>
                        <p className="text-sm mt-6">{data && data.email}</p>
                        <p className="text-sm marginLeft mt-4"><b>Phone Number :</b></p>
                        <p className="text-sm marginLeft">{data && data.phone}</p>
                        <p className="text-sm marginLeft mt-4"><b>Address :</b></p>
                        <p className="text-sm marginLeft">{data && data.address}</p>
                       

                        <div className="mt-6">
                        <Link to="/update-profile" style={{ color: '#9f9eef' }}>Edit account details</Link><br /><br />
                        <Link to="/reset-password" className=" mt-8" style={{ color: '#9f9eef' }}>Change my password</Link>
                        </div>

                    </div>
                </MainLayout>
            </div>
        </div>
    )
}

const mapStateToProps = state => ({
    ...state
});



export default connect(  mapStateToProps, null)(withRouter(MyDetails));
