import React, {  useState, useEffect } from 'react'
import { Button } from 'react-bootstrap'
import Modal from 'react-modal';
import { connect, useDispatch, useSelector } from 'react-redux';
import { getPreviousWinnder } from '../../redux/actions';
import { BASE_URL } from '../../utils/common.utils';
  
const customStyles = {
    overlay: {
        backgroundColor: 'rgb(0 0 0 / 35%)',

        zIndex: 10,
    },
    content: {
        top: '50%',
        left: '50%',
        width: '50%',
        height: 450,
        overflowY: 'atuo',
        right: 'auto',
        bottom: 'auto',
        marginRight: '-50%',
        transform: 'translate(-50%, -50%)',
        backgroundColor: '#fff',
        border: '1px solid gray',
        padding: 0,
    }
}


export const ViewResult = (winner) => {

    const [showModal, setShowModal] = useState(false);
    const onClose = () => setShowModal(false);

    const dispatchWinnder = useDispatch();
    const previousOrder =
        useSelector((state) => state.preOrderReducer.previousOrders) || [];

    useEffect(() => {
        dispatchWinnder(getPreviousWinnder());

    }, []);

   // console.log("previousOrder", previousOrder);
    const imgg = previousOrder.winners_bike_image;



    return (
        <>
            <Button className="order-btn width" style={{ marginLeft: '22%', width: '120px' }} onClick={() => { setShowModal(true); }}>View Result</Button>
            {/*  <Button variant="btn btn-primary" onClick={()=>{ setShowModal(true); }}>
              Launch demo modal
            </Button> */}

            <Modal
                isOpen={showModal}
                onRequestClose={onClose}
                style={customStyles}
                contentLabel='Example Modal'
                ariaHideApp={false}
            >
                <Button onClick={onClose} className="closeIcon" >
                    <img src="https://img.icons8.com/metro/100/000000/cancel.png" />
                </Button>

                {previousOrder ?
                <div>
                    <div className="head">
                    <span className="uppercase text-center" style={{ marginLeft: '28%' }}>The Bike Competition :{previousOrder.competition_month} 2020</span>
                </div>
                <h3 className=" text-center mt-6">This months winner is : <span style={{ color: '#75b871' }}>{previousOrder.winners_first_name} {previousOrder.winners_last_name}</span></h3>
                <p className=" text-center"> Congratulations to John on winning a brand new{previousOrder.winners_first_name} ORBEA Gain F30</p>
                <div className="flex justify-center ">
                    <div>

                        <img style={{ width: '120px' }} src={`${BASE_URL}${imgg}`} />
                    </div >
                </div>
                <p className=" text-center"> For a full list of monthly winners and runners-up please visit:</p>
                <div className="flex justify-center ">
                    <div >
                        <button className="uppercase text-center" style={{ backgroundColor: '#000', color: '#fff', padding: ' 6px 27px', borderRadius: '20px' }} type="submit" >previous winners </button>
                    </div>
                </div>
                </div>
                    : 
                    <p className=" text-center"> Result not Found</p>
                    }


            </Modal>
        </>

    );

}

const mapStateToProps = state => ({
    ...state
});


export default connect(mapStateToProps, null)(ViewResult);