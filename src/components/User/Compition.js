import React, { Component } from "react";
import { connect } from "react-redux";
import { BASE_URL } from "../../utils/common.utils";
const Compition = ({ bikeDetails }) => {
  let payload = (bikeDetails && bikeDetails.payload) || [];

  const {
    bike,
    tickets,
    unitTicketPrice,
    discount,
    retail_price,
    cash_alternative,
  } = bikeDetails.payload && JSON.parse(bikeDetails.payload);
  const { id } = bikeDetails;

  const order =
    bikeDetails && bikeDetails.payload && JSON.parse(bikeDetails.payload);
  console.log("bikeDetails >>", order);

  const renderRows = (bike, i,amount) => {
   
    const { title,brand_name,model,thumbnail_1,price,total_tickets } = bike;
    return (
      <tr key={i}>
        <td>100{id && id}</td>
    <td  style={{ borderBottom: "1px solid",borderRight:'none',width:'50%'}}>{brand_name} {model}</td>
        <td style={{ borderBottom: "1px solid",borderLeft:'none'}}>
          <img style={{ width: '50%' }} src={`${BASE_URL}${thumbnail_1}`} />
        </td>
       <td>{amount}</td>
        <td>{price}</td>
      </tr>
    );
  };
  return (
    <div>
      <table>
        <thead>
          <tr>
            <th>Order No.</th>
            <th colSpan="2">Bike Selected</th>
           <th>Quantity</th> 
            <th>Ticket Price</th>
           {/*  <th>Total</th> */}
          </tr>
        </thead>
        <tbody>
          {order ? (
            order.map((data, i) => {
              return renderRows(data.bike ,i,data.amount);
            })
          ) : (
              <p className="text-center marginLeft">
                You have not entered any competitions yet,
                <span className="uppercase text-center">
                  The Bike Competitions
              </span>
              </p>
            )}
          <tr>

            <td colSpan="3"  style={{ border: "none" }}>
            <b>Grand Total</b>
            </td>
            <td style={{ border: "none" }}><b>{bikeDetails.total_tickets}</b></td>
           {/*  <td>{bike && bike.price}</td> */}
            <td style={{ border: "none" }}><b>{bikeDetails.total_amount}</b></td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};
const mapStateToProps = (state) => ({
  ...state,
});

export default connect(mapStateToProps, null)(Compition);
