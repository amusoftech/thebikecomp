import React from 'react';
import { Link } from 'react-router-dom';

import { HowToItem } from '../../../components';
import styles from './style.module.scss';

const HowItWorks = () => {
    return (
        <section className="py-10">
            <h3 className="text-center font-bold uppercase">How does it work?</h3>
            <div className="flex flex-wrap justify-around">
                <HowToItem
                    image="/images/how-it-works-number-1.jpg"
                    textTop="TICKETS FROM 20P"
                    textBottom="Select from over 80 Bicycles" />
                {/*  <HowToItem
                    image="/images/how-it-works-number-2.jpg"
                    textTop="USE YOUR KNOWLEDGE"
                    textBottom={<span>Judge the <b>Distance</b> and <b>Height</b> jumped</span>} /> */}
                <HowToItem
                    image="/images/how-it-works-number-2.jpg"
                    textTop="USE YOUR KNOWLEDGE"
                    textBottom="Answer the skill based question" />
                {/*  <HowToItem
                    image="/images/how-it-works-number-3.jpg"
                    textTop="MONTHLY WINNER"
                    textBottom={<span>Prizes worth up to &pound;7500</span>} /> */}
                <HowToItem
                    image="/images/how-it-works-number-3.jpg"
                    textTop="MONTHLY WINNER"
                    textBottom="Watch our Live Draw to see who wins" />
            </div>
            <div className="flex flex-wrap justify-around">
            
            <div className="w-full py-2 static_padding" >
                    <div className={styles.btnContainer}>

                        {/* <Link to="/bikes"><button className={styles.enter}>Enter</button></Link> */}
                        <ol>
                            <li>There currently is only one Monthly Competition. Click the enter button to see which Bikes and E-Bikes we have available</li><br/>
                            <li>Browse and Select your Bike Comp Tickets (limited to 100 per person).</li><br/>
                            <li>Proceed to play from your shopping cart.</li><br/>
                            <li>You will be asked to create an account (new user) or Log in (for existing users).</li><br/>
                            <li>Use your Knowledge to answer the Skill question. (Only correct answers will be eligible for valid entry, The Bike Comp is a Prize Draw and NOT a raffle or lottery).</li><br/>
                            <li>Proceed to checkout where you can use our trusted payment gateways STRIPE or PAYPAL to make a secure payment</li><br/>
                            <li>You will receive a confirmation email when your purchase is complete.</li><br/>
                            <li>Once the competition is closed, we will give every valid entry a randomised number and  then a random selection is made by the Random wheel software to decide the winner.</li><br/>
                            <li>10 runners up will get all their entries re-entered into the following draw for free. Runners up will be the first five above and below the selected winner.</li><br/>
                            <li>The winner will be notified by email.</li>
                        </ol>
                    </div>
                </div>
                
            </div>


            <div className={styles.btnContainer}>
                <Link to="/bikes"><button className={styles.enter}>Enter</button></Link>
            </div>

            <div>

            </div>


        </section>
    );
}

export default HowItWorks;