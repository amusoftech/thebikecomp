import React from 'react';

const HowToItem = ({ image, textTop, textBottom }) => (
    <div className="w-full md:w-1/4 py-6">
        <h4 className="text-center font-semibold">{textTop}</h4>
        <img className="mx-auto my-3" src={image} alt="How it works - The Bikes Comp" />
        <h5 className="text-center">{textBottom}</h5>
    </div>
)

export default HowToItem;