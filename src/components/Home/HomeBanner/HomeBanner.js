import React from 'react';
import { Link } from 'react-router-dom';

import styles from './style.module.scss';

const HomeBanner = () => {

    return (

        <section className={styles.section}>
            <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="708daa26-ca2a-4fc5-91b7-8b4af55dfca7" data-blockingmode="auto" type="text/javascript"></script>
            <img className={styles.image} src="images/banner.jpg" alt="Home Banner" />
            <div className={styles.btnContainer}>
                <h1 className="text-center uppercase font-bold" style={{color:'#fff'}}>Win A Bike, Give A Bike</h1>
                <h3 className="text-center mb-2 uppercase font-semibold" style={{color:'#fff'}}>We Donate A Bike With every Bike WON</h3>
                <p className="text-center"><Link to="/bikes"><button className={styles.enter}>Enter</button></Link></p>
            </div>
        </section>
    );
}

export default HomeBanner;