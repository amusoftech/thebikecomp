import React from 'react';

import { Title1, Title2, Indent1, Indent2, LINK } from './Widgets';

const TOC = () => (
    <section class="px-3">
        <Title1  content="1. Qualifying Persons" />
        <Title2  content={<>1.1 The Bike Comp ('Promoter', 'our(s)') operate skilled prize competitions resulting in the allocation of prizes in accordance with these Terms and Conditions on the website <LINK to="www.thebikecomp.com" /> (the 'Website') - (the 'Competition(s)'). </>} />
        <Title2 content="1.2 The Competitions are open to all persons aged 16 and over and the age of majority in their country of residence excluding the Promoter's employees or members of their immediate family, agents or any other person who is connected with the creation or administration of our Competitions. " />

        <Title1 content="2. Legal Undertaking" />
        <Title2 content="2.1 By entering a Competition the entrant ('Entrant', 'you', 'your(s)') will be deemed to have legal capacity to do so, you will have read and understood these Terms and Conditions and you will be bound by them and by any other requirements set out in any related promotional material." />
        <Title2 content="2.2 These Competitions are governed by English Law and any matters relating to the Competition will be resolved under English Law and the Courts of England shall have exclusive jurisdiction." />
        <Title2 content="2.3 In the event that you participate in a Competition online via the Website, and by accepting these Terms and Conditions you confirm that you are not breaking any laws in your country of residence regarding the legality of entering our Competitions. The Promoter will not be held responsible for any Entrant entering any of our Competitions unlawfully. If in any doubt you should immediately leave the Website and check with the relevant authorities in your country. " />

        <Title1 content="3. Competition Entry" />
        <Title2 content="3.1 These Competitions may be entered online via the Website. There are two categories of Competitions: Dream Bike Competitions and Dream E-Bike Competitions. One or more Competitions may be operated at the same time and each Competition will have specific prize options. " />
        <Title2 content="3.2 Availability and pricing of Competitions and tickets is at the discretion of the Promoter and will be specified at the point of sale on the Website. " />
        <Indent1 content="5+, 10+ and 20+ ticket price categories are available in the Dream Bike Competitions and Dream E-Bike Competitions  and will not take into account the number of bike tickets and e-bike tickets already purchased in a given Dream Bike competition period. Pricing will not be applied retrospectively to orders already completed." />
        
        <Title2 className="font-semibold" content="3.3 Your The Bike Competition Account(s)" />
        <Indent1 content="In order to enter a Competition, you will need to register an account with us. " />
        <Indent1 content={<>(a) You can register an account online at <LINK to="www.thebikecomp.com"/>.</>} />
        <Indent2 content="(i) To register an account online you will be asked to provide an email address or sign in via a social media account, such as Facebook, Twitter or Google (‘Social Media Account’) " />
        <Indent1 content='(b) Please note that your email address or Social Media Account will also be the username that you use to log in to your account. Each account can only have one username attributed to it at any given time ("The Bike Comp Account"). Therefore, you cannot attribute multiple email addresses, or Social Media Accounts to your The Bike Comp Account.' />
        <Indent1 content="For example: " />
        <Indent2 content="(i) You cannot have an email address and a Social Media Account attributed to your The Bike Comp Account. " />
        <Indent2 content="(ii) You cannot have two or more email addresses attributed to your The Bike Comp Account." />
        <Indent1 content="You can however, change your username for your The Bike Comp Account by logging into your The Bike Comp Account. For example, you can create your The Bike Comp Account using one email address and at a later date change your username for your The Bike Comp Account to another email address. However, if you create multiple accounts using different email addresses or Social Media Accounts, each username will be treated as a separate The Bike Comp Account. " />

        <Title2 content="3.4 When playing a Competition online via the Website, follow the on-screen instructions to: " />
        <Indent1 content="(a) select the Competition(s) you wish to enter, choose your tickets and use your skill to answer the question(s) in accordance with 3.4(d) and (e), or the Multiple Choice Challenge in accordance with 3.4 (f) and (e); " />
        <Indent1 content="(b) and when you are ready to purchase your Ticket(s), provide your contact and payment details. You will need to check your details carefully and tick the declaration at checkout, confirming you have read and understood the Competition Terms and Conditions; " />
        <Indent1 content="(c) once your payment has cleared we will email you to confirm your entry into the Competition. Please note that when entering online you will not be deemed entered into the Competition until we confirm your Ticket(s) order back to you by email. " />

        <Indent1 content="(d) for each The Bike Competition, or The E-Bike Competition Ticket that you have purchased, you will need to complete a video challenge onscreen, which shall operate as follows: " />
        <Indent2 content="(i) you will be shown a sporting video which will not contain the speed of the cyclist; " />
        <Indent2 content="(ii) using all the information shown in the sporting video displayed on the computer screen use your skill and judgement to correctly identify the speed the cyclist is travelling which you consider the Judge will decide, is the most likely speed the cyclist is travelling ; " />
        <Indent2 content="(iii) click on the screen to input your answer in the answers box. The computer will register speed as your entry into the Competition ('Confirm the Speed Challenge'); " />

        <Indent1 content="(e) you may repeat the process for the Confirm the Speed Challenge for as many entries as you wish to make. You will require one Ticket for each entry. " />

        <Indent1 content="(f) for each The Bike Comp or The E-Bike Comp ticket that you have purchased, you will need to do as follows: " />
        <Indent2 content="(i) you will be shown a multiple-choice quiz question, which has one only correct answer; "/>
        <Indent2 content="(ii) you will need to use your skill and judgement to identify the correct answer out of a choice of potential answers; " />
        <Indent2 content="(iii) select the correct answer on the screen. The computer will register this answer as your entry into the The Bike Comp (‘Multiple Choice Challenge’) " />
        <Indent2 content="(iv) you may select as many entries for the Multiple Choice Challenge as you wish to make. You will require one Ticket for each entry. Only one Multiple Choice answer may apply to all the tickets in any single Multiple Choice Challenge order. ('Website Entries' referred to as 'Entr(y)(ies)') " />
        
        <Title2 content="3.5 The Promoter reserves the right to refuse or disqualify any incomplete Entries if it has reasonable grounds for believing that an Entrant has contravened any of these Terms and Conditions. " />
        <Title2 content="3.6 To the extent permitted by applicable law, all Entries become our property and will not be returned. " />
        <Title2 content={<>3.7 <b>Bike and E-Bike Competitions</b>: Entries (tickets) are limited to one hundred and fifty (150) per person, per Competition. Abuse of this limit will not be tolerated and if the Promoter has reasonable grounds to believe that an individual is exceeding this limit, for example by entering a Competition using multiple accounts, it reserves the right at its sole discretion to void any orders or to award any prize to the next closest entrant. In addition, if any person using a single account exceeds 150 entries in a single Competition for any reason, the Promoter reserves the right at its sole discretion, to void any tickets without notice firstly on a chronological basis with respect to orders, and then on a sequential basis with respect to ticket references contained within an order, so as to void any tickets exceeding the first 150 paid tickets entered into the Competition. </>} />
    </section>
);

export default TOC;