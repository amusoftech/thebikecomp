import React from 'react';
import styles from './style.module.scss';

export const Title1 = ({ content, className }) => (
    <>
        <p className={`${styles.title1} ${className}`}>{content}</p>
    </>
);

export const Title2 = ({ content, className }) => (
    <>
        <p className={`${styles.title2} ${className}`}>{content}</p>
    </>
);

export const Indent1 = ({ content }) => (
    <>
        <p className={`pl-6 lg:px-12 mb-2 ${styles.indent1}`}>{content}</p>
    </>
);

export const Indent2 = ({ content }) => (
    <>
        <p className={`pl-12 lg:px-20 mb-2 ${styles.indent2}`}>{content}</p>
    </>
);

export const LINK = ({ to, label }) => {
    const getFullURL = (url) => {
        if (url.includes('http') && url.includes('//')) {
            return url;
        } else {
            return 'https://' + url;
        }
    }
    return (
        <>
            <a className={styles.link} href={getFullURL(to)}  rel="noopener noreferrer" target="_blank">{label || to}</a>
        </>
    );
}
  