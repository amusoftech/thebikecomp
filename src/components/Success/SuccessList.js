import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';


import SuccessItem from './SuccessItem';

import { getTicketPrice, getUnitTicketPrice } from '../../utils/common.utils';
import { setMenuLayer } from '../../redux/actions';

import styles from './style.module.scss';


const SuccessList = ({ items }) => {


    useEffect(() => {

        // check session id
        const search = window.location.search;
        const params = new URLSearchParams(search);
        const session_id = params.get('session_id');



        return () => {

        };
    })

    /* const checkoutStripe = async () => {

    }

    const checkoutPayPal = () => {

    }

    const getDiscount = (item) => {
        let discount = 0;
        let tickets = item.ticketSubmitIndex;
        if (tickets >= 50) {
            discount = Number(item.bike.price_50);
        } else if (tickets >= 20) {
            discount = Number(item.bike.price_20);
        } else if (tickets >= 10) {
            discount = Number(item.bike.price_10);
        } else if (tickets >= 5) {
            discount = Number(item.bike.price_5);
        }
        return discount;
    } */
    console.log("items", items);

    /*  const totalTicketCount = () => {
         let total = 0;
         for (let item of items) {
             total += item.ticketSubmitIndex;
         }
         return total;
     } */

    /* const totalPrice = () => {
        let total = 0;
        for (let item of items) {
            let price = getUnitTicketPrice(item.bike, Number(item.ticketSubmitIndex)) * Number(item.ticketSubmitIndex);
            total += price;
            //console.log(price, total);
        }
        return total.toFixed(2);
    }
 */
    /* const composePurchaseItemData = () => {
        let data = [];
        for (let item of items) {
            if (item.ticketSubmitIndex === 0) continue;
            item.unitTicketPrice = getUnitTicketPrice(item.bike, item.ticketSubmitIndex);
            item.discount = getDiscount(item);
            data.push(item);
        }
        return data;
    } */

    return (
        <div className={styles.listContainer}>
            {/* {totalTicketCount() > 0 && (
                <>
                    <div className={styles.listHeader}>
                        <div className={styles.entryCol}>Your Entries</div>
                        <div className={styles.priceCol}>Price</div>
                        <div className={styles.amountCol}>Amount</div>
                        <div className={styles.actionCol}></div>
                    </div>

                    {items.filter(itm => itm.ticketSubmitIndex > 0).map((item, i) => (
                        <SuccessItem item={item} key={i} />
                    ))
                    }

                    <div className={styles.listFooter}>
                        <p>All Tickets: {totalTicketCount()}</p>
                        <p>Total: &pound;{totalPrice()}</p>
                    </div>
                </>
            )
            }

            {
                totalTicketCount() == 0 && (
                    <p className="text-center text-lg">No tickets to checkout!</p>
                )
            } */}

            <div className={`flex justify-center mt-6 ${styles.payButtonCon}`}>
                <Link to="/">
                    <span style={{
                        backgroundColor: "#000",
                        color: "#fff",
                        padding: "11px 18px",
                        borderRadius: "20px",
                    }}>&lt;&lt; Back To Home</span>
                </Link>
            </div>
        </div>
    );
}

const mapStateToProps = state => ({
    appLoaded: state.setting.appLoaded,
});

const mapDispatchToProps = {
    setMenuLayer$: setMenuLayer,
}

export default connect(mapStateToProps, mapDispatchToProps)(SuccessList);
