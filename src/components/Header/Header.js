import React, { useEffect, useContext } from 'react';
import { Link } from 'react-router-dom';
import { connect, useDispatch, useSelector } from 'react-redux';

import { MXSection } from '../../containers';
import ShoppingCart from '../ShoppingCart/ShoppingCart';
import { CartIcon, UserIcon } from '../Icons';
import { logout, toggleShoppingCart, toggleMenu, getSiteCMS } from '../../redux/actions';
import { Dropdown } from 'react-bootstrap';
import { deleteALoginUserDetails } from '../../utils/common.utils'


import styles from './style.module.scss';
import LoginMenu from './LoginMenu';
import { AUTH_REGISTER } from '../../constants/redux.contants';
import { AuthContext } from '../../utils/Auth';




const Header = ({ loggedIn, user, setting, logout$, toggleMenu$, toggleShoppingCart$ }) => {
    const { currentUser } = useContext(AuthContext);

    const dispatch = useDispatch()

    const toggleMenuStatus = (toStatus = null) => {
        toggleMenu$(toStatus);
    }

    const toggleCart = (toStatus = null) => {
        toggleShoppingCart$(toStatus);
    }

    const handleLogout = () => {
        deleteALoginUserDetails()
        logout$();
    }
    useEffect(() => {
        //mark user logged in

        currentUser && dispatch({ type: AUTH_REGISTER, payload: { ...currentUser } })
    }, [currentUser])

    const dispatchCMS = useDispatch()
    const site_details = useSelector(state => state.preOrderReducer.sitecms);
    //const site_details = useSelector(state => state.preOrderReducer.sitecms);
    useEffect(() => {
        dispatchCMS(getSiteCMS())
    }, []);
    const siteData = site_details[0];
    //console.log("Title",siteData && siteData.site_title );

    //console.log("context",currentUser);    
    return (
        <MXSection className={`bg-bg text-gray-100 ${setting.stickHeader ? styles.stickyHeader : ""}`}>
            <div className={styles.outerWrapper}>

                <div className={styles.menuHamburg} onClick={() => toggleMenuStatus(null)}>
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div className={styles.logoContainer}>
                    <Link to="/">
                        <img className={styles.logo} src="/logo192-white.png" alt="Logo of The Bike Comp" />
                    </Link>
                    <Link to="/">
                        <label className={styles.appName}>The Bike Comp</label>
                    </Link>
                </div>
                <div className={styles.menuContainer}>
                    <ul className={styles.menuList}>
                        <li className={styles.menuItem}><Link to="/">HOME</Link></li>
                        <li className={styles.menuItem}><Link to="/bikes">BIKES</Link></li>
                        <li className={styles.menuItem}><Link to="/about-us">ABOUT</Link></li>
                    </ul>
                </div>
                <div className={styles.authContainer}>

                    {!loggedIn && <Link to="/signup"><button className="bg-transparent hover:bg-gray-100 text-white hover:text-gray-800 text-10px font-normal focus:outline-none
                        mr-2 py-1 px-5 border-2 border-grey-100 hover:border-transparent rounded transition-colors transition-200">SIGN UP</button></Link>}
                    {!loggedIn && <Link to="/login"><button className="bg-transparent hover:bg-gray-100 text-white hover:text-gray-800 text-10px font-normal focus:outline-none
                        mx-2 py-1 px-5 border-2 border-grey-100 hover:border-transparent rounded">LOG IN</button></Link>}
                    {loggedIn && <span>Hi, {`${user.name}`}</span>}

                    {loggedIn && <LoginMenu handleLogout={handleLogout} />}

                    <CartIcon className={styles.cartIcon} onClick={() => toggleCart(null)} />
                </div>
                <CartIcon className={styles.mobileCartIcon} onClick={() => toggleCart(null)} />
            </div>
            <ShoppingCart show={setting.cartOpen} />
        </MXSection>
    );
}

const mapStateToProps = state => ({
    setting: state.setting,
    loggedIn: state.auth.login,
    user: state.auth.user,
});

const MapDispatchToProps = {
    logout$: logout,
    toggleMenu$: toggleMenu,
    toggleShoppingCart$: toggleShoppingCart
};

export default connect(mapStateToProps, MapDispatchToProps)(Header);