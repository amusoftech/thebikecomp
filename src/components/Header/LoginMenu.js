import React, { Component } from "react";
import './style.css';
import { CartIcon, UserIcon } from '../Icons';
import styles from './style.module.scss';
import { Link, withRouter, useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { logout } from "../../redux/actions";
import { deleteALoginUserDetails } from "../../utils/common.utils";
import { AUTH_LOGOUT } from "../../constants/redux.contants";

class LoginMenu extends Component {


  constructor() {
    super();

    this.state = {
      displayMenu: false,
    };

    this.showDropdownMenu = this.showDropdownMenu.bind(this);
    this.hideDropdownMenu = this.hideDropdownMenu.bind(this);

  };

  showDropdownMenu(event) {
    event.preventDefault();
    this.setState({ displayMenu: true }, () => {
      document.addEventListener('click', this.hideDropdownMenu);
    });
  }

  hideDropdownMenu() {
    this.setState({ displayMenu: false }, () => {
      document.removeEventListener('click', this.hideDropdownMenu);
    });

  }


  render() {
    //console.log("this.p", this.props.history);
    return (
      <div className="dropdown"  >
        <div className="button" onClick={this.showDropdownMenu}> <UserIcon className={styles.userIcon} /> </div>
        {this.state.displayMenu ? (
          <ul className="dropdownList">
            <li><Link to="/my-profile">My Acount</Link></li>
            <li><span onClick={() => {

              deleteALoginUserDetails()
              this.props.logout$()
              this.props.history.push('/')
              window.location.href = "/";
              /*  history.push("/home"); */
            }} className="bg-transparent text-white hover:text-gray-200 text-sm font-normal focus:outline-none cursor-pointer
                       " >Log out</span></li>

          </ul>
        ) :
          (
            null
          )
        }

      </div>
    );
  }
}

/* export default LoginMenu; */

const mapStateToProps = state => ({
  loggedIn: state.auth.login,
  user: state.auth.user,
});

const MapDispatchToProps = {
  logout$: logout
};

export default connect(mapStateToProps, MapDispatchToProps)(withRouter(LoginMenu));

