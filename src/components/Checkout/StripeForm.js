import React from 'react';
import {
    CardElement, Elements, useElements, useStripe, CardNumberElement,
    CardCvcElement,
    CardExpiryElement
} from '@stripe/react-stripe-js';
import { loadStripe } from '@stripe/stripe-js';


const stripePromise = loadStripe('pk_test_5vZPRXlthCAArZOOovZnK7JT00txj5drRX');
const options = {
    style: {
        base: {
            fontSize: "16px",
            color: "#424770",
            letterSpacing: "0.025em",
            fontFamily: "Source Code Pro, monospace",
            "::placeholder": {
                color: "#aab7c4"
            }
        },
        invalid: {
            color: "#9e2146"
        }
    }
};

const StripeForm = () => {
    const stripe = useStripe();
    const elements = useElements();

    const handleStripeSubmit = async (event) => {
        event.preventDefault();
        if (!stripe || !elements) {
            // Stripe.js has not loaded yet. Make sure to disable
            // form submission until Stripe.js has loaded.
            console.log('[Not ready]');
            return;
        }
        stripe.redirectToCheckout({
            sessionId: "cs_test_NQN6apihIb7GgZ4RfEtbC0hDms9tp59RHdYrR6RmxTiSDySgw72SQxR7"
        })
        .then(function(result) {
            console.log('[stripe Result]', result);
        });

        // const cardElement = elements.getElement(CardNumberElement); console.log('[Card Element]', cardElement);
        // // Use your card Element with other Stripe.js APIs
        // const { error, paymentMethod } = await stripe.createPaymentMethod({
        //     type: 'card',
        //     card: cardElement,
        // });

        // if (error) {
        //     console.log('[error]', error);
        // } else {
        //     console.log('[PaymentMethod]', paymentMethod);
        //     stripe.
        // }
    }
    return (
        <form onSubmit={handleStripeSubmit}>
            <label>
                Card number
                <CardNumberElement
                    options={options}
                    onReady={() => {
                        console.log("CardNumberElement [ready]");
                    }}
                    onChange={event => {
                        console.log("CardNumberElement [change]", event);
                    }}
                    onBlur={() => {
                        console.log("CardNumberElement [blur]");
                    }}
                    onFocus={() => {
                        console.log("CardNumberElement [focus]");
                    }}
                />
            </label>
            <label>
                Expiration date
                <CardExpiryElement
                    options={options}
                    onReady={() => {
                        console.log("CardNumberElement [ready]");
                    }}
                    onChange={event => {
                        console.log("CardNumberElement [change]", event);
                    }}
                    onBlur={() => {
                        console.log("CardNumberElement [blur]");
                    }}
                    onFocus={() => {
                        console.log("CardNumberElement [focus]");
                    }}
                />
            </label>
            <label>
                CVC
                <CardCvcElement
                    options={options}
                    onReady={() => {
                        console.log("CardNumberElement [ready]");
                    }}
                    onChange={event => {
                        console.log("CardNumberElement [change]", event);
                    }}
                    onBlur={() => {
                        console.log("CardNumberElement [blur]");
                    }}
                    onFocus={() => {
                        console.log("CardNumberElement [focus]");
                    }}
                />
            </label>

            <button type="submit" disabled={!stripe}>
                Pay
            </button>
        </form>
    );
}

export default StripeForm;