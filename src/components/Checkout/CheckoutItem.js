import React, { useState, useEffect } from 'react';

import { EyeIcon, EyeSlashIcon, TrashIcon } from '../Icons';

import styles from './style.module.scss';
import { withRouter } from "react-router-dom";
import { connect, useDispatch, useSelector } from "react-redux";
import { getOneQuestion, setSelectedQuestion } from "../../redux/actions";

export const CheckoutItem = ({ question, item }) => {



    const dispatch = useDispatch();

    const allQuestions =
        useSelector((state) => state.preOrderReducer.questions) || [];

    useEffect(() => {
        dispatch(getOneQuestion());
    }, []);

    const myQuestion = allQuestions[0] && allQuestions[0].question;
    const myAnswer = question.answerId;


    const [showChild, toggleChildShow] = useState(false);

    const itemPrice = () => {
        const discount = getDiscount();
        // const tickets = item.ticketSubmitIndex;
        const tickets = item.tickets.length;
        //let price = tickets * Number(item.bike.price) * (100 - discount) / 100;
        let price = tickets * Number(item.bike.price);
        return price.toFixed(2);
    }

    const getDiscount = () => {
        let discount = 0;
        // let tickets = item.ticketSubmitIndex;
        let tickets = item.tickets.length;
        /*  if (tickets >= 50) {
             discount = Number(item.bike.price_50);
         } else if (tickets >= 20) {
             discount = Number(item.bike.price_20);
         } else if (tickets >= 10) {
             discount = Number(item.bike.price_10);
         } else if (tickets >= 5) {
             discount = Number(item.bike.price_5);
         } */
        return discount;
    }

    const getDiscountTicketPrice = () => {
        //let price = Number(item.bike.price) * (100 - getDiscount()) / 100;
        let price = Number(item.bike.price);
        return Number(price.toFixed(2));
    }
   

    return (

        <div className={styles.itemContainer}>
            <div className={styles.mainWrapper}>
                <div className={`${styles.entryCol} ${styles.itemBody}`}>
                    <div className={styles.entryImg}>
                        <img src={`${process.env.REACT_APP_ADMIN_HOST}${item.bike.main_image}`} alt={`${item.bike.brand_name} ${item.bike.model}`} />
                    </div>
                    <p>{`${item.bike.brand_name} ${item.bike.model}`} </p>
                </div>
                <div className={`${styles.priceCol} ${styles.itemBody}`}>
                    &pound;{getDiscountTicketPrice()}
                </div>
                {/* <div className={`${styles.amountCol} ${styles.itemBody}`}>{item.ticketSubmitIndex}</div> */}
                <div className={`${styles.amountCol} ${styles.itemBody}`}>{item.amount}</div>
                <div className={styles.actionCol}>
                    {!showChild && <EyeIcon onClick={() => toggleChildShow(!showChild)} />}
                    {showChild && <EyeSlashIcon onClick={() => toggleChildShow(!showChild)} />}
                    {/*   <TrashIcon /> */}
                </div>
            </div>

            <div className={`${styles.childContainer} ${showChild ? styles.showChild : ''}`}>
                {
                    item.tickets.map((child, i) => (
                        <div key={i}>
                            {i < item.ticketSubmitIndex &&
                                <div className={styles.childWrapper}>
                                    <p className={styles.childInfo}><b>{myQuestion}</b></p>
                                    <div className={styles.childRightCon}>
                                        <div className={styles.childCoords}>
                                            <p><b>{myAnswer}</b></p>
                                        </div>
                                        {/*  <div className={styles.childTrashCon}>
                                            <TrashIcon />
                                        </div> */}
                                    </div>
                                </div>}
                        </div>
                    ))
                }
            </div>
        </div>
    );
}
const mapStateToProps = (state) => ({
    ...state,
});

const mapDispatchToProps = {
    setSelectedQuestion$: setSelectedQuestion,
};
//export default CheckoutItem;

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CheckoutItem));
