import React,{ useState, useEffect } from "react";
import { connect, useDispatch, useSelector  } from "react-redux";
import { Link } from "react-router-dom";
import { CardElement, useElements, useStripe } from "@stripe/react-stripe-js";
// import { loadStripe } from '@stripe/stripe-js';
import { PayPalButton } from "react-paypal-button-v2";
import { getSiteCMS } from '../../redux/actions';
import CheckoutItem from "./CheckoutItem";
// import StripeForm from './StripeForm';
import {
  getTicketPrice,
  getUnitTicketPrice,
  PAY_PAL_KEYS_DEV,
} from "../../utils/common.utils";
import { setMenuLayer } from "../../redux/actions";
import {
  paypalCheckoutRequest,
  stripeCheckoutRequest,
} from "../../api/checkout.api";
import styles from "./style.module.scss";

// const stripePromise = loadStripe('pk_test_5vZPRXlthCAArZOOovZnK7JT00txj5drRX');

const CheckoutList = ({ items, question ,onPaymentDone}) => {
  const stripe = useStripe();
  const redirectStripeCheckout = () => {
    stripe.redirectToCheckout({
      sessionId:
        "cs_test_NQN6apihIb7GgZ4RfEtbC0hDms9tp59RHdYrR6RmxTiSDySgw72SQxR7",
    });
  };

  const checkoutStripe = async (details, data) => {
    const data_ = composePurchaseItemData();
    const res = await stripeCheckoutRequest({data_, question,data,details});    
    if (!!res.status && !!res.status) {
      stripe.redirectToCheckout({
        sessionId: res.session_id,
      });
    }
  };

  const checkoutPayPal = async (details, data) => {
    const data_ = composePurchaseItemData();
    const res = await paypalCheckoutRequest({data_,question,data});
     console.log("data",data,"res",res);
      // if (!!res.status && !!res.link) {
        if (!!res.status && !!res.link) {
      window.location.href = res.link;
    }
  };
  const site_details = useSelector(state => state.preOrderReducer.sitecms);
    //const site_details = useSelector(state => state.preOrderReducer.sitecms);
    const dispatchCMS = useDispatch()
    useEffect(() => {
        dispatchCMS(getSiteCMS())
    }, []);
    const siteData = site_details[0];
    

  const getDiscount = (item) => {
    let discount = 0;
    let tickets = item.ticketSubmitIndex;

    //implement discount
   /*  if (tickets >= 50) {
      discount = Number(item.bike.price_50);
    } else if (tickets >= 20) {
      discount = Number(item.bike.price_20);
    } else if (tickets >= 10) {
      discount = Number(item.bike.price_10);
    } else if (tickets >= 5) {
      discount = Number(item.bike.price_5);
    } */
    return discount;
  };

  const totalTicketCount = () => {
    let total = 0;
    for (let item of items) {
       total += item.amount;
    }
    return total;
  };

  const totalPrice = () => {
    let total = 0;
    for (let item of items) {
      let price =
        // getUnitTicketPrice(item.bike, Number(item.ticketSubmitIndex)) * Number(item.ticketSubmitIndex);
        getUnitTicketPrice(item.bike, Number(item.amount)) * Number(item.amount);
      total += price;
    }
    return total.toFixed(2);
  };

  const composePurchaseItemData = () => {
    let data = [];
    for (let item of items) {
      // if (item.ticketSubmitIndex === 0) continue;
      item.unitTicketPrice = getUnitTicketPrice(
        item.bike,
        item.ticketSubmitIndex
      );
      item.discount = 0; //getDiscount(item);
      data.push(item);
    }
    return data;
  };

  return (
    <div className={styles.listContainer}>
      {totalTicketCount() > 0 && (
        <>
          <div className={styles.listHeader}>
            <div className={styles.entryCol}>Your Entries</div>
            <div className={styles.priceCol}>Price</div>
            <div className={styles.amountCol}>Amount</div>
            <div className={styles.actionCol}></div>
          </div>

          {items.map((item, i) => (
              <CheckoutItem question={question} item={item} key={i} />  
            ))}

          <div className={styles.listFooter}>
            <p>All Tickets: {totalTicketCount()}</p>
            <p>Total: &pound;{totalPrice()}</p>
          </div>
        </>
      )}

      {totalTicketCount() == 0 && (
        <p className="text-center text-lg">No tickets to checkout!</p>
      )}

      <div className="text-right pt-0">
        <Link to="/bikes">
          <span className="text-center px-3 py-3 text-bg hover:text-black text-sm">
            &lt;&lt; Back To Play
          </span>
        </Link>
      </div>

      <div className={`flex justify-center mt-6 ${styles.payButtonCon}`}>
        <button
          className={styles.payViaCard}
          onClick={checkoutStripe}
          disabled={totalTicketCount() === 0}
        >
          Checkout
        </button>
        <p className="mx-6 orText">Or</p>

       {/*  <PayPalButton 
          
          style={{
            shape: "rect",
            color: "blue",
            layout: "horizontal",
            label: "paypal",
          }} 
          
          amount= {totalPrice()}  
        
          onSuccess= {checkoutPayPal}  
           
          options={{
            clientId: PAY_PAL_KEYS_DEV.payPalClientId ,
            currency:"GBP"
          }} 
        /> */}

          <button
          className={styles.payViaPayPal}
          onClick={checkoutPayPal}
          disabled={totalTicketCount() === 0}
        >
          PayPal
        </button> 
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  appLoaded: state.setting.appLoaded,
  items: state.cart.items,
  question: state.cart.selectedQuestion
  
});


const mapDispatchToProps = {
  setMenuLayer$: setMenuLayer,
};

export default connect(mapStateToProps, mapDispatchToProps)(CheckoutList);
