import React from "react";
import SocialLogin from "react-social-login";
const ButtonStyle = {
  google: {
    width: "85%",
    backgroundColor: "transparent", 
    padding: 12,
    fontWeight: "bold",
    borderRadius: 20,
    marginTop: 20,
    border: "1px solid #aeaeae",
    marginLeft:33
  },
  facebook: {  
    width: "85%",
    backgroundColor: "#3e51b5", 
    padding: 12,
    fontWeight: "bold",
    borderRadius: 20,
    marginTop: 20,
    border: "1px solid #aeaeae",
    marginLeft:33

  }
}
class SocialButton extends React.Component {
 
  render() {
    //console.log("ppp", this.props.children[0].props.provider);

    return ( 
    

      <button   
        onClick={this.props.triggerLogin} {...this.props}
        style={ this.props.children[0].props.provider == 'facebook' ? ButtonStyle.facebook : ButtonStyle.google}
      >       
        {this.props.children}
      </button>

    );
  }
}

export default SocialLogin(SocialButton);
