import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { connect, useDispatch } from "react-redux";
import { Link, useLocation, useHistory } from "react-router-dom";
import countries from "../../utils/countries.json";
import { register, setMenuLayer, updateProfille } from "../../redux/actions";

import styles from "./style.module.scss";

const validations = {
  name: {
    required: "Name is required!",
    minLength: {
      value: 3,
      message: "Name must be at least 3 characters long!",
    },
  },
  surname: {
    required: "Surname is required!",
    minLength: {
      value: 3,
      message: "Surname must be at least 3 characters long!",
    },
  },
  email: {
    required: "Email is required!",
    minLength: {
      value: 5,
      message: "Email must be at least 5 characters long!",
    },
    pattern: {
      // eslint-disable-next-line
      value: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
      message: "Please input valid email address!",
    },
  },
  address: {
    required: "Address is required!",
    minLength: {
      value: 5,
      message: "Address must be at least 5 characters long!",
    },
  },
  phone: {
    required: "Phone number is requried!",
    minLength: {
      value: 10,
      message: "Phone number must be at least 10 characters long!",
    },
  },
  password: {
    required: "Password is requried!",
    minLength: {
      value: 8,
      message: "Password must be at least 6 characters long!",
    },
    pattern: {
      value: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
      message: "At least one digit, lower case and uppper case!",
    },
  },
};

const UpdateUser = ({
  updateProfille$,
  appLoaded,
  register$,
  setMenuLayer$,
  details,
}) => {
  const { handleSubmit, errors, register, trigger } = useForm();
  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    setMenuLayer$(-1);
    return () => {
      setMenuLayer$(1);
    };
  }, [appLoaded]);

  const handleOnChange = (e) => {
    trigger(e.target.name);
  };

  const onSubmit = async (data) => {
    // console.log(data);
    updateProfille$(data, () => {
     
      const search = window.location.search;
      const params = new URLSearchParams(search);
      const redirect = params.get("redirect");

      if (!!redirect) {
        history.push(redirect);
      } else {
        history.push("/profile-updated");
      }
    });
  };

  const triggerAllFields = () => {
    const names = ["name", "surname", "email", "address", "phone", "user_id"];
    for (let name of names) {
      trigger(name);
    }
  };
  return (
    <>
      {details ? (
        <form onSubmit={handleSubmit(onSubmit)} className="w-full px-6 py-10">
          <div className="flex flex-wrap">
            <div className={`fieldDiv ${styles.formGroup} ${styles.wFull}`}>
              <label style={{display:'inline-block'}}>
                <b>First Name</b>
              </label>
              <input
                className={styles.name}
                type="text"
                name="name"
                placeholder="Name"
                onChange={handleOnChange}
                ref={register(validations.name)}
                defaultValue={details.name}
              />

              {errors.name && (
                <p className={`text-sm ${styles.error}`}>
                  {errors.name.message}
                </p>
              )}
            </div>

            <div className={`fieldDiv ${styles.formGroup} ${styles.wFull}`}>
              <label style={{display:'inline-block'}}>
                <b>Last Name</b>
              </label>
              <input
                className="field"
                type="text"
                name="surname"
                placeholder="Surname"
                onChange={handleOnChange}
                ref={register(validations.surname)}
                defaultValue={details.surname}
              />
              {errors.surname && (
                <p className={`text-sm ${styles.error}`}>
                  {errors.surname.message}
                </p>
              )}
            </div>
          </div>
          <div className="flex flex-wrap">
            <div className={`fieldDiv ${styles.formGroup} ${styles.wFull}`}>
              <label style={{display:'inline-block'}}>
                <b>Address</b>
              </label>
              <input
                className="field"
                type="text"
                name="address"
                placeholder="Address"
                onChange={handleOnChange}
                ref={register(validations.address)}
                defaultValue={details.address}
              />
              {errors.address && (
                <p className={`text-sm ${styles.error}`}>
                  {errors.address.message}
                </p>
              )}
            </div>
            <div className={`fieldDiv ${styles.formGroup} ${styles.wFull}`}>
              <label style={{display:'inline-block'}}>
                <b>City</b>
              </label>
              <input
                className="field"
                type="text"
                name="city"
                placeholder="city"
                onChange={handleOnChange}
                ref={register(validations.city)}
                defaultValue={details.city}
              />
              {errors.city && (
                <p className={`text-sm ${styles.error}`}>
                  {errors.city.message}
                </p>
              )}
            </div>
          </div>
          <div className="flex flex-wrap">
            <div className={`fieldDiv ${styles.formGroup} ${styles.wFull}`}>
              <label style={{display:'inline-block'}}>
                <b>Postal code/Zip </b>
              </label>
              <input
                className="field"
                type="text"
                name="zipcode"
                placeholder="zipcode"
                onChange={handleOnChange}
                ref={register(validations.zipcode)}
                defaultValue={details.zipcode}
              />
              {errors.zipcode && (
                <p className={`text-sm ${styles.error}`}>
                  {errors.zipcode.message}
                </p>
              )}
            </div>
            <div className={`fieldDiv ${styles.formGroup} ${styles.wFull}`}>
              <label style={{display:'inline-block'}}>
                <b>Phone Number </b>
              </label>
              <input
                className="field"
                type="text"
                name="phone"
                placeholder="Phone"
                onChange={handleOnChange}
                ref={register(validations.phone)}
                defaultValue={details.phone}
              />
              {errors.phone && (
                <p className={`text-sm ${styles.error}`}>
                  {errors.phone.message}
                </p>
              )}
            </div>
          </div>
          <div className="flex flex-wrap" >
            <div className={`fieldDiv ${styles.formGroup} ${styles.wFull}`} >
              <label style={{display:'inline-block'}}>
                <b>Country </b>
              </label>
              <select
                defaultValue={details.country}
                ref={register(validations.country)}
                onChange={handleOnChange}
                name="country"
                id="country"
              >
                {countries &&
                  Object.keys(countries).map((item, index) => {
                    return (
                      <option selected={details.country === countries[item]} value={countries[item]}>{countries[item]}</option>
                    );
                  })}
              </select>

              {/*  <input
                className="field"
                type="text"
                name="country"
                placeholder="Country"
                onChange={handleOnChange}
                ref={register(validations.country)}
                defaultValue={details.country}
              /> */}
              {errors.country && (
                <p className={`text-sm ${styles.error}`}>
                  {errors.country.message}
                </p>
              )}
            </div>
            <div className={`fieldDiv ${styles.formGroup} ${styles.wFull}`}>
              <label>
                <b>Email</b>
              </label>
              <input
                className="field"
                type="text"
                name="email"
                placeholder="Email"
                onChange={handleOnChange}
                ref={register(validations.email)}
                readOnly="readOnly"
                defaultValue={details.email}
              />
              {errors.email && (
                <p className={`text-sm ${styles.error}`}>
                  {errors.email.message}
                </p>
              )}
            </div>
          </div>

          <div className="text-center paddlr10 log_btn mt-3">
            <button
              type="submit"
              onClick={triggerAllFields}
              style={{
                width: "50%",
                backgroundColor: "#fbb03b",
                padding: 12,
                fontWeight: "bold",
                borderRadius: 20,
                marginTop: 20,
              }}
            >
              Update Details
            </button>
          </div>
        </form>
      ) : (
          <h4>
            {" "}
            <a
              onClick={() => {
                history.push("/my-profile");
              }}
              style={{ color: "#000" }}
            >
              Invalid user,Go back to profile
          </a>
          </h4>
        )}
    </>
  );
};

const mapStateToProps = (state) => ({
  appLoaded: state.setting.appLoaded,
});

const mapDispatchToProps = {
  register$: register,
  setMenuLayer$: setMenuLayer,
  updateProfille$: updateProfille,
};

export default connect(mapStateToProps, mapDispatchToProps)(UpdateUser);
