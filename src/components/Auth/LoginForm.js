import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { connect } from "react-redux";
import { Link, useLocation, useHistory } from "react-router-dom";

import {
  getSession,
  login,
  setMenuLayer,
  socialLogin,
} from "../../redux/actions";
import SocialButton from "./SocialButton";

import styles from "./style.module.scss";

const validations = {
  email: {
    required: "Email is required!",
    minLength: {
      value: 5,
      message: "Email must be at least 5 characters long!",
    },
    pattern: {
      // eslint-disable-next-line
      value: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
      message: "Please input valid email address!",
    },
  },
  password: {
    required: "Password is requried!",
    minLength: {
      value: 8,
      message: "Password must be at least 6 characters long!",
    },
  /*   pattern: {
      value: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
      message: 'At least one digit, lower case,uppper case and special character!'
    }, */
  },
};

const LoginForm = ({ getSession$, login$, setMenuLayer$, socialLogin$ }) => {
  const { handleSubmit, errors, register, trigger } = useForm();
  const history = useHistory();
  const location = useLocation();
  const [serverError, setserverError] = useState();



  useEffect(() => {
    setMenuLayer$(-1);
    return () => {
      setMenuLayer$(1);
    };
  }, []);

  const handleOnChange = (e) => {
    setserverError(undefined)
    trigger(e.target.name);
  };

  const onSubmit = async (data) => {
    login$(data, async (result) => {
      // console.log("[process callback!!!]", result);
      // check if there is redirect param
      if (result.status) {
        const search = window.location.search;
        const params = new URLSearchParams(search);
        const redirect = params.get("redirect"); 
        await getSession$();

        window.location.replace("/my-profile")
        /*  if (!!redirect) {
           history.push(redirect);
         } else {
           history.push("/my-profile");
         } */
      } else {
        setserverError(result.message)
        //set error here
      }
    });
  };

  const triggerAllFields = () => {
    setserverError(undefined)
    const names = ["email", "password"];
    for (let name of names) {
      trigger(name);
    }
  };

  const handleSocialLogin = (user) => {
    socialLogin$(user, async () => {
      //console.log("[process callback!!!]");
      // check if there is redirect param
      const search = window.location.search;
      const params = new URLSearchParams(search);
      const redirect = params.get("redirect");
      await getSession$();
      if (!!redirect) {
        window.location.replace(redirect)
       // history.push(redirect);
      } else {
        window.location.replace("/my-profile")
        //history.push("/my-profile");
      }
    });
  };

  const handleSocialLoginFailure = (err) => {
    console.error(err);
  };

  return (
    <>
      <form className="w-full px-6 py-10" onSubmit={handleSubmit(onSubmit)}>
        <div className="flex flex-wrap">
          <div className={`${styles.formGroup} ${styles.wFull}`}>
            <input
              type="text"
              placeholder="Email"
              name="email"
              onChange={handleOnChange}
              ref={register(validations.email)}
              style={{ borderRadius: 20, paddingLeft: 25 }}
            />
            {errors.email && (
              <p
                style={{ position: "absolute" }}
                className={`text-sm ${styles.error}`}
              >
                {errors.email.message}
              </p>
            )}
          </div>
          <div className={`${styles.formGroup} ${styles.wFull}`}>
            <input
              type="password"
              placeholder="Password"
              name="password"
              onChange={handleOnChange}
              ref={register(validations.password)}
              style={{ borderRadius: 20, paddingLeft: 25 }}
            />

            {errors.password && (
              <p
                style={{ position: "absolute" }}
                className={`text-sm ${styles.error}`}
              >
                {errors.password.message}
              </p>
            )}

            {
              serverError &&
              <p
                style={{ position: "absolute" }}
                className={`text-sm ${styles.error}`}
              >
                {serverError}
              </p>

            }
          </div>
        </div>
        <div className="max-w-screen-sm mx-auto paddlr10">
          <div className="flex flex-wrap mob_login">
            <div className="w-full sm:w-8/12 p-5">
              <div className="checkbox checkoxDesign">
                <label className="text-sm ">
                  <input type="checkbox" value="option1" />
                  Keep me signed in
                </label>
              </div>
            </div>
            <div className="w-full sm:w-4/12">
              <br />
              <Link
                to="/forgot-password"
                style={{ color: "blue", marginLeft: 50 }}
              >
                Forgot Password?
              </Link>
            </div>
          </div>
        </div>
        <div className="text-center paddlr10 log_btn">
          <button
            type="submit"
            onClick={triggerAllFields}
            style={{
              width: "100%",
              backgroundColor: "#fbb03b",
              padding: 12,
              fontWeight: "bold",
              borderRadius: 20,
              marginTop: 20,
            }}
          >
            Log In
          </button>
        </div>
      </form>

      <div className="max-w-screen-sm mx-auto paddlr10">
        <div className="flex flex-wrap ">
          <div className="w-full sm:w-6/12">
            <SocialButton
              provider="facebook"
              appId="3341542519267662"
              onLoginSuccess={handleSocialLogin}
              onLoginFailure={handleSocialLoginFailure}
              key={"facebook"}
            >
              <img
                provider="facebook"
                src="https://img.icons8.com/color/48/000000/facebook-circled.png"
                style={{ margin: "-6px 0 -28px 20px", height: 29 }}
              />
              <span style={{ margin: "0px 0 0px 12px", color: "#fff" }}>
                Facebook
              </span>
            </SocialButton>
          </div>
          <div className="w-full sm:w-6/12">
            <SocialButton
              provider="google"
              appId="298852658480-af14ifi32ufio5h99i8msfr1pet7uhc5.apps.googleusercontent.com"
              onLoginSuccess={handleSocialLogin}
              onLoginFailure={handleSocialLoginFailure}
              key={"google"}
              scope={"https://www.googleapis.com/auth/user.gender.read"}
            >
              <img
                provider="google"
                src="https://img.icons8.com/fluent/48/000000/gmail.png"
                style={{ margin: "-6px 0 -28px 20px", height: 29 }}
              />
              <span style={{ margin: "0px 0 0px 12px" }}>Gmail</span>
            </SocialButton>
          </div>
          <div className="margin-left mob_log_btm">
            <p className="text-sm mt-4">
              Not a member yet?{" "}
              <Link to={`/signup${location.search}`}>
                <span className="common-link" style={{ marginLeft: 20 }}>
                  Sign up
                </span>
              </Link>
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

const mapStateToProps = (state) => ({
  appLoaded: state.setting.appLoaded,
});

const mapDispatchToProps = {
  getSession$: getSession,
  setMenuLayer$: setMenuLayer,
  login$: login,
  socialLogin$: socialLogin,
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
