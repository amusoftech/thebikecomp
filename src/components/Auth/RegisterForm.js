import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { connect } from 'react-redux';
import { Link, useLocation, useHistory } from "react-router-dom";
import { getSession, register, setMenuLayer, socialLogin } from '../../redux/actions';
import SocialButton from "./SocialButton";
import styles from './style.module.scss';


const validations = {
    name: {
        required: 'Name is required!',
        minLength: {
            value: 3,
            message: 'Name must be at least 3 characters long!'
        }
    },
    surname: {
        required: 'Surname is required!',
        minLength: {
            value: 3,
            message: 'Surname must be at least 3 characters long!'
        }
    },
    email: {
        required: 'Email is required!',
        minLength: {
            value: 5,
            message: 'Email must be at least 5 characters long!'
        },
        pattern: {
            // eslint-disable-next-line
            value: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            message: 'Please input valid email address!'
        }
    },
    address: {
        required: 'Address is required!',
        minLength: {
            value: 5,
            message: 'Address must be at least 5 characters long!'
        }
    },
    phone: {
        required: 'Phone number is required!',
        minLength: {
            value: 10,
            message: 'Phone number must be at least 10 characters long!'
        }
    },
    agree: {
        required: 'Please agree to our Terms and Conditions to proceed.',
        minLength: {
            value: 1,
            message: 'Please agree to our Terms and Conditions to proceed.'
        }
    },

    password: {
        required: 'Password is required!',
        minLength: {
            value: 8,
            message: 'Password must be at least 6 characters long!'
        },

        pattern: {
            value: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
            message: 'At least one digit, lower case,uppper case and special character!'
        }
    }
};
const mystyle = {
    color: "white",
    backgroundColor: "DodgerBlue",
    padding: "10px",
    fontFamily: "Arial"
};

const RegisterForm = ({ getSession$,appLoaded, register$, setMenuLayer$, socialLogin$ }) => {
    const [checkBoxChecked, setCheckBoxChecked] = useState(true)
    const { handleSubmit, errors, register, trigger } = useForm()
    const history = useHistory();
    const location = useLocation();
    const [serverError, setserverError] = useState();

    useEffect(() => {
        setMenuLayer$(-1);
        return () => {
            setMenuLayer$(1);
        }
    }, [appLoaded]);

    const handleOnChange = (e) => {
        setserverError(undefined)
        trigger(e.target.name)
    }

    const onSubmit = async data => {
      

        register$(data, async (result) => {
            if (data.result) {
               
                // check if there is redirect param
                const search = window.location.search;
                const params = new URLSearchParams(search);
                const redirect = params.get('redirect');
                await getSession$();
                if (!!redirect) {
                    history.push(redirect);
                }
            } else {
                if(result){
                    setserverError(result.message);
                }else{
                    history.push("/home");
                }
              
              
            }
        })
    }

    const triggerAllFields = () => {
       
        const names = ['name', 'surname', 'email', 'address', 'phone', 'password', 'agree'];
        for (let name of names) {
            trigger(name);
        }
        setserverError(undefined);
    }
    const handleSocialLogin = (user) => {

        socialLogin$(user,async (result) => {
            console.log("result",result);
            if (result.user) {
            // check if there is redirect param
            const search = window.location.search;
            const params = new URLSearchParams(search);
            const redirect = params.get('redirect');
            await getSession$() 
            if (!!redirect) { 
                //history.push(redirect);  
                window.location.replace(redirect)
            } else {
                window.location.replace("/my-profile")
                 //history.push("/my-profile");
            }
        }else{
            if(result){
                setserverError(result.message);
            }else{
                history.push("/my-profile");
            }
           
            }
            
        })

    };
    const handleSocialLoginFailure = (err) => {
        console.error(err);
    };

   

    return (
        <>
            <form className="w-full px-6 py-10" onSubmit={handleSubmit(onSubmit)}>
                <div className="flex flex-wrap">
                    <div className={`${styles.formGroup} ${styles.wFull}`}>
                        <input className={styles.name} type="text" name="name" placeholder="First Name"
                            onChange={handleOnChange}
                            ref={register(validations.name)}
                            style={{ borderRadius: 20, paddingLeft: 25 }}
                        />
                        {errors.name && (
                            <p style={{ position: 'absolute' }} className={`text-sm ${styles.error}`}>{errors.name.message}</p>
                        )}
                    </div>
                    <div className={`${styles.formGroup} ${styles.wFull}`}>
                        <input type="text" name="surname" placeholder="Last Name"
                            onChange={handleOnChange}
                            ref={register(validations.surname)}
                            style={{ borderRadius: 20, paddingLeft: 25 }}
                        />
                        {errors.surname && (
                            <p style={{ position: 'absolute' }} className={`text-sm ${styles.error}`}>{errors.surname.message}</p>
                        )}
                    </div>
                    <div className={`${styles.formGroup} ${styles.wFull}`}>
                        <input type="text" name="email" placeholder="Email"
                            onChange={handleOnChange}
                            ref={register(validations.email)}
                            style={{ borderRadius: 20, paddingLeft: 25 }}
                        />
                        {errors.email && (
                            <p style={{ position: 'absolute' }} className={`text-sm ${styles.error}`}>{errors.email.message}</p>
                        )}
                    </div>
                    <div className={`${styles.formGroup} ${styles.wFull}`}>
                        <input type="text" name="address" placeholder="Address"
                            onChange={handleOnChange}
                            ref={register(validations.address)}
                            style={{ borderRadius: 20, paddingLeft: 25 }}
                        />
                        {errors.address && (
                            <p style={{ position: 'absolute' }} className={`text-sm ${styles.error}`}>{errors.address.message}</p>
                        )}
                    </div>
                    <div className={`${styles.formGroup} ${styles.wFull}`}>
                        <input type="text" name="phone" placeholder="Phone Number"
                            onChange={handleOnChange}
                            ref={register(validations.phone)}
                            style={{ borderRadius: 20, paddingLeft: 25 }}
                        />
                        {errors.phone && (
                            <p style={{ position: 'absolute' }} className={`text-sm ${styles.error}`}>{errors.phone.message}</p>
                        )}
                    </div>
                    <div className={`${styles.formGroup} ${styles.wFull}`}>
                        <input type="password" name="password" placeholder="Password"
                            onChange={handleOnChange}
                            ref={register(validations.password)}
                            style={{ borderRadius: 20, paddingLeft: 25 }}
                        />
                        {errors.password && (
                            <p style={{ position: 'absolute' }} className={`text-sm ${styles.error}`}>{errors.password.message}</p>
                        )}
                    </div>

                </div>
                <div className="flex flex-wrap  ">
                    <div className="checkbox textStyle mt-5">
                        <label>
                            <input /* onChange={()=>{
                                 setCheckBoxChecked(!checkBoxChecked)
                                }} */
                                onChange={handleOnChange}
                                ref={register(validations.agree)}

                                type="checkbox" name="agree" value=" " />
                    By creating your account, you agree to our <b><Link to="/terms-and-conditions" className="linkStyle" >Terms and Conditions</Link> {'&'} <Link to="/privacy" className="linkStyle" >Privacy Policy </Link> </b>
                        </label>
                    </div>
                    <div className={`${styles.formGroup} ${styles.wFull}`}>
                        {errors.agree && (
                            <p style={{ position: 'absolute' }} className={`text-sm ${styles.error}`}>{errors.agree.message}</p>
                        )}
                        {
                            serverError &&
                            <p
                                style={{ position: "absolute" }}
                                className={`text-sm ${styles.error}`}
                            >
                                {serverError}
                            </p>

                        }
                    </div>

                </div>
                {/* <div className="flex justify-center py-8"> */}
                <div className="text-center paddlr10 log_btn mt-3">
                    <button /* disabled={checkBoxChecked}  */ type="submit" onClick={triggerAllFields} style={{ width: '100%', backgroundColor: '#fbb03b', padding: 12, fontWeight: 'bold', borderRadius: 20, marginTop: 20 }} >Sign Up</button>
                    {/*  <p className="text-sm mt-4">Not registered? <Link to={`/login${location.search}`}><span className="common-link">Login here</span></Link></p> */}

                    {/* </div> */}
                </div>
            </form>
            <h3 className="text-center font-bold ">or </h3>
            <h3 className="text-center font-bold ">Sign up with </h3>
            <div className="social-login mob_social_login">
                <div className="flex flex-wrap ">
                    <div className="w-full sm:w-6/12">
                        <SocialButton
                            provider="facebook"
                            appId="3341542519267662"
                            onLoginSuccess={handleSocialLogin}
                            onLoginFailure={handleSocialLoginFailure}
                            key={"facebook"}
                        >
                            <img
                                provider="facebook"
                                src="https://img.icons8.com/color/48/000000/facebook-circled.png"
                                style={{ margin: "-6px 0 -28px 20px", height: 29 }}
                            />
                            <span style={{ margin: "0px 0 0px 12px", color: "#fff" }}>
                                Facebook
                             </span>
                        </SocialButton>
                        {/*    <button type="submit" className="facebookBtn" style={{ width: '85%', backgroundColor: '#3f51b5', padding: 12, fontWeight: 'bold', borderRadius: 20, marginTop: 20 }} ><img src="https://img.icons8.com/color/48/000000/facebook-circled.png" style={{ margin: "-6px 0 -28px 20px", height: 29 }} /><span style={{ margin: "0px 0 0px 12px", color: '#fff' }}>Facebook</span></button> */}
                    </div>
                    <div className="w-full sm:w-6/12">
                        {/*  <button type="submit" className="googleBtn" style={{ width: '85%', backgroundColor: 'transparent', padding: 12, fontWeight: 'bold', borderRadius: 20, marginTop: 20, border: '1px solid #aeaeae' }} ><img src="https://img.icons8.com/fluent/48/000000/gmail.png" style={{ margin: "-6px 0 -28px 20px", height: 29 }} /> <span style={{ margin: "0px 0 0px 12px" }}>Gmail</span></button> */}
                        <SocialButton
                            provider="google"
                            appId="298852658480-af14ifi32ufio5h99i8msfr1pet7uhc5.apps.googleusercontent.com"
                            onLoginSuccess={handleSocialLogin}
                            onLoginFailure={handleSocialLoginFailure}
                            key={"google"}
                            scope={"https://www.googleapis.com/auth/user.gender.read"}
                        >
                            <img
                                provider="google"
                                src="https://img.icons8.com/fluent/48/000000/gmail.png"
                                style={{ margin: "-6px 0 -28px 20px", height: 29 }}
                            />
                            <span style={{ margin: "0px 0 0px 12px" }}>Gmail</span>
                        </SocialButton>
                    </div>

                </div>
            </div>
            <p className="text-center text-sm mt-4">Are you already a member?<Link to={`/login${location.search}`}><span className="common-link" style={{ marginLeft: 20 }}> Log in</span></Link></p>

            <p className="text-center text-sm mt-4 mrgbtm30">We respect your privacy and do not share your details with third parties.</p>

        </>
    );
}


const mapStateToProps = state => ({
    appLoaded: state.setting.appLoaded,
});

const mapDispatchToProps = {
    register$: register,
    setMenuLayer$: setMenuLayer,
    socialLogin$: socialLogin  ,
    getSession$: getSession,
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterForm);