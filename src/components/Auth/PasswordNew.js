import React, { useEffect, useState } from 'react';
import { useForm } from 'react-hook-form';
import { connect } from 'react-redux';
import { Link, useLocation, useHistory } from "react-router-dom";
import { changePassword, register, setMenuLayer } from '../../redux/actions';

import styles from './style.module.scss';


const validations = { 
    
    password: {
        required: 'Password is required!',
        minLength: {
            value: 8,
            message: 'Password must be at least 6 characters long!'
        },
            pattern: {
                value: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
                message: 'At least one digit, lower case and uppper case!'
            }
        },
        confirm_password: {
            required: 'Confirm Password is required!',
            minLength: {
                value: 8,
                message: 'Password must be at least 6 characters long!'
            },
                pattern: {
                    value: /^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
                    message: 'At least one digit, lower case and uppper case!'
                }
            }
    };
    const mystyle = {
        color: "white",
        backgroundColor: "DodgerBlue",
        padding: "10px",
        fontFamily: "Arial"
    };

    const PasswordNew = ({token,userData, appLoaded, register$, setMenuLayer$,changePassword$}) => {
        const [checkBoxChecked, setCheckBoxChecked] = useState(true)
        const { handleSubmit, errors, register, trigger } = useForm()
        const history = useHistory();
        const location = useLocation();

        useEffect(() => {
            setMenuLayer$(-1);
            return () => {
                setMenuLayer$(1);
            }
        }, [appLoaded]);

        const handleOnChange = (e) => {
            trigger(e.target.name)
            
        }
       

        const onSubmit = async data => { 
            
            data.user_id = userData && userData.id;
            data.token= token
            changePassword$(data,()=>{
                history.push('/success')
               
            });
        }

        const triggerAllFields = () => {

            const names = [ 'confirm_password', 'password'];
            for (let name of names) {
                trigger(name);
            }
        }
     

        // console.log(dirty, errors);

        return (
            <> 
                <form className="w-full px-6 py-10" onSubmit={handleSubmit(onSubmit)}>
                    <div className="flex flex-wrap">                      
                       
                        <div className={`${styles.formGroup} ${styles.wFull}`}>
                            <input type="password" name="password" placeholder="Your New Password"
                                onChange={handleOnChange}
                                ref={register(validations.password)}
                                style={{ borderRadius: 20, paddingLeft: 25 }}
                            />
                            {errors.password && (
                                <p style={{position: 'absolute'}} className={`text-sm ${styles.error}`}>{errors.password.message}</p>
                            )}
                        </div>
                        <div className={`${styles.formGroup} ${styles.wFull}`}>
                            <input type="password" name="confirm_password" placeholder="Confirm Your New Password"
                                onChange={handleOnChange}
                                ref={register(validations.confirm_password)}
                                style={{ borderRadius: 20, paddingLeft: 25 }}
                            />
                            {errors.confirm_password && (
                                <p style={{position: 'absolute'}} className={`text-sm ${styles.error}`}>{errors.confirm_password.message}</p>
                            )}
                        </div>
                        <div className={`${styles.formGroup} ${styles.wFull}`}>

                        </div>
                    </div>
                 
                    <div className="text-center paddlr10 log_btn">
                        <button    type="submit" onClick={triggerAllFields} style={{ width: '100%', backgroundColor: '#fbb03b', padding: 12, fontWeight: 'bold', borderRadius: 20, marginTop: 20 }} >Reset Password</button>
                        
                    </div>
                </form>
             
                
            </>
        );
    }


const mapStateToProps = state => ({
        appLoaded: state.setting.appLoaded,
    });

    const mapDispatchToProps = {
        register$: register,
        setMenuLayer$: setMenuLayer,
        changePassword$:changePassword
    }

export default connect(mapStateToProps, mapDispatchToProps)(PasswordNew);