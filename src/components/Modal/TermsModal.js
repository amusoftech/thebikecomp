import React, { useState } from "react";
import Modal from "react-modal";
import { getSiteCMS } from "../../redux/actions";
import { getHasToShowModal, setHasToShowModal } from "./ModalHelp";
import styles from "../Bikes/Details/style.module.scss";
import { CloseIcon } from '../Icons';

export default function TermsModal() {
  const show = getHasToShowModal();
  const [isOpenNow, setIsOpenNow] = useState(show);
  const [doNotShowAgain, setDoNotShowAgain] = useState(false)


  //DO NOT CHANGE THIS 
  function closeModal() {
    setIsOpenNow(!isOpenNow);
    //set to show or not again
    setHasToShowModal(doNotShowAgain)
  }
  //DO NOT CHANGE THIS   
  const onChange = (e) => {
    console.log("e",e);
    setDoNotShowAgain(!doNotShowAgain)
  }
  //const onClose = () => setShowModal(false);
  return (
    <div>
      <Modal
        isOpen={!isOpenNow}
        onRequestClose={closeModal}
        contentLabel="Example Modal"
        className="TermsOfUSER"
      >
        <section className={`TermsOfUSERHead ${styles.container}`}>
          {/* Header */}
          <div className={styles.header}>
            <div className="headHIW">
              <span>HOW DOES IT WORK?</span>
            </div>
            <div className="pr-2 pt-2">
              <CloseIcon className="opacity-75 hover:opacity-100 transition-opacity" onClick={closeModal} />
            </div>
          </div>
          <div className={styles.body}>


            <div className="row termpedding">
              <div className="col-sm ml-5">
                <ol>
                  <li>
                    There currently is only one Monthly Competition. Click the enter button to see which Bikes and E-Bikes we have available.
                    </li><br />
                  <li>
                    Browse and Select your Bike Comp Tickets (limited to 100 per person).
                    </li><br />
                  <li>
                    Proceed to play from your shopping cart.
                   </li><br />
                  <li>
                    You will be asked to create an account (new user) or Log in (for existing users).
                    </li><br />
                  <li>
                    Use your Knowledge to answer the Skill question. (Only correct answers will be eligible for valid entry, The Bike Comp is a Prize Draw and NOT a raffle or lottery).
                    </li><br />
                  <li>Proceed to checkout where you can use our trusted payment gateways STRIPE or PAYPAL to make a secure payment</li><br />
                  <li>You will receive a confirmation email when your purchase is complete.</li><br />
                  <li>
                    Once the competition is closed, we will give every valid entry a randomised number and  then a random selection is made by the Random wheel software to decide the winner.
                    </li><br />
                  <li>
                    10 runners up will get all their entries re-entered into the following draw for free. Runners up will be the first five above and below the selected winner.
                    </li><br />
                  <li>
                    The winner will be notified by email.
                    </li>
                </ol>
              </div>
            </div>
          </div>

          <div className="w-full md:w-1/2  py-6">
            <input onChange={onChange} type="checkbox" name={"doNotShowAgain"} id={"doNotShowAgain"} className="ml-4" />
            <label htmlFor={"doNotShowAgain"}> Do not show again</label>
          </div>
        </section>
      </Modal>
    </div>
  );
}
