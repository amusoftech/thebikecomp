import React from 'react';
import { connect } from 'react-redux';
import { withRouter } from "react-router";
import { useLocation, useHistory } from "react-router-dom";

import ShoppingItem from './ShoppingItem';

import { toggleShoppingCart, setCartHover } from '../../redux/actions';
import { getTicketDiscount, getTicketPrice } from '../../utils/common.utils';

import styles from './style.module.scss';

const ShoppingCart = ({ items, loggedIn, show, setCartHover$, toggleMenu$, toggleShoppingCart$ }) => {
    const history = useHistory();
    const location = useLocation()

    const toggleCart = (toStatus = null) => {
        toggleShoppingCart$(toStatus);
    }
    const handleOnHover = () => {
        // console.log('[Cart] not close');
        // setCartHover$(true);
    }
    const handleOnLeave = () => {
        // console.log('[Cart] may close');
        // setCartHover$(false);
    }
    const getSubTotal = () => {
        let total = 0;
        for (let item of items) {
            total += Number(item.bike.price) * item.amount;
        }
        return total.toFixed(2);
    }
    const getTotalDiscount = () => {
        let discount = 0;
      /*   for (let item of items) {
            discount += Number(getTicketDiscount(item.bike, item.amount));
        } */
        //return discount.toFixed(2);
        return discount
    }
    const getTotalPrice = () => {
        let price = 0;
        for (let item of items) {
            price += Number(getTicketPrice(item.bike, item.amount));
        }
        return price.toFixed(2);
    }
    const getTotalTicketCount = () => {
        let total = 0;
        for (let item of items) { total += item.amount; }
        return total;
    }
    const readyToPlay = () => {
        toggleShoppingCart$(false);

        if (items.length === 0) {
            history.push('/bikes');
        } else {
            if (location.pathname.includes('/question')) {
                return true;
            }
            if (loggedIn) {
                history.push('/question');
                // history.push('/checkout');
            } else {
                history.push('/login?redirect=/question');
            }
        }
    }
    return (
        <>
            <section className={`${styles.container} ${!show ? styles.hideCart : ""}`} onMouseOver={handleOnHover} onMouseLeave={handleOnLeave}>
                <div className={styles.scollContainer}>
                    {
                        items && items.length > 0 && items.map((item, i) => (
                            <ShoppingItem data={item} key={i} />
                        ))
                    }
                </div>

                {items.length > 0 &&
                    <>
                        <div className={`${styles.flexBetween} mt-8`}>
                            <label>Subtotal:</label>
                            <p>&pound;{getSubTotal()}</p>
                        </div>

                      {/*   <div className={styles.flexBetween}>
                            <label>5+ Tickets Discount 5%</label>
                            <p>-&pound;{getTotalDiscount()}</p>
                        </div> */}
                        <div className={styles.divider}></div>
                        <div className={styles.flexBetweenTotal}>
                            <label>TOTAL:</label>
                    <p className="font-extrabold">&pound;{getSubTotal()}{/* {getTotalPrice()} */}</p>
                        </div>
                      {/*   <p className="uppercase px-4 font-bold" style={{ fontSize: 12 }}>Add 4 more tickets to get 10% discount</p> */}
                    </>
                }

                {
                    items.length === 0 &&
                    <p className="mt-6 pl-3">Please add your dream bikes.</p>
                }


                <button className={styles.readyButton}
                    onClick={readyToPlay}
                >{items.length > 0 ? 'Ready to play' : 'Look for Bikes...'}</button>
                <p className="px-4 font-medium mt-2" style={{ fontSize: 14 }}>You have selected <b>{getTotalTicketCount()} Tickets</b></p>

                <p
                    className="pl-4 font-bold mt-1 p-2 cursor-pointer hover:text-gray-700 transition-color"
                    onClick={() => toggleCart(false)}
                >Hide >></p>
            </section>
        </>
    );
}

const mapStateToProps = state => ({
    items: state.cart.items,
    loggedIn: state.auth.login,
});

const MapDispatchToProps = {
    setCartHover$: setCartHover,
    toggleShoppingCart$: toggleShoppingCart
};

export default withRouter(connect(mapStateToProps, MapDispatchToProps)(ShoppingCart));