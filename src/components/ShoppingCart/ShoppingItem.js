import React from 'react';
import { connect } from 'react-redux';

import { TrashIcon } from '../Icons';
import styles from './style.module.scss';
import { addBikeToCart, deleteBike } from '../../redux/actions';
// import { getTicketPrice } from '../../utils/common.utils';

const ShoppingItem = ({ data, addBikeToCart$, deleteBike$ }) => {

    const deleteBikeFromCart = () => {
        console.log(data);
        deleteBike$(data.bike);
    }

    return (
        <>
            <div className={styles.cartItem}>
                <div className={styles.countContainer}>
                    <div className={styles.countHandler}
                        onClick={() => addBikeToCart$(data.bike, 1)}>+</div>
                    <span className={styles.cartNumber}>{data.amount}</span>
                    <div className={styles.countHandler}
                        onClick={() => addBikeToCart$(data.bike, -1)}>-</div>
                </div>

                <div className={styles.imgContainer}>
                    <img className={styles.cartImage} src={`${process.env.REACT_APP_ADMIN_HOST}${data.bike.main_image}`} alt="Bike" />
                </div>

                <div className={`${styles.brandModelCon}`}>
                    <p className={styles.type}>{data.bike.brand_name}</p>
                    <p className={styles.brand}>{data.bike.type_name}</p>
                </div>

                <div className={styles.priceCon}>
                    <p className={styles.price}>&pound;{data.bike.price}</p>
                </div>
                <div className={styles.trashContainer}>
                    <TrashIcon className={styles.trashIcon} onClick={deleteBikeFromCart} />
                </div>
            </div>
        </>
    );
}

const mapStateToProps = state => ({
    appLoaded: state.setting.appLoaded,
});

const mapDispatchToProps = {
    addBikeToCart$: addBikeToCart,
    deleteBike$: deleteBike
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingItem);