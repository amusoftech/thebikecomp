import React, { useState } from 'react';
import { connect } from 'react-redux';
// import { Link } from 'react-router-dom';
import Slider from 'react-rangeslider'

import { Thumbnail, TickitSelector } from './Widgets';
import { CloseIcon } from '../../Icons';
import { addBikeToCart, submitTicket, toggleShoppingCart } from '../../../redux/actions';

import styles from './style.module.scss';


const BikeDetails = ({ bike, onClose, addBikeToCart$, toggleShoppingCart$ ,submitTicket$}) => {
    const [selThumbnail, setSelThumbnail] = useState(0);
    const [tickets, setTickets] = useState(1);
 
    const addTickets = (count) => {
        console.log('count', count);
        addBikeToCart$(bike, count);
        submitTicket$(bike.id,{x:10,y:10}) //faking the ticket submssion
        toggleShoppingCart$(true);
        setTimeout(function(){
            // console.log('[gonna close cart]', setting);
            // if (setting.cartHover === false) 
            toggleShoppingCart$(false);
        }, 5000);
    }
    const handleOnSelectThumbnail = index => {
        setSelThumbnail(index);
    }
    /* const handleOnSelectTickets = (val) => {
        setTickets(val);
    } */
    const getDiscountPrice = (count) => {
        const discounted = Number(bike.price) * (100 - Number(bike[`price_${count}`] || 0)) / 100;
        return discounted.toFixed(2);
    }
    const getMainImageURL = () => {
        if (selThumbnail === 0) {
            return `${process.env.REACT_APP_ADMIN_HOST}${bike.main_image}`;
        } else {
            return `${process.env.REACT_APP_ADMIN_HOST}${bike[`thumbnail_${selThumbnail}`]}`
        }
    }
    const onChange = (ticketValue) =>{
        setTickets(ticketValue)
    }
    return (
        <>
            <section className={styles.container}>
                {/* Header */}
                <div className={styles.header}>
                    <div className={styles.brand}>
                      {bike.brand_name || ''} <span>{bike.model || ''}</span>
                    </div>
                    <div className="pr-2 pt-2">
                        <CloseIcon className="opacity-75 hover:opacity-100 transition-opacity" onClick={onClose} />
                    </div>
                </div>

                <div className={styles.body}>
                    <div className="w-full md:w-1/2 mb-3">
                        <div className="pl-3">
                            {
                               /*  [1, 5, 10].map((count, i) => (
                                    <span className={`mr-2 ${styles.priceItem}`} key={i}>{count}{i > 0 ? '+' : ''}|<span className={styles.priceNumber}>&pound;{getDiscountPrice(count)}</span></span>
                                )) */
                            }
                            <p className={styles.retailPrice}>retail price: &pound;{bike.retail_price}</p>
                        </div>
                        <div className="p-3">
                            <div className={styles.mainImageCon}>
                                <img className={styles.mainImage} src={getMainImageURL()} alt="Bike - TheBikeComp" />
                            </div>

                        </div>
                        <div className="flex">
                            {
                                [1, 2, 3].map((thumbnail, i) => (
                                    <Thumbnail image={`${process.env.REACT_APP_ADMIN_HOST}${bike[`thumbnail_${thumbnail}`]}`} sel={thumbnail === selThumbnail} key={i} onClick={() => handleOnSelectThumbnail(thumbnail)} />
                                ))
                            }
                        </div>
                    </div>
                    <div className="w-full md:w-1/2 mb-3">
                        <h4 className="hidden md:block text-center font-semibold">
                            {bike.title || ''}
                        </h4>
                        <h5 className="hidden md:block text-center px-3 pt-3 mb-6">
                            {bike.description || ''}
                        </h5>

                       {/*  <h5 className={`${styles.countSelector} mb-6`}>
                            <p className={styles.countList}>
                                {
                                    [1, 5, 10, 20, 50].map((count, i) => (
                                        <span onClick={() => addTickets(count)} key={i}>{count}</span>
                                    ))
                                }
                            </p>
                            <span className="px-1 md:px-2 text-white" style={{ backgroundColor: '#3f3f3f' }}>TICKETS</span>
                        </h5> */}

                        <div className={"row flex md:block justify-around items-center mb-6 d-flex justify-content-center"} >
                          <label>TICKETS</label>
                             
                            <Slider                                
                                step={1}
                                value={tickets} 
                                max={50}
                                min={1}
                                onChange={onChange}
                            />
                            
                        </div> 
                        <div className="flex md:block justify-around items-center mb-6">
                            
                            <div className="flex justify-center">
                                <a href={"#"}  onClick={(e)=>{
                                    e.preventDefault()
                                    addTickets(tickets)
                                }}
                                rel="noopener noreferrer"><span className={styles.button}>  { 'ADD TO BASKET'}</span></a>
                            </div>
                        </div>


                       
                        <div className="flex md:block justify-around items-center mb-6">
                            {/* <div className="flex justify-center my-3">
                                <Link to="/bikes"><span className={styles.button}>ADD TO BASKET</span></Link>
                            </div> */}
                            <div className="flex justify-center">
                                <a href={bike.website ? bike.website : '#'} target="_blank" rel="noopener noreferrer"><span className={styles.button}>VISIT {(bike.brand_name || '').toUpperCase()}</span></a>
                            </div>
                        </div>
                        <h4 className="block md:hidden text-center font-semibold">
                            {bike.title}
                        </h4>
                        <h5 className="block md:hidden text-center p-3">
                            {bike.description}
                        </h5>
                    </div>
                </div>
                
                <div className={`text-right pr-3 mb-2 ${styles.textGray}`}>
                    CASH ALTERNATIVE: &pound;{bike.cash_alternative}
                </div>
            </section>
        </>
    );
}

const mapStateToProps = state => ({

});

const MapDispatchToProps = {
    toggleShoppingCart$: toggleShoppingCart,
    addBikeToCart$: addBikeToCart,
    submitTicket$: submitTicket
};

export default connect(mapStateToProps, MapDispatchToProps)(BikeDetails);