import React from 'react';
import Slider from 'react-rangeslider';

export const Thumbnail = ({ sel, image, onClick }) => {
    return (
        <div className="w-1/3 p-2">
            <div className="">
                <img className={`${sel ? 'opacity-100' : 'opacity-75'}`}
                    src={image}
                    alt="Bike thumbnail - The Bike Comp"
                    onClick={onClick}
                />
            </div>
        </div>
    );
}

export const TickitSelector = ({ min, max, value, onSetValue }) => {
    return (
        <div className="flex mx-auto items-end justify-center mb-3">
            <div className="pb-3 pr-2">
                TICKETS
            </div>
            <div style={{ minWidth: 200 }}>
                <div className="flex justify-between">
                    <p>{min}</p>
                    <p>{max}</p>
                </div>
                <div className="w-full">
                    <Slider
                        min={min}
                        max={max}
                        step={1}
                        value={value}
                        onChange={onSetValue}
                        handleLabel={''}
                    />
                </div>
            </div>
        </div>
    );
}
