import React, { useEffect, useLayoutEffect, useState } from 'react';
import { connect, useDispatch, useSelector } from 'react-redux';
import Select from 'react-select';
import tabsStyle from '../../../assets/css/tabsStyle.css'
import { getSiteCMS } from '../../../redux/actions';
import { BASE_URL } from '../../../utils/common.utils';




import styles from './style.module.scss';

// refer: https://stackoverflow.com/a/19014495
function useWindowSize() {
    const [size, setSize] = useState([0, 0]);
    useLayoutEffect(() => {
        function updateSize() {
            setSize([window.innerWidth, window.innerHeight]);
        }
        window.addEventListener('resize', updateSize);
        updateSize();
        return () => window.removeEventListener('resize', updateSize);
    }, []);
    return size;
}

const filters = [
    { value: 'HTL', label: 'High to Low' },
    { value: 'LTH', label: 'Low to High' },
    { value: 'ATZ', label: 'A to Z' },
    { value: 'ZTA', label: 'Z to A' },
];


const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'Septempber', 'October', 'November', 'December']
// const thisMonth = new Date().getMonth();

const BannerFilter = ({ appLoaded, bikeBrands, bikeTypes, hideBanner = false, hideFilter = false, sortByBrand, sortByType, sortByPriceAtZ }) => {
    const [filter, setFilter] = useState();
    const [brand, setBrand] = useState();
    const [type, setType] = useState();
    // eslint-disable-next-line
    const [width, height] = useWindowSize();

    useEffect(() => {

        return () => { }
    }, [appLoaded]);

    const onFilterChange = (option) => {

        sortByPriceAtZ && sortByPriceAtZ(option)
        setFilter(option);
    }
    const onBrandChange = (brand) => {

        sortByBrand && sortByBrand(brand)
        setBrand(brand);
    }
    const onTypeChange = (type) => {
        sortByType && sortByType(type)
        setType(type);
    }

    const dispatch = useDispatch()
    const site_details = useSelector(state => state.preOrderReducer.sitecms);
    useEffect(() => {
        dispatch(getSiteCMS())
    }, []);
    const siteData = site_details[0];
    const bannerImg = siteData && siteData.bikes_page_banner;
    
    return (
        <section className={styles.section}>
            { !hideBanner &&
                <>
                    <div className={styles.textContainer}>
                        <h1 className="text-center uppercase font-bold">SELECT YOUR bike COMP tickets</h1>
                        <h3 className="text-center mb-2 uppercase font-semibold">a winner every month: {months[new Date().getMonth()]} {new Date().getFullYear()}</h3>
                    </div>
                    <div className={styles.imgWrapper}>

                        {
                            bannerImg ?
                                <img className={styles.image} src={`${BASE_URL}${bannerImg}`} alt="TheBikeComp Banner" />
                                :
                                <img className={styles.image} src="/images/banner_bikes.jpg" alt="TheBikeComp Banner" />
                        }

                    </div>
                </>
            }

            {!hideFilter &&
                <div className="flex flex-wrap justify_end mob_flex_center py-3 bg-bg-filter w-100">
                    <div className={`${styles.selectContainer} `}>
                        <button onClick={() => {
                            sortByPriceAtZ && sortByPriceAtZ('')
                            setBrand('')
                            setType('')
                            setFilter('')
                        }} className={"bg-white hover:bg-gray-200 text-fg-blue2 font-bold py-2 px-4 rounded mb-2"} style={{ borderRadius: 20 }}>Clear filter</button>
                    </div>



                    <div className={`${styles.selectContainer} `}>
                        <Select

                            className={`${styles.selectBox}`}
                            value={filter}
                            onChange={onFilterChange}
                            options={filters}
                            placeholder="Sort By"
                            classNamePrefix="react_select"
                        //   isClearable={true}
                        />
                    </div>
                    <div className={styles.selectContainer}>
                        <Select
                            className={styles.selectBox}
                            value={brand}
                            onChange={onBrandChange}
                            options={bikeBrands}
                            placeholder={width >= 640 ? 'Select Brand' : 'Brand'}
                            classNamePrefix="react_select"
                        //isClearable={true}
                        />
                    </div>
                    <div className={styles.selectContainer}>
                        <Select
                            className={styles.selectBox}
                            value={type}
                            onChange={onTypeChange}
                            options={bikeTypes}
                            placeholder={width >= 640 ? "Select Type" : "Type"}
                            classNamePrefix="react_select"
                        // isClearable={true}
                        />
                    </div>
                </div>}
        </section>
    );
}



const mapStateToProps = state => ({
    appLoaded: state.setting.appLoaded,
    bikeTypes: state.bike.types,
    bikeBrands: state.bike.brands,
});

const mapDispatchToProps = {
}
export default connect(mapStateToProps, mapDispatchToProps)(BannerFilter); 