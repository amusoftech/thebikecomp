import React, { useState } from 'react';


import styles from './style.module.scss';


const HowTtWork = () => {
   
    return (
        <>
            <section className={styles.container}>
              
                <div className={styles.header}>
                    <div className={styles.brand}>
                    <span>HOW DOES IT WORK?</span>
                    </div>
                    <div className="pr-2 pt-2">
                        <CloseIcon className="opacity-75 hover:opacity-100 transition-opacity" onClick={onClose} />
                    </div>
                </div>

                <div className={styles.body}>
                    <div className="w-full md:w-1/2 mb-3">
                        <div className="pl-3">
                            {
                               /*  [1, 5, 10].map((count, i) => (
                                    <span className={`mr-2 ${styles.priceItem}`} key={i}>{count}{i > 0 ? '+' : ''}|<span className={styles.priceNumber}>&pound;{getDiscountPrice(count)}</span></span>
                                )) */
                            }
                            <p className={styles.retailPrice}>retail price: &pound;{bike.retail_price}</p>
                        </div>
                        <div className="p-3">
                            <div className={styles.mainImageCon}>
                                <img className={styles.mainImage} src={getMainImageURL()} alt="Bike - TheBikeComp" />
                            </div>

                        </div>
                        <div className="flex">
                            {
                                [1, 2, 3].map((thumbnail, i) => (
                                    <Thumbnail image={`${process.env.REACT_APP_ADMIN_HOST}${bike[`thumbnail_${thumbnail}`]}`} sel={thumbnail === selThumbnail} key={i} onClick={() => handleOnSelectThumbnail(thumbnail)} />
                                ))
                            }
                        </div>
                    </div>
                    <div className="w-full md:w-1/2 mb-3">
                        <h4 className="hidden md:block text-center font-semibold">
                            {bike.title || ''}
                        </h4>
                        <h5 className="hidden md:block text-center px-3 pt-3 mb-6">
                            {bike.description || ''}
                        </h5>

                        <h5 className={`${styles.countSelector} mb-6`}>
                            <p className={styles.countList}>
                                {
                                    [1, 5, 10, 20, 50].map((count, i) => (
                                        <span onClick={() => addTickets(count)} key={i}>{count}</span>
                                    ))
                                }
                            </p>
                            <span className="px-1 md:px-2 text-white" style={{ backgroundColor: '#3f3f3f' }}>TICKETS</span>
                        </h5>

                        <div className="flex md:block justify-around items-center mb-6">
                            {/* <div className="flex justify-center my-3">
                                <Link to="/bikes"><span className={styles.button}>ADD TO BASKET</span></Link>
                            </div> */}
                            <div className="flex justify-center">
                                <a href={bike.website ? bike.website : '#'} target="_blank" rel="noopener noreferrer"><span className={styles.button}>VISIT {(bike.brand_name || '').toUpperCase()}</span></a>
                            </div>
                        </div>
                        <h4 className="block md:hidden text-center font-semibold">
                            {bike.title}
                        </h4>
                        <h5 className="block md:hidden text-center p-3">
                            {bike.description}
                        </h5>
                    </div>
                </div>
                
                <div className={`text-right pr-3 mb-2 ${styles.textGray}`}>
                    CASH ALTERNATIVE: &pound;{bike.cash_alternative}
                </div>
            </section>
        </>
    );
}

const mapStateToProps = state => ({

});

const MapDispatchToProps = {
    toggleShoppingCart$: toggleShoppingCart,
    addBikeToCart$: addBikeToCart,
    submitTicket$: submitTicket
};

export default connect(mapStateToProps, MapDispatchToProps)(HowTtWork);