import React from 'react';
import { CollectItem, SectionContainer, SectionTitle } from './Widgets';
import styles from './style.module.scss';

const WhatCollects = () => {
    const personalInfo = [
        { name: 'Identity', description: "first name, surname, TheBikeComp log-in information (password), country of residence" },
        { name: 'Contact', description: 'email address, telephone numbers and address' },
        { name: 'Financial', description: 'payment card details, billing address, purchase information, payment history' },
        { name: 'Profile', description: 'your preferences for marketing, other website preferences and feedback on your User interface experiences through reviews and surveys' },
        { name: 'Social', description: 'if you choose to open a TheBikeComp account using your Facebook or Google account, we will use your contact information used for the relevant account to help populate your TheBikeComp registration page' },
    ];
    const cookieItems = [
        { name: 'Usage', description: ' information about how you use our website, including time spent on page, click-throughs, download errors' },
        { name: 'Technical', description: 'IP address, browser type, hardware type, network and software identifiers, device information, operating system and system configuration' },
    ];
    return (
        <SectionContainer>
            <SectionTitle text="WHAT INFORMATION DO WE COLLECT AND WHAT DO WE USE IT FOR?" />
            <p className="mb-5">You may provide us with the following types of personal information when you register with The  Bike Comp or otherwise when you directly interact with us (when using our website or otherwise):</p>
            {
                personalInfo && personalInfo.map((info, i) => (
                    <CollectItem {...info} key={i} />
                ))
            }
            <p className="mt-10 mb-5">We may collect the following types of information from you when you use our website (using <span className="text-orange-600">Cookies</span> or other tracking technologies):</p>
            {
                cookieItems.map((item, i) => (
                    <CollectItem {...item} key={i} />
                ))
            }
            <p className="my-8">The table below sets out how we use your personal data and our lawful basis for doing so. We may process your personal data for more than one lawful basis depending on the specific purpose for which we are using it. Importantly, we will only use your personal data when the law allows us to.</p>
            <table className={styles.table}>
                <thead>
                    <tr className="text-center">
                        <th>Reason why we use the data</th>
                        <th>What data</th>
                        <th>Legal ground for using the data</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Register you as a TheBikeComp  customer</td>
                        <td>Identity, Contact, Profile, Social</td>
                        <td>Performance of a contract with you</td>
                    </tr>
                    <tr>
                        <td>Enable you to log-in to your TheBikeComp  account</td>
                        <td>Identity, Contact, Social</td>
                        <td>Performance of a contract with you</td>
                    </tr>
                    <tr>
                        <td>Enable you to participate in a TheBikeComp  competition</td>
                        <td>Identity, Contact, Financial, Surprise</td>
                        <td>Performance of a contract with you</td>
                    </tr>
                    <tr>
                        <td>To process payments which you make through our website</td>
                        <td>Identity, Contact, Financial</td>
                        <td>Performance of a contract with you</td>
                    </tr>
                    <tr>
                        <td>Arrange for the delivery of your prize if you have won a competition</td>
                        <td>Identity, Contact, Surprise</td>
                        <td>Performance of a contract with you</td>
                    </tr>
                    <tr>
                        <td>For internal administration and record keeping purposes</td>
                        <td>All</td>
                        <td>Performance of a contract with you<br/><br/> Necessary to comply with a legal obligation<br/><br/> Necessary for our legitimate interests (for effective business administration and service provision)
                        </td>
                    </tr>
                    <tr>
                        <td>Notify you of changes to our Privacy Policy, our Terms and Conditions or other changes to our services or products</td>
                        <td>Identity, Contact</td>
                        <td>Performance of a contract with you<br/><br/> Necessary to comply with a legal obligation</td>
                    </tr>
                    <tr>
                        <td>Answer your enquiries which may involve contacting you by post, e-mail or phone</td>
                        <td>Identity, Contact</td>
                        <td>Performance of a contract with you<br/><br/> Necessary for our legitimate interests (to ensure our customers are informed and satisfied with our services)</td>
                    </tr>
                    <tr>
                        <td>Get in touch with you about relevant competitions, products and services</td>
                        <td>Identity, Contact, Profile</td>
                        <td>Necessary for our legitimate interests (to develop our business, including our competitions, products and services)<br/><br/> Consent</td>
                    </tr>
                    <tr>
                        <td>Contact you about third party products and services which we believe may be relevant to you or pass your details on to third parties to contact you directly about the same (in each case, only where you have indicated you would like to hear about these)</td>
                        <td>Identity, Contact, Profile</td>
                        <td>Consent</td>
                    </tr>
                    <tr>
                        <td>Improve and personalise your experience of TheBikeComp  website by delivering more relevant content and advertising whilst you browse</td>
                        <td>Identity, Contact, Profile, Usage, Technical</td>
                        <td>Necessary for our legitimate interests (to develop our business, improve our website and overall user experience and inform our marketing strategy)</td>
                    </tr>
                    <tr>
                        <td>Administer TheBikeComp  website, including website troubleshooting, testing and analysis and to enable you to participate in interactive features of our website</td>
                        <td>All</td>
                        <td>Performance of a contract with you<br/><br/>Necessary for our legitimate interests (to ensure that our website is fully functional and operating in the most effective way for you)</td>
                    </tr>
                    <tr>
                        <td>Verify your identity and detect fraud and security issues</td>
                        <td>All</td>
                        <td>Necessary for our legitimate interests (to prevent and detect fraudulent activity, security incidents and criminal activity)</td>
                    </tr>
                    <tr>
                        <td>Give you the opportunity to provide us with feedback through reviews and surveys</td>
                        <td>Identity, Contact, Profile, Usage, Technical</td>
                        <td>Necessary for our legitimate interests (to develop our business, promote new products and services, obtain feedback from customers to improve our services)</td>
                    </tr>
                </tbody>
            </table>

            <p className="mt-8">We will only use your personal data for the purposes for which we collected it, unless we reasonably consider that we need to use it for another reason and that reason is compatible with the original purpose. If we need to use your personal data for an unrelated purpose, we will notify you and we will explain the legal basis which allows us to do so.</p>
            <p className="mt-8">In addition to the above, we may also anonymise and aggregate your personal data in a way which means you cannot be identified. This may be helpful to us for testing our internal systems, carrying out research and general customer data analysis. Because this is not personally identifiable, we can use this for any purposes.</p>
        </SectionContainer>
    );
}

export default WhatCollects;