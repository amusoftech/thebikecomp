import React from 'react';
import styles from './style.module.scss';

export const SectionContainer = ({ children }) => {
    return (
        <section className="px-3 mt-16" id="privacy-page" style={{ color: '#676767', fontSize: 14 }}>{ children }</section>
    );
}

export const SectionTitle = ({ text }) => {
    return (
    <h2 className={styles.sectionTitle}>{text}</h2>
    );
}

export const CollectItem = ({ name, description }) => (
    <li className=""><span className="font-semibold leading-10">{name}</span> - {description}</li>
)