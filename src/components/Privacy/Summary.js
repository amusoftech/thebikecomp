import React from 'react';
import { SectionContainer } from './Widgets';
import styles from './style.module.scss';

const company_name = 'XXXXX'
const company_number = 'XXXXXXXX';
const company_address = 'X XXXXX XXXX XXX';
const business_name = 'XXXXXXXX';

const Summary = () => {
    return (
        <SectionContainer>
            <p className="mb-3 font-medium text-black">At The Bike Comp, we are 100% committed to protecting the privacy and security of our customers and site visitors. We understand the trust you place in our business when you provide us with personal information. Our Privacy Policy sets out our privacy promise to you. We do not share your data with third parties for them to market their products/services to you. If you have any questions about how we Protect Your Privacy, please contact us at
            <a className={styles.link} href="mailto:info@thebikecomp.com" rel="noopener noreferrer" target="_blank"> info@thebikecomp.com.</a></p>

            <p className="mb-2">Thebikecomp.com is the website of TM Competitions LTD trading as The Bike Comp, registered in the United Kingdom (company number 12962902, registered address The Bike Comp, 3rd Floor, 86-90 Paul Street, London, EC24 4NE,). Any reference in this policy to <b>“thebikecomp”, “we”, “us”</b>, or <b>“our” </b>is to TM Competitions LTD trading as The Bike Comp.</p>


            <p className="mb-2">When you interact with us through our website (or otherwise) you may provide, or we may collect, certain information from which you are personally identifiable (which is referred to as personal data). For the purposes of the General Data Protection Regulation or “GDPR” (and all other laws relating to the use of your personal data), we are the data controller responsible for deciding how your personal data is used and for keeping your data safe only using it for legitimate reasons.</p>
            <p  className="mb-2">We are committed to protecting your privacy and will take all steps necessary to comply with our legal obligations when using your personal data. This Privacy Policy explains how we fulfil this commitment. Please read this carefully.
</p>
        </SectionContainer>
    );
}

export default Summary;