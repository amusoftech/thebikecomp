 import React from 'react';
 import { SectionContainer, SectionTitle } from './Widgets';
 import styles from './style.module.scss';

 const WhatTellsYou = () => {
     const items = [
         'What types of personal data you provide to us (or which we collect from you) when using our website or when you directly interact with us on other occasions;',
         'How and why we use this data and the reasons we are legally allowed to do so;',
         'Who we share your data with;',
         'Your rights over your data and how you can exercise those rights; and',
         'How to contact us if you have any issues or want to find out more.'
     ];
     return (
        <SectionContainer>
            <SectionTitle text="What this policy tells you" />
            <ol>
                {
                    items.map((item, i) => (
                    <li className={styles.listItem} key={i}>{item}</li>
                    ))
                }
            </ol>
        </SectionContainer>
     );
 }

 export default WhatTellsYou;