import React from 'react';
import { Link } from 'react-router-dom';
import { SectionContainer, SectionTitle } from './Widgets';
import styles from './style.module.scss';

const WhoShareWith = () => {
    const items = [
        'Delivering relevant TheBikeComp email and text marketing (to the extent you have not unsubscribed)',
        'Our customer reviews and surveys',
        'Personalising the content on our website to ensure a tailored user experience',
        'Delivering relevant targeted and re-targeted advertising to keep you up to date with our services',
        'Detecting fraud or criminal activity',
        'Creating your TheBikeComp account (being your social media companies who you have used to provide log-in information as part of the sign-up process)',
        'Running our competitions, such as our auditors, judges, professional advisors',
        'Other aspects of our service delivery, such as hosting our website and processing customer payments',
    ];
    return (
        <SectionContainer>
            <SectionTitle text="WHO DO WE SHARE YOUR DATA WITH?" />
            <p className="mt-6">Importantly, we do not pass your personal data onto any third parties for them to market their products/services to you. If in the future we decide that we want to, we will only do so if we have your consent.</p>
            <p className="mt-8">We do however share your personal data with third parties to help us deliver our products and services to you in the most effective way possible. These include third parties who assist us with:</p>
            <ul className="list-disc mt-5 pl-12">
                {
                    items && items.map((item, i) => (
                        <li className="leading-8" key={i}>{item}</li>
                    ))
                }
            </ul>
            <p className="mt-8">If we share personal data with third parties, we will ensure that access is limited on a strictly need to know basis and is subject to suitable obligations relating to confidentiality and security. Please note that certain of these third party service providers use cookies or other tracking technologies, which are explained more fully in our <Link to="/cookie-policy"><span className={styles.link}>Cookies Policy</span></Link></p>
            <p className="mt-8">In addition to the above, we may also be required to share your personal data with third parties if required by law or regulation. In such circumstances, we will make sure that the disclosure is only to the extent required by law or regulation.</p>
        </SectionContainer>
    )
}

export default WhoShareWith;