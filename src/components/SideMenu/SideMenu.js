import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link, withRouter, useHistory } from "react-router-dom";
import { logout } from "../../redux/actions";
import { toggleMenu } from '../../redux/actions/setting.actions'
import styles from './style.module.scss';



class SideMenu extends Component {
    handleMenuClick = () => {
        this.props.toggleMenu$(false);
    }
    render() {
        return (
            <div className="px-3 py-6">
                <div className="flex justify-center">
                    <img className={styles.logo} src="/logo256-mix.png" alt="Sidemenu Logo of TheBikeComp" />
                </div>
                <div>
                    {this.props.loggedIn && this.props.user && <p className="text-sm text-center text-gray-600">You're logged in as <span className="text-lg text-gray-800 font-bold">{`${this.props.user.name} ${this.props.user.surname}`}</span>.</p>}
                </div>
                <ul className="mt-8 list-style-none">
                    {this.props.loggedIn && this.props.user && <Link to="/my-profile">
                        <li className={styles.navMenuItem} onClick={this.handleMenuClick}>My Profile</li>
                    </Link>}
                    {/*    <Link to="/my-profile"><li className={styles.navMenuItem} onClick={this.handleMenuClick}>My Profile</li></Link> */}
                    <Link to="/"><li className={styles.navMenuItem} onClick={this.handleMenuClick}>Home</li></Link>
                    <Link to="/bikes"><li className={styles.navMenuItem} onClick={this.handleMenuClick}>Bikes</li></Link>
                    <Link to="/about-us"><li className={styles.navMenuItem} onClick={this.handleMenuClick}>About</li></Link>

                    {!this.props.loggedIn && <Link to="/login"><li className={styles.navMenuItem} onClick={this.handleMenuClick}>Login</li></Link>}
                    {!this.props.loggedIn && <Link to="/signup"><li className={styles.navMenuItem} onClick={this.handleMenuClick}>Signup</li></Link>}

                    {this.props.loggedIn && <Link to="/"><li className={styles.navMenuItem} onClick={() => { this.props.logout$() }}>Log out</li></Link>}
                </ul>
            </div>
        );
    }
}
const mapStateToProps = state => ({
    loggedIn: state.auth.login,
    user: state.auth.user,
});

const MapDispatchToProps = {
    logout$: logout,
    toggleMenu$: toggleMenu
};

export default connect(mapStateToProps, MapDispatchToProps)(withRouter(SideMenu));