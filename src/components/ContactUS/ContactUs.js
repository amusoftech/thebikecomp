import React, { useState,useRef } from 'react';
import { EmailIcon, MobileIcon, MapMarkerIcon } from '../Icons';
import styles from './style.module.scss';
import { useForm } from 'react-hook-form';
import { Link, useLocation, useHistory } from "react-router-dom";
import { contact } from '../../redux/actions';
import { connect } from 'react-redux';



const validations = {
    name: {
        required: 'Name is required!',
        minLength: {
            value: 3,
            message: 'Name must be at least 3 characters long!'
        }
    },
    surname: {
        required: 'Surname is required!',
        minLength: {
            value: 3,
            message: 'Surname must be at least 3 characters long!'
        }
    },
    email: {
        required: 'Email is required!',
        minLength: {
            value: 5,
            message: 'Email must be at least 5 characters long!'
        },
        pattern: {
            // eslint-disable-next-line
            value: /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i,
            message: 'Please input valid email address!'
        }
    },

    phone: {
        required: 'Phone number is required!',
        minLength: {
            value: 10,
            message: 'Phone number must be at least 10 characters long!'
        }
    },
    message: {
        required: 'Message is required!',
        minLength: {
            value: 5,
            message: 'Message must be at least 5 characters long!'
        }
    },


};

const ContactUsForm = ({ siteData, contact$, }) => {
const formref = useRef()
    const { handleSubmit, errors, register, trigger } = useForm()
    const history = useHistory();
    const location = useLocation();
    const [serverError, setserverError] = useState();

    const handleOnChange = (e) => {
        setserverError(undefined)
        trigger(e.target.name)
    }
    const triggerAllFields = () => {

        const names = ['name', 'surname', 'email', 'address', 'phone', 'password', 'agree'];
        for (let name of names) {
            trigger(name);
        }
        setserverError(undefined);
    }



    const onSubmit = async data => {

        contact$(data, async (result) => {
            if (data.result) {

                // check if there is redirect param
                const search = window.location.search;
                const params = new URLSearchParams(search);
                const redirect = params.get('redirect');

                if (!!redirect) {
                    history.push(redirect);
                } 
            } else {
                if (result) {
                    formref && formref.current.reset();
                    setserverError(result.message)
                }
             

            }
        })
    }

    return (
        <div className="flex flex-wrap border" id="contact-form">
            <div className="w-full sm:w-7/12 p-5">
                <p className="text-xl font-semibold">Send us a Message</p>
                {/*<form onSubmit={onSubmit} > */}
                <form ref={formref} onSubmit={handleSubmit(onSubmit)}> 
                    <div className="form-group">
                        <label>First Name</label>
                        <input className="form-control" placeholder="First Name" name="name"
                            onChange={handleOnChange}
                            ref={register(validations.name)}

                        />
                        {errors.name && (
                            <p style={{ position: 'absolute' }} className={`error_msg ${styles.error}`}>{errors.name.message}</p>
                        )}
                    </div>
                    <div className="form-group">
                        <label>Last Name</label>
                        <input className="form-control" placeholder="Last Name" name="surname"
                            onChange={handleOnChange}
                            ref={register(validations.surname)}
                        />
                        {errors.surname && (
                            <p style={{ position: 'absolute' }} className={`error_msg ${styles.error}`}>{errors.surname.message}</p>
                        )}
                    </div>

                    <div className="form-group">
                        <label>Email</label>
                        <input className="form-control" placeholder="Email" name="email" onChange={handleOnChange}
                            ref={register(validations.email)}

                        />
                        {errors.email && (
                            <p style={{ position: 'absolute' }} className={`error_msg ${styles.error}`}>{errors.email.message}</p>
                        )}
                    </div>
                    <div className="form-group">
                        <label>Phone</label>
                        <input className="form-control" placeholder="Phone" name="phone" onChange={handleOnChange}
                            ref={register(validations.phone)}
                        />
                        {errors.phone && (
                            <p style={{ position: 'absolute' }} className={`error_msg`}>{errors.phone.message}</p>
                        )}
                    </div>
                    <div className="form-group">
                        <label>Phone</label>
                        <textarea className="form-control" placeholder="Message" name="message" onChange={handleOnChange}
                            ref={register(validations.message)}></textarea>

                        {errors.message && (
                            <p style={{ position: 'absolute' }} className={`error_msg`}>{errors.message.message}</p>
                        )}

                        {
                            serverError &&
                            <p
                                style={{ position: "absolute" ,color:'green'}}
                              
                            >
                                {serverError}
                            </p>

                        }
                    </div>
                    <div className="flex justify-end">
                        <button className="py-2 px-8 border bg-gray-900 hover:bg-gray-800 text-white" onClick={triggerAllFields}>Send Now!</button>
                    </div>
                </form>
            </div>
            <div className={`w-full sm:w-5/12 p-5 ${styles.contactInfo}`}>
                {/* <div className="info-item items-center">
                    <MobileIcon />
    <div>{siteData &&siteData.site_phone_number}</div>
                </div> */}
                <div className="info-item items-center">
                    <EmailIcon />
                    <div><a class="common-link" href={`mailto:${"info@thebikecomp.com"}`} rel="noopener noreferrer" target="_blank">{siteData && siteData.site_email}</a></div>
                </div>
                <div className="info-item items-start">
                    <MapMarkerIcon />
                    <div>
                        {siteData && siteData.site_address}<br />
                    </div>
                </div>
            </div>
        </div>
    );
}
const mapStateToProps = state => ({
    appLoaded: state.setting.appLoaded,
});
const mapDispatchToProps = {
    contact$: contact
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactUsForm);

