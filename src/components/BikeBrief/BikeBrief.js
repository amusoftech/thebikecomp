import React from 'react';
import { connect } from 'react-redux';
// import { toastr } from 'react-redux-toastr';

import { addBikeToCart, submitTicket, toggleShoppingCart } from '../../redux/actions';

import styles from './style.module.scss';

const BikeBrief = ({ bike, onClick, setting, addBikeToCart$, toggleShoppingCart$ , submitTicket$}) => {
    const addTickets = (count) => {
        
        addBikeToCart$(bike, count);
        submitTicket$(bike.id,{x:10,y:10}) //faking the ticket submssion
        toggleShoppingCart$(true);
        setTimeout(function(){
            // console.log('[gonna close cart]', setting);
            if (setting.cartHover === false) toggleShoppingCart$(false);
        }, 5000);
    }
    const showBikeDetails = () => {
        onClick();
    }
    const getBikeImage = () => {
        return `${process.env.REACT_APP_ADMIN_HOST}${bike.main_image}`;
    }
    const getDiscountPrice = (count) => {
        const discounted =  Number(bike.price) * (100 - Number(bike[`price_${count}`] || 0)) / 100;
        return discounted.toFixed(2);
    }
    return (
        <div className={styles.wrapper}>
            <div className={styles.sectionContainer}>
                <div className={styles.hoverPrice}>
                    <h6 className="leading-none">
                        {/* <span className={styles.hoverCount}>1</span>| */}<span className={styles.price}>&pound;{bike.price}</span>
                        {/* <span className={styles.hoverCount}>5+</span><span className={styles.price}>|&pound;{getDiscountPrice(5)}</span>
                        <span className={styles.hoverCount}>10+</span><span className={styles.price}>|&pound;{getDiscountPrice(10)}</span> */}
                    </h6>
                    <h5 className={styles.countSelector}>
                        <p className={styles.countList}>
                            {
                                [1,5,10,20,50].map((count, i) => (
                                    <span onClick={()=>addTickets(count)} key={i}>{count}</span>
                                ))
                            }
                        </p>
                        <span className="px-1 md:px-2 text-white" style={{backgroundColor: '#3f3f3f'}}>TICKETS</span>
                    </h5>
                </div>
                <div className="null-wrapper" onClick={showBikeDetails}>
                <h5 className="font-bold">{bike.brand_name} <span className="float-right font-normal">&pound;{bike.price}</span></h5>
                <h5 className="text-sm-bike mb-2 truncate ...">{bike.model}</h5>
                <div className={styles.imgWrapper}>
                    <img className={styles.bikeImg} src={getBikeImage()} alt="Bike" />
                </div>
                <p className={`retailsPrice ${styles.retailPrice}`}>Retail price : &pound;{bike.retail_price}</p>
                </div>
            </div>
        </div>
    );
}

const mapStateToProps = state => ({
    setting: state.setting,
}); 

const MapDispatchToProps = {
    toggleShoppingCart$: toggleShoppingCart,
    addBikeToCart$: addBikeToCart,
    submitTicket$: submitTicket
};

export default connect(mapStateToProps, MapDispatchToProps)(BikeBrief);