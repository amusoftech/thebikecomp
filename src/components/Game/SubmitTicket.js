import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

import GameTicket from './GameTicket';
import { setMenuLayer } from '../../redux/actions';

import styles from './style.module.scss';


const SubmitTicket = ({ appLoaded, items, range, onSetHScroll, onSetVScroll, setMenuLayer$ }) => {

    const [selTicketId, selectTicket] = useState(0);

    useEffect(() => {
        setMenuLayer$(-1);
        return () => {
            setMenuLayer$(1);
        }
    }, [appLoaded]);

    return (
        <div>
            <div className={styles.ticketContainer}>
                {
                    items.map((cartItem, i) => (
                        <GameTicket
                            item={cartItem}
                            onClick={() => selectTicket(i)}
                            onSetDistance={onSetHScroll}
                            onSetHeight={onSetVScroll}
                            range={range}
                            selected={i === selTicketId}
                            key={i}
                        />
                    ))
                }
            </div>
            <div className="flex justify-end mb-10 px-3 max-w-screen-sm mx-auto lg:max-w-none">

                <Link to="/checkout">
                    <button className={styles.btnCheckout}>Checkout</button>
                </Link>
            </div>
        </div>
    );
}

const mapStateToProps = state => ({
    appLoaded: state.setting.appLoaded,
    items: state.cart.items,
});

const mapDispatchToProps = {
    setMenuLayer$: setMenuLayer,
}

export default connect(mapStateToProps, mapDispatchToProps)(SubmitTicket);