import React from 'react';
import { connect } from 'react-redux';

import { addBikeToCart, deleteBike, submitTicket } from '../../redux/actions'
import { CheckCircleIcon, PlusCircleIcon, TrashIcon } from '../Icons';
import styles from './style.module.scss';

const GameTicket = ({ item, range, selected, onClick, onSetDistance, onSetHeight, addBikeToCart$, deleteBike$, submitTicket$ }) => {

    const handleOnAdd = () => {
        console.log('Click');
        addBikeToCart$(item.bike, 1);
    }

    const deleteTicket = () => {
        deleteBike$(item.bike);
    }

    const handleOnDistanceChange = (e) => {
        if (selected) {
            onSetDistance(e.target.value);
        } else {
            
        }
    }

    const handleOnHeightChange = (e) => {
        if (selected) {
            onSetHeight(e.target.value);
        }
    }

    const handleOnSubmit = () => {
        console.log('Submit');
        submitTicket$(item.bike.id, range)
    }
    return (
        <div className={styles.ticket}
            onClick={onClick}>
            <div className={`${styles.ticketWrapper} ${selected ? styles.selected : ''}`} >
                <div className={styles.ticketTopContainer}>
                    <div className={styles.ticketInfoGroup}>
                        <p>{item.bike.brand_name}</p>
                        <p className="model">{item.bike.model}</p>
                    </div >
                    <div className={styles.ticketCounter}>
                        <p><span>Ticket</span> <span>{selected ? item.ticketSubmitIndex : item.ticketSubmitIndex} of {item.amount}</span></p>
                    </div>
                    <div>
                        <TrashIcon className={styles.trashIcon} width={22} height={24} onClick={deleteTicket}/>
                    </div>
                </div>

                <div className={styles.ticketBottomContainer}>
                    <div className="w-5/12">
                        <div className={styles.bikeImg}>
                            <img src={`${process.env.REACT_APP_ADMIN_HOST}${item.bike.main_image}`} />
                        </div>
                    </div>

                    <div className="w-7/12 flex items-end">
                        <div className="w-7/12 flex flex-col justify-center">
                            <div className={styles.ticketInputGroup}>
                                <label>Distance:</label> 
                                <input type="number"
                                    step="0.02"
                                    min={0}
                                    max={30}
                                    value={selected ? range.x : ''}
                                    onChange={handleOnDistanceChange}
                                />
                            </div>
                            <div className={styles.ticketInputGroup}>
                                <label>Height:</label>
                                <input type="number"
                                    step="0.01"
                                    min={0}
                                    max={20}
                                    value={selected ? range.y : ''}
                                    onChange={handleOnHeightChange}
                                />
                            </div>
                        </div>
                        <div className="w-5/12 flex justify-end">
                            <div className={styles.ticketBottomButton}
                                onClick={handleOnAdd}>
                                <PlusCircleIcon/>
                                <span>Add</span>
                            </div>
                            <div className={styles.ticketBottomButton}
                                onClick={handleOnSubmit} >
                                <CheckCircleIcon />
                                <span>Submit</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

const mapStateToProps = state => ({
    appLoaded: state.setting.appLoaded,
});

const mapDispatchToProps = {
    addBikeToCart$: addBikeToCart,
    deleteBike$: deleteBike,
    submitTicket$: submitTicket,
}

export default connect(mapStateToProps, mapDispatchToProps)(GameTicket);