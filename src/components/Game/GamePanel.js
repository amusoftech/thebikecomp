import React, { Component, createRef } from 'react';

import Slider from 'react-rangeslider'

import { DRAW_TOOL_LINE, DRAW_TOOL_CURVE } from '../../constants/common.constants';
import { checkInDistance, Parabola } from '../../utils/common.utils';
// import { useWindowSize } from '../../utils/customHooks';

import './rangeslider.css';
import styles from './style.module.scss';

const bgImage = 'images/game1.png';

class GamePanel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // hRange: 0,
            // vRange: 0,
            winHeight: 0,
            winWidth: 0,
            board: { x: 0, y: 0 },
            shapes: [], // {mode: '', points: []}
            drawTool: 'none',
            drawPoints: [],
        };

        // global variable
        this.bgImage = undefined;

        // ref: https://blog.logrocket.com/a-guide-to-react-refs/
        this.hRulerCanvasRef = createRef();
        this.vRulerCanvasRef = createRef();
        this.mainCanvasRef = createRef();
        this.bgImageRef = createRef();

        // binding methods
        this.handleBoardDoubleClick = this.handleBoardDoubleClick.bind(this);
        this.handleBoardMouseDown = this.handleBoardMouseDown.bind(this);
        this.handleBoardMouseMove = this.handleBoardMouseMove.bind(this);
        this.toggleLineTool = this.toggleLineTool.bind(this);
        this.toggleCurveTool = this.toggleCurveTool.bind(this);
        this.drawLine = this.drawLine.bind(this);
        this.underDrawTempLine = this.underDrawTempLine.bind(this);
    }

    componentDidMount() {
        window.addEventListener('resize', this.updateDimentions);
        this.drawHRulerDrawingScale();
        this.drawVRulerDrawingScale();
        this.drawBgImageOnMain();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateDimentions);
    }

    drawHRulerDrawingScale() {
        const ctx = this.hRulerCanvasRef.current.getContext("2d");
        // ref: https://stackoverflow.com/questions/4032179/how-do-i-get-the-width-and-height-of-a-html5-canvas
        this.hRulerCanvasRef.current.width = this.hRulerCanvasRef.current.getBoundingClientRect().width;
        this.hRulerCanvasRef.current.height = this.hRulerCanvasRef.current.getBoundingClientRect().height;

        ctx.fillStyle = "#000";
        ctx.lineWidth = 0.5;

        const unitWidth = (this.hRulerCanvasRef.current.width - 30) / 30;
        for (let i = 0; i <= 30; i++) {
            if (i % 5 === 0) {
                ctx.strokeStyle = "#353535";
                ctx.font = "12px Courier bold";
                ctx.fillText(i.toFixed(2) + 'm', i * unitWidth + (i > 10 ? -5 : 3), 30);
            } else {
                ctx.strokeStyle = "#898989";
            }
            ctx.moveTo(i * unitWidth + 14, 15);
            ctx.lineTo(i * unitWidth + 14, (i % 5 === 0) ? 0 : 10);
            ctx.stroke();

        }
    }

    drawVRulerDrawingScale() {
        const ctx = this.vRulerCanvasRef.current.getContext("2d");
        // ref: https://stackoverflow.com/questions/4032179/how-do-i-get-the-width-and-height-of-a-html5-canvas
        this.vRulerCanvasRef.current.width = this.vRulerCanvasRef.current.getBoundingClientRect().width;
        this.vRulerCanvasRef.current.height = this.vRulerCanvasRef.current.getBoundingClientRect().height;

        ctx.fillStyle = "#000";
        ctx.lineWidth = 0.5;

        const unitWidth = (this.vRulerCanvasRef.current.height - 30) / 20;
        for (let i = 0; i <= 20; i++) {
            if (i % 5 === 0) {
                // ref: https://stackoverflow.com/a/5400970
                // ctx.save();
                // ctx.translate(20, 0);
                // ctx.rotate(- Math.PI / 2);
                ctx.strokeStyle = "#353535";
                ctx.font = "12px Courier bold";
                ctx.fillText((20 - i).toFixed(2) + 'm', 15, i * unitWidth + 18);
                // ctx.fillText(i.toFixed(1) + 'm',50, 10);
                // ctx.restore();
                // ctx.rotate(Math.PI / 2);
            } else {
                ctx.strokeStyle = "#898989";
            }
            ctx.moveTo(15, i * unitWidth + 14);
            ctx.lineTo((i % 5 === 0) ? 0 : 10, i * unitWidth + 14);
            ctx.stroke();

        }
    }

    drawBgImageOnMain() {
        // get context
        const ctx = this.mainCanvasRef.current.getContext('2d');
        this.mainCanvasRef.current.width = this.mainCanvasRef.current.getBoundingClientRect().width;
        this.mainCanvasRef.current.height = this.mainCanvasRef.current.getBoundingClientRect().height;

        this.bgImage = this.bgImageRef.current;
        this.bgImage.onload = () => {
            ctx.drawImage(this.bgImage, 0, 0, this.bgImage.width, this.bgImage.height, 0, 0, this.mainCanvasRef.current.width, this.mainCanvasRef.current.height);
        }
    }

    setHRangeValue = val => {
        // val = Number(val).toFixed(2);
        // this.setState({ ...this.state, hRange: val });
        const { onSetHScroll } =  this.props;
        onSetHScroll(val);
    }

    setVRangeValue = val => {
        // val = Number(val).toFixed(2);
        // this.setState({ ...this.state, vRange: val });
        const { onSetVScroll } = this.props;
        onSetVScroll(val);
    }

    toggleLineTool = () => {
        const curDrawTool = this.state.drawTool;
        this.setState({ ...this.state, drawTool: curDrawTool === DRAW_TOOL_LINE ? 'none' : DRAW_TOOL_LINE, drawPoints: [] });
    }

    toggleCurveTool = () => {
        const curDrawTool = this.state.drawTool;
        this.setState({ ...this.state, drawTool: curDrawTool === DRAW_TOOL_CURVE ? 'none' : DRAW_TOOL_CURVE });
    }

    handleBoardMouseDown = (e) => {
        // this.setState({...this.state, board: {x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY}})
        // this.refreshMainBoardDraw();        
        if (this.state.drawTool === 'none') {

        } else if (this.state.drawTool === DRAW_TOOL_LINE) {
            if (this.state.drawPoints.length > 0) {
                // this is the second point(last point, termiate drawing line)
                let curDrawPoints = this.state.drawPoints;
                curDrawPoints = [curDrawPoints[0], { x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY, click: false }]; // console.log(curDrawPoints);
                this.setState({ ...this.state, drawPoints: curDrawPoints });
                // // push it to the array of the compelted shapes
                // let currentShapes = this.state.shapes || [];
                // currentShapes.push({ mode: this.state.drawTool, points: curDrawPoints });

                // initialize the draw tool(mode)
                // this.setState({ ...this.state, drawPoints: [] });
            } else {
                // this.state.drawPoints.push({x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY});
                this.setState({ ...this.state, drawPoints: [{ x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY, click: true }] });
            }
        } else if (this.state.drawTool === DRAW_TOOL_CURVE) {
            if (this.state.drawPoints.length > 0) {
                let curDrawPoints = this.state.drawPoints;
                curDrawPoints = curDrawPoints.filter(point => point.click);
                curDrawPoints.push({ x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY, click: false });

                this.setState({ ...this.state, drawPoints: curDrawPoints });
            } else {
                // add the first point
                this.setState({ ...this.state, drawPoints: [{ x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY, click: true }] });
            }
        } else {

        }
        this.refreshMainBoardDraw();
    }

    handleBoardMouseMove = e => {
        if (this.state.drawTool === 'none') {

        } else if (this.state.drawTool === DRAW_TOOL_LINE) {
            // console.log(this.state.drawPoints);
            if (this.state.drawPoints.length > 0) {
                // set second point
                this.setState({ ...this.state, drawPoints: [this.state.drawPoints[0], { x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY, click: false }] });
            }
        } else if (this.state.drawTool === DRAW_TOOL_CURVE) {
            if (this.state.drawPoints.length > 0) {
                let curDrawPoints = this.state.drawPoints.filter(point => point.click);
                curDrawPoints.push({ x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY, click: false });
                this.setState({ ...this.state, drawPoints: curDrawPoints });
            }
        }

        this.refreshMainBoardDraw();
    }

    handleBoardMouseUp = (e) => {
        if (this.state.drawTool === 'none') {

        } else if (this.state.drawTool === DRAW_TOOL_LINE) {
            // console.log(this.state.drawPoints);
            if (this.state.drawPoints.length === 2) {
                // if (distance(this.state.drawPoints[0].x, this.state.drawPoints[0].y, e.nativeEvent.offsetX, e.nativeEvent.offsetY) < 10) {
                //     this.setState({ ...this.state, drawPoints: [] });
                //     return;
                // }

                // let clickPoints = this.state.drawPoints.filter(point => point.click);
                // if (clickPoints.length < 2) {
                //     return;
                // }

                // this is the second point(last point, termiate drawing line)
                let curDrawPoints = this.state.drawPoints;
                // curDrawPoints.push({ x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY });
                curDrawPoints = [curDrawPoints[0], { x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY, click: true }];

                // push it to the array of the compelted shapes
                let currentShapes = this.state.shapes || [];

                currentShapes.push({ mode: this.state.drawTool, points: curDrawPoints });


                // initialize the draw tool(mode)
                this.setState({ ...this.state, drawPoints: [], shapes: currentShapes });
            } else {
                // this.state.drawPoints.push({x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY});
                this.setState({ ...this.state, drawPoints: [{ x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY, click: true }] });
            }
        } else if (this.state.drawTool === DRAW_TOOL_CURVE) {
            if (this.state.drawPoints.length > 0) {
                
                const point = {x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY};
                let clickPoints = this.state.drawPoints.filter(point => point.click);
                // prevent same points
                if (checkInDistance(clickPoints, point, 10)) { return false; }
                clickPoints.push({ x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY, click: true });

                if (clickPoints.length === 3) {
                    // console.log(clickPoints);
                    let currentShapes = this.state.shapes || [];
                    currentShapes.push({ mode: DRAW_TOOL_CURVE, points: clickPoints });
                    this.setState({ ...this.state, drawPoints: [], shapes: currentShapes });
                } else {
                    this.setState({ ...this.state, drawPoints: clickPoints });
                }
            } else {
                this.setState({ ...this.state, drawPoints: [{ x: e.nativeEvent.offsetX, y: e.nativeEvent.offsetY, click: true }] });
            }
        }
        this.refreshMainBoardDraw();
    }

    // right click
    contextMenu = (e) => {
        e.preventDefault();
        this.operationBack();
    }

    operationBack = () => {
        let currentShapes = this.state.shapes;
        // console.log('[Right Click]', currentShapes.length ,currentShapes, currentShapes[currentShapes.length - 1]);
        currentShapes.pop(); //console.log('[After Popup]', currentShapes.length, currentShapes);
        this.setState({ ...this.state, shapes: currentShapes, drawPoints: [] }, this.refreshMainBoardDraw);
    }

    deleteAllShapes = () => {
        this.setState({ ...this.state, shapes: [], drawPoints: [] }, this.refreshMainBoardDraw);
    }

    // double click board
    handleBoardDoubleClick = (e) => {
        console.log('double clicked');
        this.setState({ ...this.state, drawPoints: [] });
    }

    refreshMainBoardDraw = () => {
        // console.log('[Refresh Board]');
        const ctx = this.mainCanvasRef.current.getContext('2d');
        this.mainCanvasRef.current.width = this.mainCanvasRef.current.getBoundingClientRect().width;
        this.mainCanvasRef.current.height = this.mainCanvasRef.current.getBoundingClientRect().height;

        ctx.drawImage(this.bgImage, 0, 0, this.bgImage.width, this.bgImage.height, 0, 0, this.mainCanvasRef.current.width, this.mainCanvasRef.current.height);

        ctx.strokeStyle = "#fff";
        ctx.lineWidth = 1;

        // draw determined shapes
        for (let shape of this.state.shapes) {
            if (shape.mode === DRAW_TOOL_LINE) {
                this.drawLine(ctx, shape.points);
            } else if (shape.mode === DRAW_TOOL_CURVE) {
                this.drawParabola(ctx, shape.points);
            }
        }

        // draw temp shape
        ctx.strokeStyle = "#eee";
        ctx.setLineDash([3, 3]);
        if (this.state.drawPoints.length > 1) {
            if (this.state.drawTool === DRAW_TOOL_LINE) {
                this.drawLine(ctx, this.state.drawPoints);
            } else if (this.state.drawTool === DRAW_TOOL_CURVE) {
                this.drawParabola(ctx, this.state.drawPoints);
            }
        }
    }



    render() {
        return (
            <div className={styles.container43}>
                <div className={styles.buttonContainer}>
                    <button className={`${styles.button} ${this.state.drawTool === DRAW_TOOL_LINE ? styles.buttonFocused : ''}`}
                        onClick={this.toggleLineTool}>
                        <img src="images/btn_line.png" alt="Draw Line" />
                    </button>
                    <button className={`${styles.button} ${this.state.drawTool === DRAW_TOOL_CURVE ? styles.buttonFocused : ''}`}
                        onClick={this.toggleCurveTool}>
                        <img src="images/btn_curve.png" alt="Draw Line" />
                    </button>
                    <button className={`${styles.button}`}
                        onClick={this.operationBack.bind(this)}>
                        <img src="images/btn_back.png" alt="Draw Line" />
                    </button>
                    <button className={`${styles.button}`}
                        onClick={this.deleteAllShapes.bind(this)}>
                        <img src="images/btn_close.png" alt="Draw Line" />
                    </button>
                </div>

                <div className={styles.boardContainer}>
                    <canvas className={styles.mainCanvas} ref={this.mainCanvasRef}
                        onMouseDown={this.handleBoardMouseDown}
                        onMouseMove={this.handleBoardMouseMove}
                        onMouseUp={this.handleBoardMouseUp}
                        onContextMenu={this.contextMenu}
                        onDoubleClick={this.handleBoardDoubleClick}
                    ></canvas>
                    <img className="hidden" src={bgImage} ref={this.bgImageRef} alt="Background" />
                </div>

                <div className={styles.hRulerCon}>
                    <div style={{ paddingLeft: 5, paddingRight: 5 }}>
                        <Slider
                            min={0}
                            max={30}
                            step={0.01}
                            value={this.props.hScroll}
                            // orientation="vertical"
                            onChange={value => this.setHRangeValue(value)}
                        />
                    </div>
                    <canvas className={styles.hRulerCanvas} ref={this.hRulerCanvasRef}></canvas>
                    <p className="text-center text-sm font-bold">Distance (note scaled to image)</p>
                </div>

                <div className={styles.vRulerCon}>
                    <div className="relative w-full h-full">
                        <div className={styles.vRangeWrapper}>
                            <Slider
                                min={0}
                                max={20}
                                step={0.01}
                                value={this.props.vScroll}
                                orientation="vertical"
                                onChange={value => this.setVRangeValue(value)}
                            />
                        </div>
                        <canvas className={styles.vRulerCanvas} ref={this.vRulerCanvasRef}></canvas>
                        <p className={styles.verticalText}>Distance (note scaled to image)</p>
                    </div>
                </div>
            </div>
        );
    }

    underDrawTempLine = () => {
        return this.state.drawTool === DRAW_TOOL_LINE &&
            (
                this.state.drawPoints.length === 1 ||           // has only 1 point
                (this.state.drawPoints.length === 2 && this.state.drawPoints[1].click === false)
            ); // has two points, but second one is due to mouse move
    }

    underDrawTempCurve = (mode, n) => {
        return this.state.drawTool === mode &&
            (
                (this.state.drawPoints.length < n && this.state.drawPoints.length > 0) ||
                (this.state.drawPoints.length === n && this.state.drawPoints[n - 1].click === false)
            );
    }

    drawLine = (ctx, coords) => {
        // console.log(coords);
        ctx.moveTo(coords[0].x, coords[0].y);
        ctx.lineTo(coords[1].x, coords[1].y);
        ctx.stroke();
    }

    drawParabola = (ctx, coords) => {
        coords.sort(function (a, b) { return (a.x > b.x) ? 1 : ((b.x > a.x) ? -1 : 0); });

        if (coords.length === 2) {
            ctx.moveTo(coords[0].x, coords[0].y);
            ctx.lineTo(coords[1].x, coords[1].y);
            ctx.stroke();
        } else if (coords.length === 3) {

            const parabola = new Parabola(coords[0], coords[1], coords[2]);
            const delta = 3;
            ctx.moveTo(coords[0].x, parabola.getVal(coords[0].x));
            for (let x = coords[0].x + delta; x < coords[2].x; x += delta) { 
                ctx.lineTo(x, parabola.getVal(x).toFixed(0));
            }
            ctx.stroke();
        }
    }

    // Window Resize Listeners
    updateDimentions = () => {
        this.setState({ ...this.state, winWidth: window.innerWidth, winHeight: window.innerHeight });
        this.drawHRulerDrawingScale();
        this.drawVRulerDrawingScale();
    }
}

export default GamePanel;