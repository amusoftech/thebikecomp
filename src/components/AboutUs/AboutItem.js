import React from 'react';
import styles from './style.module.scss';

const AboutItem = ({ description, style, title }) => {
    return (
        <div className={styles.box}>
            <div className={styles.boxWrapper} style={style}>
                <h4 className={`text-center font-semibold uppercase text-white mb-2 ${styles.boxTitle}`}>{title}</h4>
                <p className="text-white text-center">{description}</p>
            </div>
        </div>
    );
}

export default AboutItem;