import React from 'react';
import AboutItem from './AboutItem';
import styles from './style.module.scss';
import Question from './../../pages/Question'

const AboutUs = () => {
    return (
        <section className={styles.body}>
            <div className={`px-3 mx-auto max-w-6xl`} style={{ fontSize: 14 }}>
                <h3 className="py-8 text-center font-bold uppercase" style={{ color: '#445D6C' }}>About the bike comp</h3>
                <p className="mb-10" style={{textAlign: 'justify'}}>
                    The Bike Comp is an online bicycle competition based in England, with entrants allowed from over XX countries.The era of sustainability and renewable energy is truly upon us and with it comes The Bike Comp.<br /><br />

                    The Bike Comp is led by its Co-Founders, Thomas Van Bel and Morne Vancayseele, who formed the fast growing company in 2020, with the aim of allowing people to use their skills to win High-End Quality Bicycles and E-Bikes, increasing the amount of carbon neutral transport in our communities.
                </p>

                <div className={`flex flex-wrap mt-20 ${styles.bottomPadding}`}>
                    <AboutItem
                        title="quality"
                        description="We only list High-End Bicycles and E-Bike, although all bikes are great we want our customers to win the best"
                        style={{ backgroundColor: '#212326' }}
                    />
                    <AboutItem
                        title="sustainability"
                        description="The Bike Comp actively promotes Sustainability, Renewable Energy and Healthy Living"
                        style={{ backgroundColor: '#5B775C' }}
                    />
                    <AboutItem
                        title="Charity"
                        description="A portion of every entry will be donated to charity. We believe strongly in giving back to the community, especially the ones who need it the most"
                        style={{ backgroundColor: '#7B4B4B' }}
                    />
                </div>
            </div>
           
        </section>

    );
}

export default AboutUs;