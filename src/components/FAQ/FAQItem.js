import React from 'react';
import { DownArrowIcon } from '../Icons';
import styles from './style.module.scss';

const FAQItem = ({title, description, isOpen, onClick}) => {
    return (
        <section className="mb-4 cursor-pointer">
            <div className={styles.head} onClick={onClick}>
                <DownArrowIcon className={isOpen ? styles.iconOpen : styles.icon} /><span className="title">{title}</span>
            </div>
            <div className={isOpen ? styles.bodyOpen : styles.body}>
                <h5>{description}</h5>
            </div>
        </section>
    );
}

export default FAQItem;