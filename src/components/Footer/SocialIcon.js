import React from 'react';
import styles from './style.module.scss';

const SocialIcon = ({ image, link }) => {
    return (
        <div className="mx-2"><a href={link}><img className={styles.socialIcon} src={image} alt="Social Icon of TheBikeComp" /></a></div>
    );
}

export default SocialIcon;