import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { MXSection } from "../../containers";
import SocialIcon from "./SocialIcon";
import { useForm } from "react-hook-form";
import styles from "./style.module.scss";
import { subscribe } from "./../../redux/actions";
import { connect } from "react-redux";
import { toastr } from 'react-redux-toastr'

const socialIcons = [
  {
    image: "/images/social/facebook.png",
    link: "https://www.facebook.com/thebikecompofficial",
  },
  {
    image: "/images/social/instagram.png",
    link: "https://instagram.com/thebikecomp",
  },
];
const Footer = ({ subscribe$ }) => {
  const { handleSubmit, errors, register, trigger } = useForm();
  const [userEmail, setUserEmail] = useState()
  const handleOnChange = (e) => {
    console.log("name", e.target.value);
    setUserEmail(e.target.value)

  };
  const onSubmit = async (data) => {
    console.log("==>>", userEmail);
    if (userEmail) {
      subscribe$(userEmail);
      toastr.success("Done", "You have successfully Subscribed to us.")
      setUserEmail('')
    } else {
      toastr.error("Error", "Invalid email.")
    }

  };
  const triggerAllFields = () => {
    const names = ["email"];
    for (let name of names) {
      trigger(name);
    }
  };

  const cookies = document.cookie;
  const history = useHistory();
  const search = window.location.search;
  const params = new URLSearchParams(search);
  const redirect = params.get('redirect');

  /*   if (cookies) {
        history.push("/cookie-policy");
    } else {
        history.push("/");
    }
   */

  return (
    <MXSection className="bg-bg text-gray-100 pt-16 pb-10 text-center sm:text-left">
      <div className="flex flex-wrap">
        <div className="w-full lg:w-1/2 flex flex-wrap">
          <div className="w-full sm:w-1/3">
            <Link to="/contact-us">
              <span className={styles.firstMenu}>CONTACT US</span>
            </Link>
            <Link to="/contact-us">
              <span className={styles.menuItem}>info@thebikecomp.com</span>
            </Link>
          </div>
          <div className="w-full sm:w-1/3 mt-6 sm:mt-0">
            <Link to="/contact-us">
              <span className={styles.firstMenu}>customer service</span>
            </Link>
            <Link to="/">
              <span className={styles.menuItem}>Ordering & Payment</span>
            </Link>
            <Link to="/">
              <span className={styles.menuItem}>Shipping</span>
            </Link>
            <Link to="/faq">
              <span className={styles.menuItem}>FAQ</span>
            </Link>
          </div>
          <div className="w-full sm:w-1/3 mt-6 sm:mt-0">
            <Link to="/contact-us">
              <span className={styles.firstMenu}>information</span>
            </Link>
            <Link to="/about-us">
              <span className={styles.menuItem}>About The Bike Comp</span>
            </Link>
            <Link to="/contact-us">
              <span className={styles.menuItem}>Work With Us</span>
            </Link>
            <Link to="/privacy">
              <span className={styles.menuItem}>Privacy Policy</span>
            </Link>
            <Link to="/terms-and-conditions">
              <span className={styles.menuItem}>Terms & Conditions</span>
            </Link>
          </div>
        </div>

        <div className="w-full lg:w-1/2 flex flex-wrap mt-6 lg:mt-0">
          <div className="w-full md:w-2/3">
            <span className={styles.subscribeHead}>Subscribe</span>
            <p className="text-fg-blue1 mb-4" style={{ fontSize: 14 }}>
              Sign up for the latest updates from The Bike Comp
            </p>
            <div className="flex flex-wrap sm:flex-no-wrap justify-center sm:justify-start">
              <form
                className="w-full"
                onSubmit={handleSubmit(onSubmit)}
              >
                <input
                  value={userEmail}
                  type="text"
                  onChange={handleOnChange}
                  className="text-gray-100 placeholder-gray-500 py-2 px-4 rounded-md bg-transparent
                                focus:outline-none border border-fg-blue1 mr-2 mb-2"
                  style={{ fontSize: 14 }}
                  placeholder="Email Address"
                  name="email"
                />
                <button
                  type="submit"
                  className="bg-white hover:bg-gray-200 text-fg-blue2 font-bold py-2 px-4 rounded mb-2"
                  style={{ fontSize: 10, height: 38, padding: '4px 31px 1px 30px' }}
                >
                  SUBSCRIBE
                </button>
              </form>
            </div>
          </div>
          <div className="w-full md:w-1/3 flex items-end">
            <label
              className="w-full text-center sm:text-right mt-5 sm:mt-0"
              style={{ fontSize: 10 }}
            >
              @{new Date().getFullYear()} The Bike Comp
            </label>
          </div>
        </div>
      </div>
      <div className="flex justify-center">
        {socialIcons &&
          socialIcons.map((icon, i) => <SocialIcon {...icon} key={i} />)}
      </div>
    </MXSection>
  );
};
const mapStateToProps = (state) => ({
  ...state,
});

const mapDispatchToProps = {
  subscribe$: subscribe,
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
//export default Footer;
