import React from 'react';

import { Footer, Header } from '../components';
// import { BodyContainer } from '../containers';
import { FullBodyContainer } from './BodyContainer';

const AboutLayout = ({ children }) => {

    return (
        <>
            <Header />
            <FullBodyContainer>
                {children}
            </FullBodyContainer>
            <Footer />
        </>
    );
}

export default AboutLayout;