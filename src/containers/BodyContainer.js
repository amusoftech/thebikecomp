import React from 'react';

import styles from './BodyContainer.module.scss';

const BodyContainer = ({children}) => {

    return (
        <section className={styles.container}>
            {children}
        </section>
    );
}


export const FullBodyContainer = ({ children }) => {
    return (
        <section className={styles.fullContainer}>
            {children}
        </section>
    );
}

export default BodyContainer;