import React  from 'react'; 

import { Footer, Header } from '../components';
import { BodyContainer } from '../containers';
 
const MainLayout = ({ children }) => { 

    return (
        <>
            <Header  />
            <BodyContainer> 
                {children}
            </BodyContainer>
            <Footer />
        </>
    );
}

export default MainLayout; 