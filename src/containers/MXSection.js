import React from 'react';
// 1280, 1366, 1440, 1680
import styles from './MXSection.module.scss';

const MXSection = ({ children, className }) => {

    return (
        <section className={`px-0 lg:px-0 ${className}`} style={{paddingTop: 30, paddingBottom: 30}}>
            <div className={styles.container} style={{ maxWidth: 1920 }}>
                {children}
            </div>
        </section>
    );
}

export default MXSection;