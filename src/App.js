import React, { useEffect, useLayoutEffect, useState, Suspense } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import SideBar from 'react-sidebar';
import { connect } from 'react-redux';
// eslint-disable-next-line
import ReduxToastr, { toastr } from 'react-redux-toastr';
// eslint-disable-next-line
import MyProfile from './pages/MyProfile';
import EmailPreferance from './components/User/EmailPreferance';
import HelpPage from './components/User/HelpPage';
import MyDetails from './components/User/MyDetails';
import MyCompition from './components/User/MyCompition'
import { AboutUsPage, BikesPage, CheckoutPage, CookiePrivacyPage, ComingSoonPage, ContactUsPage, FAQPage, GamePage, HomePage, LoginPage, PrivacyPage, SignUpPage, SuccessPage, TOCPage } from './pages';
import Question from './pages/Question';
import { SideMenu } from './components';
import { getBikeBrands, getBikesInfo, getBikeTypes, getSession, toggleMenu, getUserFromAsync } from './redux/actions';
import ResetPassword from './pages/ResetPassword';
import UpdateProfile from './pages/UpdateProfile';
import ForgotPassword from './pages/ForgotPassword';
import Message from './pages/Message'
import SuccessMessage from './pages/SuccessMessage'
import ProfileUpdate from './pages/ProfileUpdate';
import { AuthProvider } from './utils/Auth';
import PrivateRoute from './utils/PrivateRoute';
import privateRoutes from './utils/privateRoutes'
import Welcome from './pages/Welcome';
import NewPassword from './pages/NewPassword';

// import { encryptString } from './utils/common.utils';


function useWindowSize() {
  const [size, setSize] = useState([0, 0]);
  useLayoutEffect(() => {
    function updateSize() {
      setSize([window.innerWidth, window.innerHeight]);
    }
    window.addEventListener('resize', updateSize);
    updateSize();
    return () => window.removeEventListener('resize', updateSize);
  }, []);
  return size;
}

const App = ({ appLoaded, menuLayer, menuOpen, getBikeBrands$, getBikesInfo$, getBikeTypes$, getSession$, toggleMenu$ }) => {
  // eslint-disable-next-line
  const [width, height] = useWindowSize();


  // one time loading
  useEffect(() => {
    loadInitialData();

    return () => {

    }
  }, [appLoaded]);

  const onSetSidebarOpen = (open) => {
    // console.log(open);
    toggleMenu$(open);
  }

  const loadInitialData = () => {


    getBikeBrands$();
    getBikeTypes$();
    getBikesInfo$();
    getSession$();

  }
  const getStyle = (flag) => {
    return !flag ? { display: 'none' } : {}
  }
  return (
    <AuthProvider>
      <Router>
        <Suspense fallback={<p>Loading…</p>}></Suspense>
        {(width < 1092) && <SideBar
          sidebar={<SideMenu />}
          open={!!menuOpen && width < 1092}
          onSetOpen={onSetSidebarOpen}
          touch={true}
          styles={{
            sidebar: { background: "white", minWidth: 300, position: 'fixed' },
            root: getStyle(menuOpen),
          }}
        >
          <button onClick={() => onSetSidebarOpen(true)}></button>
        </SideBar>
        }
        <ReduxToastr
          timeOut={4000}
          newestOnTop={true}
          preventDuplicates
          position="top-right"
          getState={(state) => state.toastr} // This is the default
          transitionIn="fadeIn"
          transitionOut="fadeOut"
          progressBar={false}
          closeOnToastrClick />
        <Switch>
          <Route path="/about-us"><AboutUsPage /></Route>
          <Route path='/bikes'><BikesPage /></Route>
          {/* <Route path='/checkout'><CheckoutPage/></Route> */}
          <Route path='/contact-us'><ContactUsPage /></Route>
          <Route path='/cookie-policy'><CookiePrivacyPage /></Route>
          <Route path="/faq"><FAQPage /></Route>
          <Route path="/home"><HomePage /></Route>
          {/*  <Route path="/question"><Question /></Route> */}
          <Route path="/login"><LoginPage /></Route>
          <Route path="/play"><GamePage /></Route>
          <Route path="/privacy"><PrivacyPage /></Route>
          <Route path="/signup"><SignUpPage /></Route>
          <Route path="/thank-you"><SuccessPage /></Route>
          <Route path="/terms-and-conditions"><TOCPage /></Route>
          <Route path="/welcome"><Welcome /></Route>

          {privateRoutes.map((item, key) => {
            return <PrivateRoute key={key} path={item.url} component={item.component} />
          })
          }



          <Route path="/my-profile/:at?"><MyProfile /></Route>  
          <Route path="/my-compition"><MyCompition /></Route>
          <Route path="/email-preferance"><EmailPreferance /></Route>
          <Route path="/help"><HelpPage /></Route>
          <Route path="/my-details"><MyDetails /></Route>
          <Route path="/reset-password"><ResetPassword /></Route>
          <Route path="/new-password/:token?"><NewPassword /></Route>
          <Route path="/update-profile"><UpdateProfile /></Route>
          <Route path="/forgot-password"><ForgotPassword /></Route>
          <Route path="/message/:email?" component={Message} />
          <Route path="/profile-updated"><ProfileUpdate /></Route>
          <Route path="/success"><SuccessMessage /></Route>
          <Route path="/"><HomePage /></Route>

        </Switch>
      </Router>
    </AuthProvider>
  );
}

const mapStateToProps = state => ({
  menuLayer: state.setting.menuLayer,
  menuOpen: state.setting.menuOpen,
  appLoaded: state.setting.appLoaded,
  // setting: state.setting,
});

const mapDispatchToProps = {
  toggleMenu$: toggleMenu,
  getSession$: getSession,
  getBikeBrands$: getBikeBrands,
  getBikesInfo$: getBikesInfo,
  getBikeTypes$: getBikeTypes,
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
