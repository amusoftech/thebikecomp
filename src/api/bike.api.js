import { API_BASE_URL } from '../utils/common.utils';
import axios from './index';




export const getBikeBrandsRequest = async (params = null) => { 
    try {
        const { data: res, status } = await axios.get(`${API_BASE_URL}/get_bike_brands`,{params:params});
        if (status === 200) {
            return res.data;
        } else {
            // process error
            return { message: res.message };
        }
    } catch (e) {
        return { message: e.message }
    }
    // return new Promise((resolve, reject) => {
    //     axios.get(`/get_bike_brands`)
    //         .then(({data: res, status}) => {
    //             if (status === 200) {
    //                 resolve(res.data);
    //             } else {
    //                 // process error
    //                 reject({message: res.message});
    //             }
    //         })
    //         .catch(err => {
    //             console.error('[Get Bike Brands] error', err);
    //             reject({message: 'Unknown error!'});
    //         })
    // });
}

export const getBikeTypesRequest = async (params) => {
    try {
        const { data: res, status } = await axios.get(API_BASE_URL + '/get_bike_types',{params:params});
        if (status === 200) {
            return res.data;
        } else {
            // process error
            return { message: res.message };
        }
    } catch (e) {
        return { message: e.message }
    }
}

export const getBikesInfoRequest = async (params = null) => {
    try {
      
        const url =params  !=null ?  `${API_BASE_URL}/get_bikes${params}` : `${API_BASE_URL}/get_bikes`
  
        const { data: res, status } = await axios.get(url);

        if (status === 200) {
            return res.data;
        } else {
            // process error
            return { message: res.message };
        }
    } catch (e) {
        return { message: e.message }
    }
}