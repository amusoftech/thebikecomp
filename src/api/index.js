import axios from 'axios'
import { getAuthToken } from '../utils/common.utils';

const instance = axios.create({
  baseURL: process.env.REACT_APP_API_ENDPOINT,
  headers: {
    'Authorization': {
      toString() {
        return `Bearer ${getAuthToken()}`
      }
    },
    // 'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'X-Requested-With': 'TheBikeComp',
    'Custom-Header': 'Hey'
  }
});

// instance.defaults.headers.common['Authorization'] = `Bearer ${getAuthToken()}`;
// instance.defaults.headers.post['Content-Type'] = 'application/json';
export default instance;