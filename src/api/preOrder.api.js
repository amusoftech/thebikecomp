import { API_BASE_URL } from '../utils/common.utils';
import axios from './index';


export const getPreOrders = async (params) => {
   try {
     
       let url = params !=null ? `${API_BASE_URL}/get_pre_orders${params}` : API_BASE_URL + '/get_pre_orders'
        const { data: res, status } = await axios.get(url);
       if (status === 200) {
           return res.data;
       } else {
           // process error
           return { message: res.message };
       }
   } catch (e) {
       return { message: e.message }
   }
}
export const getPreviousWinnderResult = async () => {
    
    try {
        const { data: res, status } = await axios.get(API_BASE_URL + '/get_previous_winner');
        if (status === 200) {
            return res.data;
        } else {
            // process error
            return { message: res.message };
        }
    } catch (e) {
        return { message: e.message }
    }
 }



export const getQuestion = async () => {
    try {
        const { data: res, status } = await axios.get(API_BASE_URL + '/get_question');
        if (status === 200) {
            return res.data;
        } else {
            // process error
            return { message: res.message };
        }
    } catch (e) {
        return { message: e.message }
    }
 }


 export const getQuestionOne = async () => {
    try {
        const { data: res, status } = await axios.get(API_BASE_URL + '/get_one_question');
        if (status === 200) {
            return res.data;
        } else {
            return { message: res.message };
        }
    } catch (e) {
        return { message: e.message }
    }
 }



 export const getFaq = async () => {
    try {
        const { data: res, status } = await axios.get(API_BASE_URL + '/get_all_faq');
        if (status === 200) {
            return res.data;
        } else {
            // process error
            return { message: res.message };
        }
    } catch (e) {
        return { message: e.message }
    }
 } 
 
 
 export const getSiteCMSDetails = async () => {
    try {
        const { data: res, status } = await axios.get(API_BASE_URL + '/get_site_CMS_details');
        if (status === 200) {  
            return res.data;
        } else {
            // process error
            return { message: res.message };
        }
    } catch (e) {
        return { message: e.message }
    }
 }