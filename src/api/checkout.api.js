import { API_BASE_URL } from '../utils/common.utils';
import axios from './index';

export const stripeCheckoutRequest = async (param) => {
    try {
        const { data: res, status } = await axios.post(API_BASE_URL + '/checkout/stripe', param);
        
        if (status === 200) {
            return res;
        } else {
            // process error
            return { message: res.message };
        }
    } catch (e) {
        return { message: e.message }
    }
}

export const confirmCheckoutRequest = async (token) => {
    try {
        const {data: res, status} = await axios.post(API_BASE_URL + '/checkout/confirm', {token: token});
        if (status === 200) {
            return res;
        } else {
            return {message: 'Unkown Error!'};
        }
    } catch (e) {
        return {message: e.message};
    }
}

export const paypalCheckoutRequest = async (param) => {
   
    try {
      //  const { data: res, status } = await axios.post('/checkout/paypal', param);
      const { data: res, status } = await axios.post(API_BASE_URL + '/checkout/paypal', param);
     
       
        if (status === 200) {
            return res;
        } else {
            // process error
            return { message: res.message };
        }
    } catch (e) {
        return { message: e.message }
    }
}
