import { API_BASE_URL } from '../utils/common.utils';
import axios from './index';


export const getSessionRequset = async token => {
    try {
        const { data: res } = await axios.post(`${API_BASE_URL}/session`, { token: token });
        return res;
    } catch (e) {
        return { status: false, message: e.message };
    }
}

export const loginRequest = async data => {
    try {
        const { data: res } = await axios.post(`${API_BASE_URL}/login`, data);
        return res;
    } catch (e) {
        return { status: false, message: e.message };
    }
}

export const contactRequest = async data => {
    try {
        const { data: res } = await axios.post(`${API_BASE_URL}/contact_us`, data);
        return res;
    } catch (e) {
        return { status: false, message: e.message };
    }
}

export const registerRequest = async data => {
    try {
        const { data: res } = await axios.post(`${API_BASE_URL}/register`, data);
        return res;
    } catch (e) {
        return { status: false, message: e.message };
    }
}

export const socialRegisterRequest = async data => {
    try {
        const { data: res } = await axios.post(`${API_BASE_URL}/socialLogin`, data);
        return res;
    } catch (e) {
        return { status: false, message: e.message };
    }
}

export const getUserDetails = async (params = null) => {

    try {
        const { data: res, status } = await axios.get(`${API_BASE_URL}/user_details`);
        if (status === 200) {
            return res.data;
        } else {
            // process error
            return { message: res.message };
        }
    } catch (e) {
        return { message: e.message }
    }
}

export const fetchUserByTokenApi = async data => {
    try {

        const { data: res } = await axios.get(`${API_BASE_URL}/find_user?token=${data.token.replace(/ /g,'')}`);
        return res;
    } catch (e) {
        return { message: e.message };
    }
}


export const forgotPassRequest = async data => {
    try {
        const { data: res } = await axios.get(`${API_BASE_URL}/forget_password?email=${data.email}`);
        return res;
    } catch (e) {
        return { message: e.message };
    }
}
export const subscribeRequest = async data => {
    try {
        const { data: res } = await axios.get(`${API_BASE_URL}/subscribe_user?email=${data}`);
        return res;
    } catch (e) {
        return { message: e.message };
    }
}

export const changePassRequest = async data => {

    try {
        const { data: res } = await axios.get(`${API_BASE_URL}/change_password?pwd=${data.password}&token=${data.token}&id=${data.user_id}`);
        return res;
    } catch (e) {
        return { message: e.message };
    }
}
export const updateProfileRequest = async data => {
    try {
        //const { data: res } = await axios.post(`${API_BASE_URL}/register`, data);
        const { data: res } = await axios.post(`${API_BASE_URL}/update_profile`, data);
        return res;
    } catch (e) {
        return { message: e.message };
    }
}


export const toggleUserEmailPref = async data => {
    try {
        //const { data: res } = await axios.post(`${API_BASE_URL}/register`, data);
        const { data: res } = await axios.post(`${API_BASE_URL}/update_email_preferances`, data);
        return res;
    } catch (e) {
        return { message: e.message };
    }
}

