export const TOKEN_NAME = 'thebikecomp.token';
export const CART_NAME = 'thebikecomp.cart';


export const DRAW_TOOL_LINE = 'draw.line';
export const DRAW_TOOL_CURVE = 'draw.curve';


export const LOGGED_USER = 'logged_user';

export const GET_FAQ ="get_faq";