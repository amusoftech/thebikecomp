import React from 'react';

const dummyText = "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,";

export const FAQs = [
    { title: 'Where is The Bike Comp based?', description: <span>The Bike Comp is based in (add address)</span> },
    { title: 'Who are we?', description: <span>The Bike Comp is an online bicycle competition Co-Founded by Thomas Van Bel and Morne Vancayseele in 2020. </span> },
    {
        title: 'How do I enter the competition?', description: <div>
            To enter any of The Bike Comp is really easy, simply follow the steps below:<br /><br />
            <ul className="list-none">
                <li>1. Select your tickets.</li>
                <li>2. Proceed to Play from your Basket.</li>
                <li>3. Create an Account or Log In if you are a returning entrant.</li>
                <li>4. Play the game. You can watch a video of how the game works #here or read our terms and conditions #here</li>
                <li>5. Confirm your entries and proceed to the checkout</li>
                <li>6. Payment is easy, simply select your payment method,fill in your payment details and check the box confirming you have read our #Terms and Conditions. For a list of our payment methods click #here.</li>
                <li>7. Once your payment is complete your entry will be registered and we will send you an email containing your invoice for your entries and the values you have entered.</li>
                <li>8. Wait for the competition to close and watch our Judges make their decisions.</li>
            </ul>
        </div>
    },
    {
        title: 'How is the winner decided?', description:
            <div>
                When the competition closes three judges watch the same video shown to entrants with the presence of an independent lawyer. They will use their PROFESSIONAL skills to judge the Speed and Angle/Height of the cyclist. <br /><br />
            Once all three judges have finalized their answers the average of their answers will be the winning result<br /><br />
            Example using Speed and Angle:<br />
                <div className="pl-6">
                    <b>Judge 1</b>: Speed 23.20 mph, Angle 75.50°<br />
                    <b>Judge 2</b>: Speed 25.50 mph, Angle 78.20°<br />
                    <b>Judge 3</b>: Speed 21.85 mph, Angle 80.55°<br />
                </div>
                Winning Speed: (23.20 + 25.50 + 21.85) / 3 = 23.52 mph  (rounded to two decimal places)<br />
                Winning Angle: (75.50 + 78.20 + 80.55) / 3 = 78.03°         (rounded to two decimal places)<br /><br />
                Example using Speed and height:<br />
                <div className="pl-6">
                    <b>Judge 1</b>: Speed 23.20 mph, Height 176.5cm<br/>
                    <b>Judge 2</b>: Speed 25.50 mph, Height 150.2cm<br/>
                    <b>Judge 3</b>: Speed 21.85 mph, Height 184.5cm<br/>
                </div>
                <br />
                Winning Speed: (23.20 + 25.50 + 21.85) / 3 = 23.52 mph  (rounded to two decimal places)<br />
                Winning Height: (176.5 + 150.2 + 184.5) / 3 = 170.4cm    (rounded to two decimal places)
            </div>
    },
    { title: 'How will the winner be notified?', description: <span>{dummyText}</span> },
    { title: 'What’s my ticket number?', description: <span>There are no ticket numbers in The Bike Comp. Please see “How does it work”.</span> },
    { title: 'How many tickets can I buy?', description: <span>There is a limit of 150 tickets per customer, per competition.</span> },
    { title: 'Can I win if I don’t live in the UK?', description: <span>Yes! The good news is you don’t have to live in the UK to win. However, we suggest you review your country/state laws regarding online skills competitions before you enter.</span> },
    { title: 'How often do you update the bikes available on The Bike Comp?', description: <span>Specifications, reviews, and availability of The Bike Comp bikes are correct as of June 2020. The bike specifications and RRP are checked on a monthly basis. We endeavour to ensure all our information on bikes to be correct but we can not be held responsible for inaccuracies due to manufacturers adjusting details without notice.</span> },
    { title: "What happens if more than one person is equally close to the judge's answer?", description: <span>There can only be one winner in each of the monthly The Bike Comp competitions. If there are two or more players equally close to the judges position on speed and angle, the winner will be determined by taking the next closest entry of the players in question. The person with the closest second ticket will then be the winner. If any player in this scenario only has one ticket, then all players that qualify will play another “speed and angle” competition to determine the winner.</span> },
    {
        title: "Why don’t you use the actual speed and angle of the cyclist in the video?", description:
            <span>In Short, The Bike Comp is a skills competition. We therefore have to present a competition game whereby it’s the skill of an entrant versus the skill of professionals - not versus the chance that the original speed and angle on the video is picked. Legally we are bound by this definition.<br /><br />
            The panel of judges is formed of experienced ex or present professionals, mainly professional cyclists, who each offer their best opinion.<br /><br />
            Using more than one judge and an aggregate calculation ensures that nobody can ever know the original speed and angle or height of the cyclists - resulting in a process that is fair, transparent, audited and honest.
        </span>
    },
    {
        title: 'How is the winner judged?', description:
            <span>
                The winner will be selected based on whoever chose the closest to the speed(primary) and then the closest to the angle or height (secondary) selected by the judges calculated averages.<br/><br/>
                Example of win/lose situation:<br/><br/>
                <b>Judges Final Answer</b>: 23.52 mph(speed) 78.05°(angle)<br/><br/>
                <b>Entrant 1</b>: Speed 23.52mph   Angle 78.00°<br/>
                <b>Entrant 2</b>: Speed 23.12mph   Angle 78.05°<br/>
                <b>Entrant 3</b>: Speed 23.51mph   Angle 78.10°<br/>
                <span className="bg-green-500"><b>Entrant 4</b>: Speed 23.52mph   Angle 78.07° WINNER</span><br/>
                <b>Entrant 5</b>: Speed 22.5mph   Angle 78.00°<br/><br/>
                <b>Entrant 4</b> will be announced as winner.<br/>
                <b>Entrant 1</b> and <b>4</b> both chose the correct/closest speed but <b>Entrant 4</b> was closer to the correct angle.<br/>
                <b>Entrant 2</b> chose the correct angle but his Speed(primary) was not close/correct.<br/><br/>
                It is important to note that we guarantee a bike every month.
            </span>
    },
    { title: 'Who buys the bike?', description: <span>The Bike Comp pays for the bike and shipping costs. There are no further costs to the entrant(s).</span> },
    { title: 'How is the bike delivered?', description: <span>The Bike Comp will deliver the bike in person(s) where possible. If this is not possible, The Bike Comp will arrange delivery. There will be no delivery costs to the winner.</span> },
    { title: 'Can I choose the set-up of my bike?', description: 
        <span>
            Where bike value package tickets are selected the entrant will be able to set-up their new bike up to the value of their selected amount from any retailer they choose. <br/><br/>
            Any value packages can not be redeemed for cash and must be used in full.
        </span> },
    { title: 'Will I get a receipt for my purchase?', description: <span>You will receive an email containing a receipt of your entries upon payment confirmation.</span> },
    { title: "How do I contact The Bike Comp?", description: <span>The Bike Comp can be contacted through email via info@thebikecomp.com.</span> }
];