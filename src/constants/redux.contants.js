
export const AUTH_LOGIN         = 'auth:login';
export const AUTH_LOGOUT        = 'auth:logout';
export const AUTH_REGISTER      = 'auth.register';
export const SET_AUTH_USER      = 'set.auth.user';
export const FORGOT_PASSWORD    ='auth:forgot';
export const CHANGE_PASSWORD    ="auth:change";
export const UPDATE_PROFILE     ="auth:update"
export const SUBSCRIBE_USER     ="auth:update"
export const SOCIAL_REGISTER    ="auth:update"


export const  FETCH_USER_BY_TOKEN    ="auth:fetch_user_by_token"

export const TOGGLE_EMAIL_PREF = 'user:email:pref';

export const CART_TOGGLE    = 'cart:toggle';
export const CART_HOVER     = 'cart:hover';
export const MENU_TOGGLE    = 'menu:toggle';
export const MENU_LAYER     = 'menu:layer';
export const STICK_HEADER   = 'header:stick';

////////////////////////////////////////////////////////////////
//                                                            //
//               B I K E    C O N S T A N T                   //
//                                                            //
////////////////////////////////////////////////////////////////

export const BIKE_GET_BIKES     = 'bike.get-bikes';
export const BIKE_GET_BRANDS    = 'bike.get-brands';
export const BIKE_GET_TYPES     = 'bike.get-types';


////////////////////////////////////////////////////////////////
//                                                            //
//               B I K E    C O N S T A N T                   //
//                                                            //
////////////////////////////////////////////////////////////////

export const CART_ADD           = 'cart.add';
export const CART_DELETE        = 'cart.delete';
export const CART_DELETE_ALL    = 'cart.delete-all';
export const CART_SET           = 'cart.set';
export const CART_SUBMIT_TICKET = 'cart.submit-ticket';
export const SELECTED_QUESTION  = 'selected.question'

/*
* Get Orders 
 */
export const RECENT_ORDER = 'recent_order';


////////////////////////////////////////////////////////////////
//                                                            //
//               P R E    O R D E R S                         //
//                                                            //
////////////////////////////////////////////////////////////////

export const GET_PRE_ORDERS = 'GET_PRE_ORDERS';
export const GET_PREVIOUS_ORDERS = 'GET_PREVIOUS_ORDERS';




////////////////////////////////////////////////////////////////
//                                                            //
//               Questions                                    //
//                                                            //
////////////////////////////////////////////////////////////////

export const GET_QUESTION = 'GET_QUESTION';
export const GET_ONE_QUESTION = 'GET_ONE_QUESTION';


////////////////////////////////////////////////////////////////
//                                                            //
//               All FAQs                                     //
//                                                            //
////////////////////////////////////////////////////////////////

export const GET_FAQ = 'GET_FAQ';




////////////////////////////////////////////////////////////////
//                                                            //
//               All Site CMS                                 //
//                                                            //
////////////////////////////////////////////////////////////////

export const SITE_CMS = 'SITE_CMS';
export const CONTACT_US    ="CONTACT_US"
