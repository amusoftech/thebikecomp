
import CryptoJS from "crypto-js";
import { CART_NAME, TOKEN_NAME, LOGGED_USER } from '../constants/common.constants';


/* export const API_BASE_URL = "http://localhost/047-botb-admin/api/v1";
export const BASE_URL = "http://localhost/047-botb-admin/"; */


export const STRIPE_CLIENT_KEY_DEV = "pk_test_4OAjx55Hx8lZpKu9iJcNht1000fOgCpg7S";
/*   export const API_BASE_URL = "http://localhost/the-bike-compition-admin/api/v1";
 export const BASE_URL = "http://localhost/the-bike-compition-admin/"
 */
  
export const API_BASE_URL = "https://admin.thebikecomp.com/api/v1";
export const BASE_URL = "https://admin.thebikecomp.com/";  


export const PAY_PAL_KEYS_DEV = {
    payPalClientId:
        "AbK1JC9x-rj88uvKvdLx2td4SIazFZuHNe9SzqUwmH72LKfkq0q067lKFFFMT7dXVSmM1D8-iVOQSqnc",
    payPalClientSecret:
        "EHGxLBwrfmPvYsnV6ykEbJcRrDJ1SYjy0JEUs5-8RNlc8xhS1SYEQufTM1Xx9dgKLlajKm8q0Jo10nck"
};




export const getAuthToken = () => {
    const encToken = window.localStorage.getItem(TOKEN_NAME);
    return !!encToken ? decryptString(encToken) : '';
}

export const saveAuthToken = (token) => {
    if (!token) return;
    const encToken = encryptString(token);
    window.localStorage.setItem(TOKEN_NAME, encToken);
}

export const deleteAuthToken = () => {
    localStorage.removeItem(TOKEN_NAME);
}


export const encryptString = (src) => {
    return CryptoJS.AES.encrypt(src, process.env.REACT_APP_SECRET).toString();
}

export const decryptString = (src) => {
    let temp = CryptoJS.AES.decrypt(src, process.env.REACT_APP_SECRET);
    return temp.toString(CryptoJS.enc.Utf8)
}



export const saveCartInfo = cart => {
    let saveInfo = "";
    if (!!cart) {
        saveInfo = encryptString(JSON.stringify(cart));

    }
    localStorage.setItem(CART_NAME, saveInfo);
}

export const loadCartInfo = cart => {
    const saveInfo = localStorage.getItem(CART_NAME);
    if (!saveInfo) { return []; }
    else {
        try {
            return JSON.parse(decryptString(saveInfo));
        } catch (e) {
            return [];
        }
    }
}



export const getTicketPrice = (bike, count) => {
    let price = 0;
    count = Number(count);
    if (count < 5) {
        price = Number(bike.price) * count;
    } else if (count < 10) {
        //price = Number(bike.price) * count * (1 - Number(bike.price_5) / 100);
        price = Number(bike.price) * count;
    } else if (count < 20) {
        price = Number(bike.price) * count;
    } else if (count < 50) {
        price = Number(bike.price) * count;
    } else if (count >= 50) {
        price = Number(bike.price) * count;
    }

    return Number(price.toFixed(2));
}

export const getUnitTicketPrice = (bike, count) => {
    let price = 0;
    count = Number(count);
    if (count < 5) {
        price = Number(bike.price);
    } else if (count < 10) {
        price = Number(bike.price);
    } else if (count < 20) {
        price = Number(bike.price);
    } else if (count < 50) {
        price = Number(bike.price);
    } else if (count >= 50) {
        price = Number(bike.price);
    }

    return Number(price.toFixed(2));
}

export const getTicketDiscount = (bike, count) => {
    let price = 0;

    if (count < 5) {
        price = 0;
    } else if (count < 10) {
        price = Number(bike.price) * count /* * Number(bike.price_5) / 100 */;
    } else if (count < 20) {
        price = Number(bike.price) * count /* * Number(bike.price_10) / 100 */;
    } else if (count < 50) {
        price = Number(bike.price) * count /* * Number(bike.price_20) / 100 */;
    } else if (count >= 50) {
        price = Number(bike.price) * count /* * Number(bike.price_50) / 100 */;
    }

    return price.toFixed(2);
}

export const distance = (x1, y1, x2, y2) => {
    return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
}

export const checkInDistance = (coords, point, delta) => {
    for (let coord of coords) {
        if (distance(coord.x, coord.y, point.x, point.y) < delta) { return true; }
    }
    return false;
}

export const orderCoordinateByX = (coords) => {
    for (let i = 0; i < coords.length - 1; i++) {
        for (let j = i + 1; j < coords.length; j++) {

        }
    }
}

// refer: https://www.desmos.com/calculator/lac2i0bgum
export class Parabola {
    constructor(p1, p2, p3) {
        this.initializeCoe(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y);
    }

    initializeCoe(x1, y1, x2, y2, x3, y3) {
        let A1 = Math.pow(x2, 2) - Math.pow(x1, 2);
        let B1 = -x1 + x2;
        let D1 = -y1 + y2;

        let A2 = Math.pow(x3, 2) - Math.pow(x2, 2);
        let B2 = x3 - x2;
        let D2 = y3 - y2;

        let B = -(B2 / B1);
        let A3 = B * A1 + A2;
        let D3 = B * D1 + D2;

        this.a = D3 / A3;
        this.b = (D1 - A1 * this.a) / B1;
        this.c = y1 - this.a * Math.pow(x1, 2) - this.b * x1;
    }

    getVal(x) {
        let fl = this.a * Math.pow(x, 2) + this.b * x + this.c;
        return fl;
    }
}


export const getLoginUserDetails = () => {
    const user = window.localStorage.getItem(LOGGED_USER);
    return JSON.parse(user);
}

export const saveALoginUserDetails = (user) => {
    window.localStorage.setItem(LOGGED_USER, user);
}

export const deleteALoginUserDetails = () => {
    localStorage.removeItem(LOGGED_USER);

}


