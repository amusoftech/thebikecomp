import {
    BIKE_GET_BIKES, BIKE_GET_BRANDS, BIKE_GET_TYPES, CART_SET
} from '../../constants/redux.contants';

import {
    getBikesInfoRequest,
    getBikeBrandsRequest,
    getBikeTypesRequest
} from '../../api/bike.api';

import { loadCartInfo } from '../../utils/common.utils';

export const getBikesInfo = (params=null) => {
    return async dispatch => {
        try {
            const bikes = await getBikesInfoRequest(params);
           //console.log(params);
            const rawCart = loadCartInfo(); //console.log('[prev Cart]', rawCart);

            let cartContent = [];
            for (let item of rawCart) {
                const bike = getBikeFromArray(bikes, item.id);
                if (!!bike) {
                    cartContent.push({
                        bike: getBikeFromArray(bikes, item.id),
                        amount: item.amount,
                    });
                }
            }
            dispatch({ type: BIKE_GET_BIKES, payload: bikes });
            dispatch({ type: CART_SET, payload: cartContent });
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const getBikeBrands = (params =null) => {
    return async dispatch => {
        try {
            // await getBikeBrandsRequest()
            // .then(brands => {
            //     dispatch({type: BIKE_GET_BRANDS, payload: brands}); 
            // })
            // .catch(err => {
            //     console.error(err);
            // });

            const brands = await getBikeBrandsRequest(params);
            //console.log(brands);
            dispatch({ type: BIKE_GET_BRANDS, payload: brands });
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const getBikeTypes = (params =null) => {
    return async dispatch => {
        try {
            const types = await getBikeTypesRequest(params );
            // console.log(types);
            dispatch({ type: BIKE_GET_TYPES, payload: types });
        }
        catch (err) {
            console.log(err);
        }
    }
}





const getBikeFromArray = (bikes, id) => {
    const filtered = bikes.filter(bike => bike.id === id);
    return filtered.length > 0 ? filtered[0] : null;
}