import {
    GET_PRE_ORDERS, SITE_CMS, GET_PREVIOUS_ORDERS
} from '../../constants/redux.contants';

import {
    getPreOrders, getSiteCMSDetails
} from '../../api/preOrder.api';


import {
    GET_QUESTION, GET_ONE_QUESTION
} from '../../constants/redux.contants';

import {
    getQuestion, getQuestionOne,getPreviousWinnderResult
} from '../../api/preOrder.api';


import {
    GET_FAQ
} from '../../constants/redux.contants';

import {
    getFaq
} from '../../api/preOrder.api';





export const getPreOrdersByUser = (params) => {

    return async dispatch => {
        try {
            const orders = await getPreOrders(params);
          //  console.log("params....", orders);

            dispatch({ type: GET_PRE_ORDERS, payload: orders });
        }
        catch (err) {
            console.log(err);
        }
    }
}
export const getPreviousWinnder = (params) => {

    return async dispatch => {
        try {
        //    console.log("params....", params);
            const previousOrders = await getPreviousWinnderResult(params);
            dispatch({ type: GET_PREVIOUS_ORDERS, payload: previousOrders });
        }
        catch (err) {
            console.log(err);
        }
    }

}

    export const getAllQuestion = () => {
        return async dispatch => {
            try {

                const questions = await getQuestion();

                dispatch({ type: GET_QUESTION, payload: questions });
            }
            catch (err) {
                console.log(err);
            }
        }
    }

    export const getOneQuestion = () => {
        return async dispatch => {
            try {
                const questions = await getQuestionOne();
                dispatch({ type: GET_ONE_QUESTION, payload: questions });
            }
            catch (err) {
                console.log(err);
            }
        }
    }

    export const getSiteCMS = () => {
        return async dispatch => {
            try {
                const allData = await getSiteCMSDetails();
                //console.log('ss',allData);
                dispatch({ type: SITE_CMS, payload: allData });
            }
            catch (err) {
                console.log(err);
            }
        }
    }


    export const getAllFaq = () => {
        return async dispatch => {
            try {
                const faq = await getFaq();

                dispatch({ type: GET_FAQ, payload: faq });
            }
            catch (err) {
                console.log(err);
            }
        }
    }