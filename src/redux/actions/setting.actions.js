/**
 * Setting Actions
 * @description includes auth, menu etc.
 * */

import {
    CART_HOVER,
    CART_TOGGLE,
    MENU_LAYER,
    MENU_TOGGLE,
    STICK_HEADER
} from '../../constants/redux.contants';

export const setMenuLayer = (zIndex) => {
    return async dispatch => {
        dispatch({ type: MENU_LAYER, payload: zIndex });
    }
}

export const toggleMenu = (isOpen = null) => {
    return async dispatch => {
        dispatch({ type: MENU_TOGGLE, payload: isOpen });
    }
}

export const toggleShoppingCart = (isOpen = null) => {
    return async dispatch => {
        dispatch({ type: CART_TOGGLE, payload: isOpen });
    }
}

export const toggleHeaderStick = (toStick = null) => {
    return async dispatch => {
        dispatch({ type: STICK_HEADER, payload: toStick });
    }
}

export const setCartHover = (toValue) => {
    return dispatch => {
        console.log('[setCartHover action]', toValue);
        dispatch({ type: CART_HOVER, payload: toValue });
    }
}