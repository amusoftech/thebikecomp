import { 
    CART_ADD,
    CART_DELETE,
    CART_DELETE_ALL,
    CART_SUBMIT_TICKET,
    SELECTED_QUESTION,
} from '../../constants/redux.contants';



export const addBikeToCart = (bike, amount) => {
    return dispatch => {
        try {
            dispatch({type: CART_ADD, payload: { bike: bike, amount: amount }}); 
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const deleteBike = (bike) => {
    return dispatch => {
        try {
            dispatch({ type: CART_DELETE, payload: bike });
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const emptyCart = () => {
    return dispatch => {
        try {
            dispatch({ type: CART_DELETE_ALL, payload: {} });
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const submitTicket = (bike_id, coords) => {
    return dispatch => {
        try {
            dispatch({ type: CART_SUBMIT_TICKET, payload: {bike_id, coords} })
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const setSelectedQuestion = (questionId, answerId) => {
    return dispatch => {
        try {
           // console.log("questionId, answerId",questionId, answerId);
            dispatch({ type: SELECTED_QUESTION, payload: {questionId, answerId} })
        }
        catch (err) {
            console.log(err);
        }
    }
}



