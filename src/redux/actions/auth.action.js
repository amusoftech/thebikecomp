import {
    TOGGLE_EMAIL_PREF,
    AUTH_LOGIN, AUTH_LOGOUT, AUTH_REGISTER,CHANGE_PASSWORD,FORGOT_PASSWORD,RECENT_ORDER, SET_AUTH_USER, SOCIAL_REGISTER,UPDATE_PROFILE,SUBSCRIBE_USER, FETCH_USER_BY_TOKEN,CONTACT_US
} from '../../constants/redux.contants';
import {toastr} from 'react-redux-toastr'

import {
    fetchUserByTokenApi,
    changePassRequest,
    forgotPassRequest,
    subscribeRequest,
    getSessionRequset, loginRequest, registerRequest, toggleUserEmailPref, updateProfileRequest,socialRegisterRequest,contactRequest
} from '../../api/auth.api';

import {  deleteAuthToken, getAuthToken, getLoginUserDetails, saveALoginUserDetails, saveAuthToken,deleteALoginUserDetails } from '../../utils/common.utils';

export const getSession = () => {
    return async dispatch => {
        try {
            const token = getAuthToken();
            const res = await getSessionRequset(token);
            console.log('tokkenn',res);
            if (res.status === true) {
                dispatch({ type: AUTH_REGISTER, payload: res.user });
            } else {
                // dispatch error
            }
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const login = (data, callback) => {
    return async dispatch => {
        try {
            const res = await loginRequest(data);
            if (res.status === true) {
                saveALoginUserDetails(JSON.stringify(res.user));
                saveAuthToken(res.token); 
                dispatch({ type: AUTH_LOGIN, payload:data });  
                if (typeof callback === 'function') {
                    callback(res);
                }
            } else {
                callback(res);
              //  toastr.error('Oops', res.message)
                // dispatch error
            }
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const logout = () => {
    return async dispatch => {
        try {
            dispatch({ type: AUTH_LOGOUT, payload: null });
            deleteAuthToken();
            deleteALoginUserDetails();
           
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const contact = (data, callback = null) => {
    return async dispatch => {
        try {
            const res = await contactRequest(data);            
            if (res.status === true) {
                saveAuthToken(res.token);
                dispatch({ type: CONTACT_US, payload: res.user });
                if (typeof callback === 'function') {
                    callback();
                }
            } else {
                callback(res);
                // dispatch error
               // alert(res.message)
            }
           
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const register = (data, callback = null) => {
    return async dispatch => {
        try {
            const res = await registerRequest(data);
            const ress = await toggleUserEmailPref(data);  
            if (res.status === true) {
                saveAuthToken(res.token);
                dispatch({ type: AUTH_REGISTER, payload: res.user });
                if (typeof callback === 'function') {
                    callback();
                }
            } else {
                callback(res);
                // dispatch error
               // alert(res.message)
            }
           
        }
        catch (err) {
            console.log(err);
        }
    }
}
export const socialLogin = (data, callback = null) => {
    return async dispatch => {
      
        try {
            const res = await socialRegisterRequest(data);
            console.log('social Login',res.user);           
            if (res.status === true) {
                saveALoginUserDetails(JSON.stringify(res.user));              
                saveAuthToken(res.token);               
                dispatch({ type: SOCIAL_REGISTER, payload: res.user });
                if (typeof callback === 'function') {
                    callback(res);
                }
            } else {
                callback(res);
                // dispatch error
            }
            // dispatch({ type: BIKE_GET_BIKES, payload: bikes });
            // dispatch({ type: CART_SET, payload: cartContent });
        }
        catch (err) {
            console.log(err);
        }
    }
}


export const recentOrder = () => {
    return async dispatch => {
        try {
            deleteAuthToken();
            dispatch({ type: RECENT_ORDER, payload: null });
        }
        catch (err) {
            console.log(err);
        }
    }
}


export const getUserFromAsync = () => {
    return async dispatch => {
        try {
            const userDeails = getLoginUserDetails()
            //console.log("userDeails asyb",userDeails);
            if (userDeails) {
                
                dispatch({ type: SET_AUTH_USER, payload: userDeails });
                 
            } else {
                
                // dispatch error
            }
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const fetchUserByToken = (data, callback) => { 
     return async dispatch => {
         try {  
             const res = await fetchUserByTokenApi(data)  //loginRequest(data);
            
             console.log("fetchUserByToken",res);
               if (res.status === true) {                 
                 dispatch({ type: FETCH_USER_BY_TOKEN, payload: {status:res.status, user: res.data} });
                 if (typeof callback === 'function') {
                     callback();
                 }
             } else { 
                  
             }  
         }
         catch (err) {
             console.log(err);
         }
     }
 }



export const forgot = (data, callback) => {
   // console.log("forgot email",data);
    return async dispatch => {
        try {
            const res = await forgotPassRequest(data)  //loginRequest(data);
            //console.log("response status",res);
            //toastr.success(res.data, res.message) 
            if (res.status === true) {
                // saveALoginUserDetails(JSON.stringify(res.user))
               // saveAuthToken(res.token);
                dispatch({ type: FORGOT_PASSWORD, payload: res.status });
                if (typeof callback === 'function') {
                    callback(res);
                }
            } else {
                callback(res);
                // dispatch error
            }
        }
        catch (err) {
            console.log(err);
        }
    }
}
export const subscribe = (data, callback) => {   
    return async dispatch => {
        try {
            const res = await subscribeRequest(data)  //loginRequest(data);
            console.log("response status",res);
            //toastr.success(res.data, res.message) 
            if (res.status === true) {
                // saveALoginUserDetails(JSON.stringify(res.user))
               // saveAuthToken(res.token);
                dispatch({ type: SUBSCRIBE_USER, payload: res.status });
                if (typeof callback === 'function') {
                    callback();
                }
            } else {
                
                // dispatch error
            }
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const changePassword = (data, callback) => {
    return async dispatch => {
        try { 
            const res = await changePassRequest(data)  //loginRequest(data); 
            console.log("response status",res);  
            if (res.status === true) {  
                dispatch({ type: CHANGE_PASSWORD, payload: data });
                if (typeof callback === 'function') {
                    callback();
                }
            } else {
                // dispatch error
            }
        }
        catch (err) {
            console.log(err);
        }
    }
}
export const updateProfille = (data, callback) => {
    return async dispatch => {
        try {
            const res = await updateProfileRequest(data)  ;
            console.log("Response status",data); 
            if (res.status === true) {
           
                saveALoginUserDetails(JSON.stringify(data));                
               // saveAuthToken(res.token);
                dispatch({ type: UPDATE_PROFILE, payload: data });
                dispatch({ type: AUTH_LOGIN, payload: data });
                if (typeof callback === 'function') {
                    callback();
                }
            } else {
                console.log("Response status False"); 
            }
        }
        catch (err) {
            console.log(err);
        }
    }
}

export const updateEmailPref = (data, callback) => {
    return async dispatch => {
        try {
            const res = await toggleUserEmailPref(data);             
            if (res.status === true) { 
                dispatch({ type: TOGGLE_EMAIL_PREF, payload: res }); 
                dispatch({ type: AUTH_LOGIN, payload: res });
                if (typeof callback === 'function') {
                    callback();
                }
            } else {
                console.log("Response status False"); 
            }
        }
        catch (err) {
            console.log(err);
        }
    }
}
