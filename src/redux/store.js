import { applyMiddleware, compose, combineReducers, createStore } from 'redux'
import thunk from 'redux-thunk'

import { authReducer, bikeReducer, cartReducer, settingReducer, preOrderReducer } from './reducers'
import { reducer as toastrReducer } from 'react-redux-toastr'

const reducers = combineReducers({
    auth: authReducer,
    bike: bikeReducer,
    cart: cartReducer,
    setting: settingReducer,
    preOrderReducer:preOrderReducer,
    toastr: toastrReducer

});

let allStoreEnhancers;
if (process.env.NODE_ENV === 'development') {
    /* const reduxDevtools =
        typeof window !== 'undefined' && process.env.NODE_ENV !== 'production'
            ? window.__REDUX_DEVTOOLS_EXTENSION__ &&
            window.__REDUX_DEVTOOLS_EXTENSION__()
            : f => f */
    allStoreEnhancers = compose(applyMiddleware(thunk));

} else {
    allStoreEnhancers = compose(applyMiddleware(thunk));
}

const makeStore = () => {
    const store = createStore(reducers, allStoreEnhancers)
    return store
}

export default makeStore;