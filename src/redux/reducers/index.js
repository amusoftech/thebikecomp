export { default as authReducer } from './auth.reducer';
export { default as bikeReducer } from './bikes.reducer';
export { default as cartReducer } from './cart.reducer';
export { default as settingReducer } from './setting.reducers';
export { default as preOrderReducer } from './preOrder.reducer';
