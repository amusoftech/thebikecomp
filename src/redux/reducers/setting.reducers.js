/**
 * Setting Reducer
 * @includes auth, menu etc.
 */

import {
    CART_HOVER,
    CART_TOGGLE,
    MENU_LAYER,
    MENU_TOGGLE,
    STICK_HEADER
} from '../../constants/redux.contants';

export default (
    state = {
        cartHover: false,
        cartOpen: false,
        menuLayer: 0,
        menuOpen: false,
        stickHeader: false,
        appLoaded: true,
    },
    { type, payload }) => {
    let newState;
    switch (type) {
        case CART_HOVER:
            console.log('[new state]', { ...state, cartHover: payload });
            return { ...state, cartHover: payload };
        case CART_TOGGLE:
            newState = payload === null ? !state.cartOpen : payload;
            if (!!newState) {
                document.body.style.overflow = 'hidden';
            } else {
                document.body.style.overflow = 'auto';
            }
            return { ...state, cartOpen: newState, stickHeader: newState };
        case MENU_LAYER:
            return { ...state, menuLayer: payload };
        case MENU_TOGGLE:
            newState = payload === null ? !state.menuOpen : payload;
            // console.log(state, newState);
            if (!!newState) {
                document.body.style.overflow = 'hidden';
            } else {
                document.body.style.overflow = 'auto';
            }
            return { ...state, menuOpen: newState };
        case STICK_HEADER:
            newState = payload === null ? !state.stickHeader : payload;
            return { ...state, stickHeader: newState };
        default:
            return state;
    }
}


