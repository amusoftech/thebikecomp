/**
 * Bike Reducer
 * @includes brands, types, bikes
 */

import { GET_PRE_ORDERS, GET_QUESTION, GET_FAQ, SITE_CMS ,GET_PREVIOUS_ORDERS} from "../../constants/redux.contants";

export default (
  state = {
    preOrders: [],
    questions: [],
    sitecms:[],
    previousOrders:[]
  },
  { type, payload }
) => {
  // let newState;
  switch (type) {
    case GET_PRE_ORDERS:
      return { ...state, preOrders: payload };
    case GET_QUESTION:
      return { ...state, questions: payload };
    case GET_FAQ:
      return { ...state, faqs: payload };
    case SITE_CMS:
      return {...state, sitecms: payload}
      case GET_PREVIOUS_ORDERS:
        return {...state, previousOrders: payload}
    default:
      return state;
  }
};
