/**
 * Bike Reducer
 * @includes brands, types, bikes
 */

import {
    BIKE_GET_BIKES,
    BIKE_GET_BRANDS,
    BIKE_GET_TYPES,
} from '../../constants/redux.contants';

export default (
    state = {
        bikes: [],
        brands: [],
        types: [],
    },
    { type, payload }) => {
    // let newState;
    switch (type) {
        case BIKE_GET_BIKES:
            return { ...state, bikes: payload };
        case BIKE_GET_BRANDS:
            return { ...state, brands: payload };
        case BIKE_GET_TYPES:
            return { ...state, types: payload };
        default:
            return state;
    }
}