/**
 * Bike Reducer
 * @includes brands, types, bikes
 */

import {
    AUTH_LOGOUT, AUTH_REGISTER, FORGOT_PASSWORD, RECENT_ORDER,
    SET_AUTH_USER,
    UPDATE_PROFILE,
    FETCH_USER_BY_TOKEN
} from '../../constants/redux.contants';

export default (
    state = {
        user: null,
        login: false,
        forgotStatus: false,
        userByToken: null,
        loadingByToken:true
    },
    { type, payload }) => {
    // let newState;
    switch (type) {
        case AUTH_REGISTER:
            return { ...state, login: true, user: payload };
        case AUTH_LOGOUT:
            return { ...state, user: null, login: false };
        case SET_AUTH_USER:
            return { ...state, user: { ...payload }, login: true };

        case RECENT_ORDER:
            return { ...state, login: true, user: payload };
        case FORGOT_PASSWORD:
            return { ...state, forgotStatus: payload };
        case UPDATE_PROFILE:
            return { ...state, user: { ...payload } };
         case FETCH_USER_BY_TOKEN:
            return  { ...state,userByToken : payload.user,loadingByToken:false }   
        default:
            return state;
    }
}