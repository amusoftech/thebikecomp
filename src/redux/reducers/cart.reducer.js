/**
 * Cart Reducer
 * @includes brands, types, bikes
 */
 
import {
    CART_ADD,
    CART_DELETE,
    CART_DELETE_ALL,
    CART_SET,
    CART_SUBMIT_TICKET,
    SELECTED_QUESTION
} from '../../constants/redux.contants';

import { saveCartInfo } from '../../utils/common.utils';



export default (
    state = {
        items: [], // {bike, amount, ticket}
        selectedQuestion:{}
    },
    { type, payload }) => {
    // let newState;
    let newItems;
    let cacheData;
    const newTicketItem = { x: '0.00', y: '0.00' };
    switch (type) {
        case CART_ADD:
            newItems = state.items;
            let bikeIds = state.items.map(item => item.bike.id);

            if (bikeIds.includes(payload.bike.id)) {
                newItems = state.items.map(item => Number(item.bike.id) === Number(payload.bike.id) ? { ...item, amount: Math.max(item.amount + payload.amount, 1) } : item);
            } else {
                newItems.push(payload);
            }

            //save only cart info, not ticket info
            cacheData = newItems.map(item => { return { id: item.bike.id, amount: item.amount }; }); //console.log('[cart]', cacheData);
            saveCartInfo(cacheData);

            // compose tickets info
            for (let item of newItems) {
                let tickets = item.tickets || [];
                if (tickets.length > item.amount) {
                    tickets = tickets.slice(0, item.amount);
                } else {
                    item.tickets = item.tickets || [];
                    for (let i = 0; i < item.amount - tickets.length; i++) {
                        tickets.push(newTicketItem);
                    }
                }
                item.tickets = tickets;
                item.ticketSubmitIndex = Math.max(0, item.ticketSubmitIndex || 0);
            }

            return { ...state, items: newItems };
        case CART_DELETE:
            newItems = state.items.filter(item => item.bike.id !== payload.id);
            cacheData = newItems.map(item => { return { id: item.bike.id, amount: item.amount }; }); console.log('[cart]', cacheData);
            saveCartInfo(cacheData);
            return { ...state, items: newItems };
        case CART_DELETE_ALL:
            newItems = [];
            cacheData = [];
            saveCartInfo(cacheData);
            return { ...state, items: newItems };
        case CART_SET:
            // console.log('[Cart Set]', payload)
            // compose tickets info
            for (let item of payload) {
                // console.log('[Item]', item);
                let tickets = item.tickets || [];
                if (tickets.length > item.amount) {
                    item.amount = tickets.slice(0, item.amount);
                } else {
                    let initTicketCount = tickets.length;
                    for (let i = 0; i < item.amount - initTicketCount; i++) {
                        tickets.push(newTicketItem);
                    }
                    item.tickets = tickets;
                    item.ticketSubmitIndex = 0;
                }
            }
            return { ...state, items: payload };
        case CART_SUBMIT_TICKET:            
            return {
                ...state,
                items: state.items.map((item, i) => {
                    return {
                        ...item,
                        tickets: item.tickets.map((ticket, i) => item.bike.id===payload.bike_id && item.ticketSubmitIndex === i && item.ticketSubmitIndex < item.amount ? payload.coords : ticket),
                        ticketSubmitIndex: item.bike.id===payload.bike_id && item.ticketSubmitIndex < item.amount? item.ticketSubmitIndex + 1 : item.ticketSubmitIndex,
                    }
                })
            };

         case SELECTED_QUESTION :
            return {
                ...state,
                selectedQuestion:payload
             }   
            break;
        default:
            return state;
    }
}